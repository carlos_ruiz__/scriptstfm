#'#################################################################################
#'#################################################################################
#' Run survival test on TCGA
#'#################################################################################
#'#################################################################################

# Load files and libraries ####
library(survival)

load("colDataTumors.Rdata")
load("TCGAinversionClassificationGenos.Rdata")
load("TCGA_ancestry.Rdata")


## Add columns with data required for survival
# get the columns that contain data we can use: days to death, new tumor event, last day contact to....
addSurvivalCols <- function(clinical){
  ind_keep <- grep('days_to_new_tumor_event_after_initial_treatment', colnames(clinical))
  
  # this is a bit tedious, since there are numerous follow ups, let's collapse them together and keep the first value (the higher one) if more than one is available
  new_tum <- as.matrix(clinical[,ind_keep])
  new_tum_collapsed <- c()
  for (i in 1:nrow(new_tum)){
    if ( sum ( is.na(new_tum[i,])) < dim(new_tum)[2]){
      m <- min(new_tum[i,],na.rm=T)
      new_tum_collapsed <- c(new_tum_collapsed,m)
    } else {
      new_tum_collapsed <- c(new_tum_collapsed,NA)
    }
  }
  
  # do the same to death
  ind_keep <- grep('days_to_death',colnames(clinical))
  death <- as.matrix(clinical[,ind_keep])
  death_collapsed <- c()
  for (i in 1:dim(death)[1]){
    if ( sum ( is.na(death[i,])) < dim(death)[2]){
      m <- max(death[i,],na.rm=T)
      death_collapsed <- c(death_collapsed,m)
    } else {
      death_collapsed <- c(death_collapsed,NA)
    }
  }
  
  # and days last follow up here we take the most recent which is the max number
  ind_keep <- grep('days_to_last_followup',colnames(clinical))
  fl <- as.matrix(clinical[,ind_keep])
  fl_collapsed <- c()
  for (i in 1:dim(fl)[1]){
    if ( sum (is.na(fl[i,])) < dim(fl)[2]){
      m <- max(fl[i,],na.rm=T)
      fl_collapsed <- c(fl_collapsed,m)
    } else {
      fl_collapsed <- c(fl_collapsed,NA)
    }
  }
  
  # and put everything together
  all_clin <- data.frame(new_tum_collapsed,death_collapsed,fl_collapsed)
  colnames(all_clin) <- c('new_tumor_days', 'death_days', 'followUp_days')
  
  # create vector with time to new tumor containing data to censor for new_tumor
  all_clin$new_time <- ifelse (is.na(all_clin$new_tumor_days), 
                               all_clin$followUp_days, all_clin$new_tumor_days)
  
  # create vector time to death containing values to censor for death
  all_clin$new_death <- ifelse (is.na(all_clin$death_days),
                                      all_clin$followUp_days,all_clin$death_days)
  
  
  all_clin$death_event <- ifelse(clinical$patient.vital_status == 'alive', 0,1)
  all_clin$new_event <- as.numeric(!is.na(all_clin$new_tumor_days))
  # Add covariates
  covars <- c("PC1", "PC2", "PC3", "PC4")
  
  pvars <- colnames(clinical)
  
  if ("years_to_birth" %in% pvars){
    all_clin$years_to_birth <- clinical$years_to_birth
  }
  
  if ("gender" %in% pvars){
    all_clin$gender <- clinical$gender
  }
  if("histological_type" %in% pvars){
    var <- clinical$histological_type
    var <- var[!is.na(var)]
    if (length(unique(var)) > 1){
      all_clin$histological_type <- clinical$histological_type
    }
  }
  all_clin <- cbind(all_clin, ancestry[clinical$patientID, c(paste0("PC", 1:4))])
  

  if("pathologic_stage" %in% pvars){
    var <- clinical$pathologic_stage
    var <- var[!is.na(var)]
    if (length(unique(var)) > 1){
      all_clin$pathologic_stage <- clinical$pathologic_stage
    }
  }
  
  #finally add row.names to clinical
  rownames(all_clin) <- rownames(clinical)
  all_clin
}
colDataSurv <- lapply(colDataList, addSurvivalCols)
save(colDataSurv, file = "SurvivalData.Rdata")

# Evaluate all tumors together ####
## Create data.frame with all tumors ####

## ColDataList including gender
#### Remove UCEC because it does not contain samples' age
colDataSurvGender <- colDataSurv[names(colDataSurv) != "UCEC"]

colDataSurvGender <- lapply(names(colDataSurvGender), function(tum){
  df <- colDataSurvGender[[tum]]
  df$tumor <- tum
  df
})
commonCols <- Reduce(intersect, lapply(colDataSurvGender, colnames))

colDataSurvGender <- lapply(colDataSurvGender, function(df){
  df <- df[, commonCols]
  df
})
colDataSurvAll <- Reduce(rbind, colDataSurvGender)
save(colDataSurvAll, file = "SurvivalDataAll.Rdata")

## Select inversions ####
invs <- TCGA_inversions
invs <- invs[!names(invs) %in% c("inv1_008", "inv21_005")]

# Survival Function ####
computeSurvival <- function(clinical, invGenos, vars = NULL, form = "Surv(clinical$new_death, clinical$death_event) ~ inv +"){
  
  clinical$inv <- invGenos[rownames(clinical)]
  clinical <- clinical[!is.na(clinical$inv), ]
  
  ## Do not compute codominant model when number of II samples is low
  if (is.factor(clinical$inv) & min(table(clinical$inv)) < 10){
    stop("Not enough samples in II group to test codominant model")
  }
  
  if ("gender" %in% vars & !all(clinical$gender == "female", na.rm = TRUE) & 
      !all(clinical$gender == "male", na.rm = TRUE)){
  } else{
    vars <- vars[vars != "gender"]
  }
  
  vars <- vars[vars %in% colnames(clinical)]

  form <- paste(form, paste(vars, collapse = " + "))
  res <- coxph(formula(form), data = clinical)
  res
}

## Remove samples without inversions
colDataSurvAll$inv17 <- TCGA_inversions$inv17_007[rownames(colDataSurvAll)]
colDataSurvAll <- colDataSurvAll[!is.na(colDataSurvAll$inv17), ]

# Survival with all tumors ####

## Select tumors with at least 30 events
selTumors <- names(which(
  tapply(colDataSurvAll$death_event, colDataSurvAll$tumor, sum) > 30))
clinSurv <- colDataSurvAll[colDataSurvAll$tumor %in% selTumors, ]

survivalAllAdd <- lapply(invs, function(y) {
  
  inv <- rep(0, length(y))
  inv[y == "NI"] <- 1
  inv[y == "II"] <- 2
  names(inv) <- names(y)
  tryCatch(computeSurvival(clinSurv, inv, vars = c("years_to_birth", "gender", "PC1", "PC2", "PC3", "PC4"), 
                           form =  "Surv(clinical$new_death, clinical$death_event) ~ inv + frailty(tumor) +"), error = function(e) as.character(e))
  })

# Recurrence with all tumors ####

## Select tumors with at least 30 events
selTumorsRec <- names(which(
  tapply(colDataSurvAll$new_event, colDataSurvAll$tumor, sum) > 30))
clinRec <- colDataSurvAll[colDataSurvAll$tumor %in% selTumorsRec, ]


recAllAdd <- lapply(TCGA_inversions, function(y) {
  
  inv <- rep(0, length(y))
  inv[y == "NI"] <- 1
  inv[y == "II"] <- 2
  names(inv) <- names(y)
  tryCatch(computeSurvival(clinRec, inv, vars = c("years_to_birth", "gender", "PC1", "PC2", "PC3", "PC4"), 
                           form =  "Surv(clinical$new_time, clinical$new_event) ~ inv + frailty(tumor) +"), error = function(e) as.character(e))
})
save(survivalAllAdd, recAllAdd, file = "SurvivalResAllTumors.Rdata")

# Evaluate each tumor independently ####
TCGA_inversions2 <- TCGA_inversions[c("inv17_007", "invX_006_female", "invX_006_male", "inv8_001", "inv7_005")]

### Survival ####
clinListSurv <- colDataSurv[selTumors]

survivalTumAddCrude <- lapply(clinListSurv, function(x){
  res <- lapply(TCGA_inversions, function(y) {
    inv <- rep(0, length(y))
    inv[y == "NI"] <- 1
    inv[y == "II"] <- 2
    names(inv) <- names(y)
    
    tryCatch(computeSurvival(x, inv,vars = NULL,
                             form = "Surv(clinical$new_death, clinical$death_event) ~ inv" ), error = function(e) NULL)
  })
  res <- res[!sapply(res, is.null)]
  res
})
survivalpvalsAddCrude <- lapply(survivalTumAddCrude, sapply, function(y) 
  tryCatch(summary(y)$coefficients[1, 5],  error = function(e) NA))
survpvalsAddCrudeinv17 <- sapply(survivalTumAddCrude, function(y) 
  tryCatch(summary(y$inv17_007)$coefficients[1, 5],  error = function(e) NA))



survivalTumAdd <- lapply(clinListSurv, function(x){
  res <- lapply(TCGA_inversions, function(y) {
    inv <- rep(0, length(y))
    inv[y == "NI"] <- 1
    inv[y == "II"] <- 2
    names(inv) <- names(y)
    
    tryCatch(computeSurvival(x, inv,vars = c("years_to_birth", "gender", paste0("PC", 1:4)),
                             form = "Surv(clinical$new_death, clinical$death_event) ~ inv +" ), error = function(e) NULL)
  })
  res <- res[!sapply(res, is.null)]
  res
})
survivalpvalsAdd <- lapply(survivalTumAdd, sapply, function(y) 
  tryCatch(summary(y)$coefficients[1, 5],  error = function(e) NA))
survpvalsAddinv17 <- sapply(survivalTumAdd, function(y) 
  tryCatch(summary(y$inv17_007)$coefficients[1, 5],  error = function(e) NA))

## Further adjust nominally significant associations

## STAD.inv17_007
### Add inversion
clin <- colDataSurv$STAD
clin$inv <- invs$inv17_007[rownames(clin)]
clin$invadd <- NA
clin$invadd[clin$inv == "NN"] <- 0
clin$invadd[clin$inv == "NI"] <- 1
clin$invadd[clin$inv == "II"] <- 2

## Recode stage
clin$stage <- clin$pathologic_stage
clin$stage[clin$stage %in% c("stage ia", "stage ib")] <- "stage i"
clin$stage[clin$stage %in% c("stage iia", "stage iib")] <- "stage ii"
clin$stage[clin$stage  %in% c("stage iiia", "stage iiib", "stage iiic")] <- "stage iii"
survSTAD.inv17_007 <- coxph(Surv(clin$new_death, clin$death_event) ~ invadd + gender + years_to_birth + stage + PC1 + PC2 + PC3 + PC4, data = clin)
# p-val: 0.0012


## MESO.inv17_007
### Add inversion
clin <- colDataSurv$MESO
clin$inv <- invs$inv17_007[rownames(clin)]
clin$invadd <- NA
clin$invadd[clin$inv == "NN"] <- 0
clin$invadd[clin$inv == "NI"] <- 1
clin$invadd[clin$inv == "II"] <- 2

## Recode stage
clin$stage <- clin$pathologic_stage
clin$stage[clin$stage %in% c("stage ia", "stage ib")] <- "stage i"
clin$stage[clin$stage %in% c("stage iia", "stage iib")] <- "stage ii"
clin$stage[clin$stage  %in% c("stage iiia", "stage iiib", "stage iiic")] <- "stage iii"
survMESO.inv17_007 <- coxph(Surv(clin$new_death, clin$death_event) ~ invadd + gender + years_to_birth + stage + PC1 + PC2 + PC3 + PC4, data = clin)
# p-val: 0.035



## LIHC.inv2_013 
clin <- colDataSurv$LIHC
clin$inv <- invs$inv2_013[rownames(clin)]
clin$invadd <- 0
clin$invadd[clin$inv == "NI"] <- 1
clin$invadd[clin$inv == "II"] <- 2

## Recode stage
clin$stage <- clin$pathologic_stage
clin$stage[clin$stage  %in% c("stage iiia", "stage iiib", "stage iiic")] <- "stage iii"
clin$stage[clin$stage  %in% c("stage iva", "stage ivb")] <- "stage iv"

survLIHC.inv2_013 <- coxph(Surv(clin$new_death, clin$death_event) ~ invadd + gender + years_to_birth + stage + PC1 + PC2 + PC3 + PC4, data = clin)
# p-val: 0.0176 (bigger)

## LIHC.inv11_001 
clin <- colDataSurv$LIHC
clin$inv <- invs$inv11_001[rownames(clin)]
clin$invadd <- 0
clin$invadd[clin$inv == "NI"] <- 1
clin$invadd[clin$inv == "II"] <- 2

## Recode stage
clin$stage <- clin$pathologic_stage
clin$stage[clin$stage  %in% c("stage iiia", "stage iiib", "stage iiic")] <- "stage iii"
clin$stage[clin$stage  %in% c("stage iva", "stage ivb")] <- "stage iv"

survLIHC.inv11_001 <- coxph(Surv(clin$new_death, clin$death_event) ~ invadd + gender + years_to_birth + stage + PC1 + PC2 + PC3 + PC4, data = clin)
# p-value: 0.023

## MESO.inv17_007 
clin <- colDataSurv$MESO
clin$inv <- invs$inv17_007[rownames(clin)]
clin$invadd <- 0
clin$invadd[clin$inv == "NI"] <- 1
clin$invadd[clin$inv == "II"] <- 2

## Recode stage
clin$stage <- clin$pathologic_stage
clin$stage[clin$stage  %in% c("stage ia", "stage ib")] <- "stage i"
survMESO.inv17_007 <- coxph(Surv(clin$new_death, clin$death_event) ~ invadd + gender + years_to_birth + stage + PC1 + PC2 + PC3 + PC4, data = clin)
# p-value: 0.035 (bigger)

## BLCA.inv7_005
clin <- colDataSurv$BLCA
clin$inv <- invs$inv7_005[rownames(clin)]
clin$invadd <- 0
clin$invadd[clin$inv == "NI"] <- 1
clin$invadd[clin$inv == "II"] <- 2

## Recode stage
clin$stage <- clin$pathologic_stage
clin$stage[clin$stage  %in% c("stage i", "stage ii")] <- "stage i/ii"
survBLCA.inv7_005 <- coxph(Surv(clin$new_death, clin$death_event) ~ invadd + gender + years_to_birth + stage + PC1 + PC2 + PC3 + PC4, data = clin)
# p-value: 0.03212

### Recurrence ####
clinListRec <- colDataSurv[selTumorsRec]


recTumAddCrude <-  lapply(clinListRec, function(x){
  res <- lapply(invs, function(y) {
    inv <- rep(NA, length(y))
    inv[y == "NN"] <- 0
    inv[y == "NI"] <- 1
    inv[y == "II"] <- 2
    names(inv) <- names(y)
    tryCatch(computeSurvival(x, inv, vars = NULL,
                             form =  "Surv(clinical$new_time, clinical$new_event) ~ inv"), error = function(e) as.character(e))
  })
  
  res <- res[!sapply(res, is.null)]
  res
})
recpvalsAddCrude <- lapply(recTumAddCrude, sapply, function(y) 
  tryCatch(summary(y)$coefficients[1, 5],  error = function(e) NA))
recpvalsAddCrudeinv17 <- sapply(recTumAddCrude, function(y) 
  tryCatch(summary(y$inv17_007)$coefficients[1, 5],  error = function(e) NA))


recTumAdd <-  lapply(clinListRec, function(x){
  res <- lapply(invs, function(y) {
    inv <- rep(0, length(y))
    inv[y == "NI"] <- 1
    inv[y == "II"] <- 2
    names(inv) <- names(y)
    tryCatch(computeSurvival(x, inv, vars = c("years_to_birth", "gender", paste0("PC", 1:4)),
                             form =  "Surv(clinical$new_time, clinical$new_event) ~ inv +"), error = function(e) as.character(e))
  })
  
  res <- res[!sapply(res, is.null)]
  res
})
recpvalsAdd <- lapply(recTumAdd, sapply, function(y) 
  tryCatch(summary(y)$coefficients[1, 5],  error = function(e) NA))


## Further adjust nominally significant associations

## COAD.inv17_007
### Add inversion
clin <- colDataSurv$COAD
clin$inv <- invs$inv17_007[rownames(clin)]
clin$invadd <- NA
clin$invadd[clin$inv == "NN"] <- 0
clin$invadd[clin$inv == "NI"] <- 1
clin$invadd[clin$inv == "II"] <- 2

## Recode stage
clin$stage <- clin$pathologic_stage
clin$stage[clin$stage == "stage ia"] <- "stage i"
clin$stage[clin$stage %in% c("stage iia", "stage iib", "stage iic")] <- "stage ii"
clin$stage[clin$stage  %in% c("stage iiia", "stage iiib", "stage iiic")] <- "stage iii"
clin$stage[clin$stage  %in% c("stage iva", "stage ivb")] <- "stage iv"
recCOAD.inv17_007 <- coxph(Surv(clin$new_time, clin$new_event) ~ invadd + gender + years_to_birth + stage + PC1 + PC2 + PC3 + PC4, data = clin)
recCOAD.inv17_007_crude <- coxph(Surv(clin$new_time, clin$new_event) ~ invadd, data = clin)

clin <- subset(clin, inv != "II")
recCOAD.NN_NI.inv17_007 <- coxph(Surv(clin$new_time, clin$new_event) ~ invadd + gender + years_to_birth + stage + PC1 + PC2 + PC3 + PC4, data = clin)
recCOAD.NN_NI.inv17_007_crude <- coxph(Surv(clin$new_time, clin$new_event) ~ invadd, data = clin)


## Overdominant
clin$invover <- NA
clin$invover[clin$inv == "NN"] <- 0
clin$invover[clin$inv == "NI"] <- 1
clin$invover[clin$inv == "II"] <- 0
recCOAD.inv17_007.over <- coxph(Surv(clin$new_time, clin$new_event) ~ invover + gender + years_to_birth + stage + PC1 + PC2 + PC3 + PC4, data = clin)

## Dominant
clin$invdom <- NA
clin$invdom[clin$inv == "NN"] <- 0
clin$invdom[clin$inv == "NI"] <- 1
clin$invdom[clin$inv == "II"] <- 1
recCOAD.inv17_007.dom <- coxph(Surv(clin$new_time, clin$new_event) ~ invdom + gender + years_to_birth + stage + PC1 + PC2 + PC3 + PC4, data = clin)



## READ inv17_007
clin <- colDataSurv$READ
clin$inv <- invs$inv17_007[rownames(clin)]
clin$invadd <- NA
clin$invadd[clin$inv == "NN"] <- 0
clin$invadd[clin$inv == "NI"] <- 1
clin$invadd[clin$inv == "II"] <- 2

## Recode stage
clin$stage <- clin$pathologic_stage
clin$stage[clin$stage == "stage ia"] <- "stage i"
clin$stage[clin$stage %in% c("stage iia", "stage iib", "stage iic")] <- "stage ii"
clin$stage[clin$stage  %in% c("stage iiia", "stage iiib", "stage iiic")] <- "stage iii"
clin$stage[clin$stage  %in% c("stage iva", "stage ivb")] <- "stage iv"
recREAD.inv17_007 <- coxph(Surv(clin$new_time, clin$new_event) ~ invadd + gender + years_to_birth + stage + PC1 + PC2 + PC3 + PC4, data = clin)
recREAD.inv17_007_crude <- coxph(Surv(clin$new_time, clin$new_event) ~ invadd, data = clin)

clin <- subset(clin, inv != "II")
recREAD.NN_NI.inv17_007 <- coxph(Surv(clin$new_time, clin$new_event) ~ invadd + gender + years_to_birth + stage + PC1 + PC2 + PC3 + PC4, data = clin)
recREAD.NN_NI.inv17_007_crude <- coxph(Surv(clin$new_time, clin$new_event) ~ invadd, data = clin)



## COAD + READ inv17_007
clin <- rbind(colDataSurv$COAD, colDataSurv$READ[, -10])
clin$inv <- invs$inv17_007[rownames(clin)]
clin$invadd <- NA
clin$invadd[clin$inv == "NN"] <- 0
clin$invadd[clin$inv == "NI"] <- 1
clin$invadd[clin$inv == "II"] <- 2
clin$type <- rep(c("COAD", "READ"), 
                 c(nrow(colDataSurv$COAD), nrow(colDataSurv$READ)))

## Recode stage
clin$stage <- clin$pathologic_stage
clin$stage[clin$stage == "stage ia"] <- "stage i"
clin$stage[clin$stage %in% c("stage iia", "stage iib", "stage iic")] <- "stage ii"
clin$stage[clin$stage  %in% c("stage iiia", "stage iiib", "stage iiic")] <- "stage iii"
clin$stage[clin$stage  %in% c("stage iva", "stage ivb")] <- "stage iv"
recCOADREAD.inv17_007 <- coxph(Surv(clin$new_time, clin$new_event) ~ invadd + gender + years_to_birth + stage + PC1 + PC2 + PC3 + PC4, data = clin)
recCOADREAD.inv17_007_crude <- coxph(Surv(clin$new_time, clin$new_event) ~ invadd, data = clin)

## Overdominant
clin$invover <- NA
clin$invover[clin$inv == "NN"] <- 0
clin$invover[clin$inv == "NI"] <- 1
clin$invover[clin$inv == "II"] <- 0
recCOADREAD.inv17_007.over <- coxph(Surv(clin$new_time, clin$new_event) ~ invover + gender + years_to_birth + stage + PC1 + PC2 + PC3 + PC4, data = clin)

## Dominant
clin$invdom <- NA
clin$invdom[clin$inv == "NN"] <- 0
clin$invdom[clin$inv == "NI"] <- 1
clin$invdom[clin$inv == "II"] <- 1
recCOADREAD.inv17_007.dom <- coxph(Surv(clin$new_time, clin$new_event) ~ invdom + gender + years_to_birth + stage + PC1 + PC2 + PC3 + PC4, data = clin)

## Recessive
clin$invrec <- NA
clin$invrec[clin$inv == "NN"] <- 0
clin$invrec[clin$inv == "NI"] <- 0
clin$invrec[clin$inv == "II"] <- 1
recCOADREAD.inv17_007.rec <- coxph(Surv(clin$new_time, clin$new_event) ~ invrec + gender + years_to_birth + stage + PC1 + PC2 + PC3 + PC4, data = clin)

## Modify stage to include it in the model

clinListRecStage <- lapply(clinListRec, function(x){
  if ("pathologic_stage" %in% colnames(x)){
    x$stage <- x$pathologic_stage
    x$stage[x$stage %in% c("stage ia", "stage ib", "stage ic")] <- "stage i"
    x$stage[x$stage %in% c("stage iia", "stage iib", "stage iic")] <- "stage ii"
    x$stage[x$stage %in% c("stage iiia", "stage iiib", "stage iiic")] <- "stage iii"
    x$stage[x$stage %in% c("stage iva", "stage ivb", "stage ivc")] <- "stage iv"
  }
  x  
})

recTumAddStage <-  lapply(clinListRecStage, function(x){
  res <- lapply(invs, function(y) {
    inv <- rep(NA, length(y))
    inv[y == "NN"] <- 0
    inv[y == "NI"] <- 1
    inv[y == "II"] <- 2
    names(inv) <- names(y)
    tryCatch(computeSurvival(x, inv, vars = c("years_to_birth", "gender", "stage", paste0("PC", 1:4)),
                             form =  "Surv(clinical$new_time, clinical$new_event) ~ inv +"), error = function(e) as.character(e))
  })
  
  res <- res[!sapply(res, is.null)]
  res
})
recpvalsAddStage <- lapply(recTumAddStage, sapply, function(y) 
  tryCatch(summary(y)$coefficients[1, 5],  error = function(e) NA))

recpvalsAddStageinv17 <- sapply(recTumAddStage, function(y) 
  tryCatch(summary(y$inv17_007)$coefficients[1, 5],  error = function(e) NA))


save(survivalTumAdd, recTumAdd, recTumAddStage, file = "survivalResultsNew.Rdata")


################################### 
# A partir de aquí viejo







recTum <- lapply(names(recTum1), 
                  function(x) Map(anova, recTum0[[x]], recTum1[[x]]))
names(recTum) <- names(recTum1)


pvalsRec <- unlist(lapply(recTum, function(x) sapply(x, function(y) y[["P(>|Chi|)"]][2])))
pvalsRec <- pvalsRec[!is.na(pvalsRec)]













lapply(colDataSurv, function(x){
  lapply(TCGA_inversions, function(y) tryCatch(computeSurvival2(x, y ), error = function(e) as.character(e)))})

survivalcoefs2NI <- lapply(survivals2, sapply, function(y) 
  tryCatch(summary(y)$coefficients[1, 5],  error = function(e) NA))

survivalcoefs2NN <- lapply(survivals2, sapply, function(y) 
  tryCatch(summary(y)$coefficients[2, 5],  error = function(e) NA))


## Additive ####
survivalsAdd <- lapply(colDataSurv, function(x){
  lapply(TCGA_inversions, function(y) {
    
    invs <- rep(0, length(y))
    invs[y == "NI"] <- 1
    invs[y == "II"] <- 2
    names(invs) <- names(y)
    
    tryCatch(computeSurvival(x, invs ), error = function(e) as.character(e))
    
    })})
survivalAddCoefs <- lapply(survivalsAdd, sapply, function(y) 
  tryCatch(summary(y)$coefficients[1, 5],  error = function(e) NA))

survivalsAdd2 <- lapply(colDataSurv, function(x){
  lapply(TCGA_inversions, function(y) {
    
    invs <- rep(0, length(y))
    invs[y == "NI"] <- 1
    invs[y == "II"] <- 2
    names(invs) <- names(y)
    
    tryCatch(computeSurvival2(x, invs ), error = function(e) as.character(e))
    
  })})
survivalAddCoefs2 <- lapply(survivalsAdd2, sapply, function(y) 
  tryCatch(summary(y)$coefficients[1, 5],  error = function(e) NA))


## Overdominant
survivalOver <- lapply(colDataSurv, function(x){
  lapply(TCGA_inversions, function(y) {
    
    invs <- rep(0, length(y))
    invs[y == "NI"] <- 1
    names(invs) <- names(y)
    
    tryCatch(computeSurvival(x, invs ), error = function(e) as.character(e))
    
  })})
survivalOverCoefs <- lapply(survivalOver, sapply, function(y) 
  tryCatch(summary(y)$coefficients[1, 5],  error = function(e) NA))

survivalOver2 <- lapply(colDataSurv, function(x){
  lapply(TCGA_inversions, function(y) {
    
    invs <- rep(0, length(y))
    invs[y == "NI"] <- 1
    names(invs) <- names(y)
    
    tryCatch(computeSurvival2(x, invs ), error = function(e) as.character(e))
    
  })})
survivalOverCoefs2 <- lapply(survivalOver2, sapply, function(y) 
  tryCatch(summary(y)$coefficients[1, 5],  error = function(e) NA))

save(survivals, survivals2, survivalsAdd, survivalsAdd2, file = "survivalAnalysis.Rdata")


### Summary
df <- data.frame(codNI = unlist(survivalcoefsNI), codNN = unlist(survivalcoefsNN),
                 add = unlist(survivalAddCoefs), over = unlist(survivalOverCoefs),
                 cancer = rep(names(survivalAddCoefs), lengths(survivalAddCoefs)),
                 inv = names(survivalAddCoefs[[1]]))

library(ggplot2)
library(tidyr)

dfPlot <- gather(df, "model", "p.value", 1:4)
ggplot(dfPlot, aes(x = p.value, fill = model, color = model)) + geom_density(alpha = 0.3)

ggplot(dfPlot, aes(x = -log10(p.value), fill = model, color = model)) + geom_density(alpha = 0.3)


df2 <- data.frame(codNI = unlist(survivalcoefs2NI), codNN = unlist(survivalcoefs2NN),
                 add = unlist(survivalAddCoefs2), over = unlist(survivalOverCoefs2),
                 cancer = rep(names(survivalAddCoefs2), lengths(survivalAddCoefs2)),
                 inv = names(survivalAddCoefs2[[1]]))
dfPlot2 <- gather(df2, "model", "p.value", 1:4)

ggplot(dfPlot2, aes(x = p.value, fill = model, color = model)) + geom_density(alpha = 0.3)

ggplot(dfPlot2, aes(x = -log10(p.value), fill = model, color = model)) + geom_density(alpha = 0.3)


## Explore plots
clinical <- colDataSurv$OV
invGenos <- TCGA_inversions$invX_006_female

clinical$inv <- invGenos[rownames(clinical)]
clinical <- clinical[!is.na(clinical$inv), ]

plot(survfit(Surv(clinical$new_time, clinical$new_event) ~ inv, clinical), col = 1:3)
legend("bottomleft", c("II", "NI", "NN"), 
       col=1:3, lty=1)


clinical <- colDataSurv$KICH
invGenos <- TCGA_inversions$inv16_017

clinical$inv <- invGenos[rownames(clinical)]
clinical <- clinical[complete.cases(clinical), ]
plot(survfit(Surv(clinical$new_time, clinical$new_event) ~ inv, clinical), col = 1:3)
legend("bottomleft", c("II", "NI", "NN"), 
       col=1:3, lty=1)

clinical <- colDataSurv$KIRP
invGenos <- TCGA_inversions$inv7_005

clinical$inv <- invGenos[rownames(clinical)] == "II"
clinical <- clinical[complete.cases(clinical), ]
plot(survfit(Surv(clinical$new_time, clinical$new_event) ~ inv, clinical), col = 1:3)
legend("bottomleft", c("II", "NI", "NN"), 
       col=1:3, lty=1)


KICH_clin$inv <- TCGA_inversions$inv16_017[rownames(KICH_clin)]
KICH_clin$invadd <-  0
KICH_clin$invadd[KICH_clin$inv == "NI"] <- 1
KICH_clin$invadd[KICH_clin$inv == "II"] <- 2

KICH_inv <- runCoxph("invadd", KICH_clin, c(genes, covars))
## inversion is still associated to recurrence so the genes are not mediating the association

### CESC - invX_006_female ####
load("CESC_MDS.RData")

## Select cancer samples
RNA <- mds[["RNAseq"]]
RNA$status <- ifelse(as.numeric(substring(colnames(RNA), 14, 15)) < 5, "cancer", "normal")
RNA <- RNA[, RNA$status == "cancer"]
colnames(RNA) <- RNA$patientID

genes <- subset(rnaAdditiveMeanInvGlobalSVA, inv == "invX_006_female" & tumor == "CESC")$feat

CESC_clin <- colDataSurv[["CESC"]]

## Select covars
covars <- colnames(CESC_clin)[-c(1:7)]
CESC_clin <- cbind(CESC_clin, data.frame(t(assay(RNA[genes, ])))[rownames(CESC_clin), , drop = FALSE])

CESC_genes <- runCoxph("ASTN2", df = CESC_clin, covars = covars)
# Not significant

CESC_clin$inv <- TCGA_inversions$invX_006_female[rownames(CESC_clin)]

CESC_inv <- runCoxph("inv", CESC_clin, c(genes, covars))
## inversion is still associated to recurrence so the genes are not mediating the association

save(KICH_inv, KICH_genes, CESC_inv, CESC_genes, file = "mediationRecurrenceExprs.Rdata")

