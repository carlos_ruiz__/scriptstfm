#'#################################################################################
#'#################################################################################
#' Preprocess SNPs files to impute in Michigan server
#'#################################################################################
#'#################################################################################

## Create array with compressed files containing matching between samples and arrays
array=()
while IFS=  read -r -d $'\0'; do
    array+=("$REPLY")
done < <(find . -type f -name '*.gz' -print0)

## Put mapping files in parent directory
for i in "${array[@]}"
do
 cp $i .
done

## Create VCF files from all arrays in parent folder
for d in */ 
do     
  filename=$(basename $d/*.birdseed.data.txt)
  name="${filename%.birdseed*}"
   ~/ANACONDA_VELL/bin/python2 ~/SequencingSoftware/birdseed2vcf/birdseed2vcf.py --birdseed \
   $d/$name.birdseed.data.txt --snp_annotation_file /Lacie_RRW10023/DATASETS/STUDY/TCGA/GenomeWideSNP_6.na33.annot.csv --fasta \
   /DATA/DATAW5/NGS/RefGenomes/hs37d5.fa --output_vcf $name.vcf --vcf_sample $name --array_sample $name
done



## Merge VCFs in one big VCF
for i in *.vcf
do
  awk '{ print $10}' $i > ./filteredChromosomes/filt$i
done


## Check that vcfs are equally ordered
for i in *.vcf
do
  sdiff -s <(cut -f3 ACOLD_p_TCGA_Batch17_SNP_N_GenomeWideSNP_6_A01_466074.vcf) <(cut -f3 $i) > ./filteredChromosomes/diff$i
done


## Run in filteredChromosomes folder
### Create lists of files
ls -1 filt*.vcf  | split -l 1000 -d - lists

## Paste files by batch
for list in lists*; do paste $(cat $list) > merge${list##lists}; done
paste merge* > merged.vcf

## Add position
cut -f1-9 YOWIE_p_TCGA_b71and106_N_GenomeWideSNP_6_F10_764074.vcf > ./filteredChromosomes/filtPositions.vcf

## Merged positions and genotypes
paste filtPositions.vcf merged.vcf > AllTCGASNPs.vcf

## Sort and index
vcf-sort  AllTCGASNPs.vcf  | bgzip -c > AllTCGASNPs.vcf.gz
tabix -p vcf AllTCGASNPs.vcf.gz



# Make file with whole mapping
#################################################
head -n 1 broad.mit.edu_ACC.Genome_Wide_SNP_6.sdrf.txt > allMapFiles.txt
tail -qn +1 broad.mit.edu_* >> allMapFiles.txt

# Get samples from control tissues (R)
map <- read.delim("allMapFiles.txt", header = T)

which(colnames(map) == "Derived.Array.Data.Matrix.File.1")

## Get data.frame with Barcode and Array name
minimap <-  map[, c(2, 31)]
colnames(minimap) <- c("Barcode", "Birdseed_filename")
minimap$ArrayId <- gsub(".birdseed.data.txt", "", minimap[, 2])
minimap$TissueCode <- substring(minimap$Barcode, 14, 15) 
minimap <- minimap[!minimap$TissueCode %in% c("", " B"), ]
minimap$Tissue <- ifelse(as.numeric(minimap$TissueCode) < 10, "cancer", "normal")

write.table(minimap, file = "allMapFilt.txt", quote = FALSE, row.names = FALSE, col.names = FALSE)
save(minimap, file = "mapping.Rdata")

write.table(minimap[minimap$Tissue == "cancer", "ArrayId"], file = "cancer.ids", quote = FALSE, row.names = FALSE, col.names = FALSE)
write.table(minimap[minimap$Tissue == "normal" & minimap$ArrayId != "DLP_REDO_FROM_MACON_C06", "ArrayId"], file = "normal.ids", quote = FALSE, row.names = FALSE, col.names = FALSE)
#################################################

## Create different vcf files
array=(1 2 3 6 7 8 11 12 14 16 17 21 X)
for i in "${array[@]}"
do
  vcftools --vcf AllTCGASNPs.vcf --chr $i --keep normal.ids --recode --recode-INFO-all -c | vcf-sort | bgzip -c > AllTCGA_chr$i.vcf.gz
done



## Correct Chr X
plink --vcf AllTCGA_chrX.vcf.gz --const-fid --out AllTCGA_chrX
plink --bfile AllTCGA_chrX --freq --out AllTCGA_chrX
perl ~/SequencingSoftware/HRC-1000G-check-bim.pl -b AllTCGA_chrX.bim -f AllTCGA_chrX.frq -r /SYNCRW10125/PROJECTS/cruizPhD/Imputation/HRC.r1-1.GRCh37.wgs.mac5.sites.tab -h 

plink --bfile AllTCGA_chrX --exclude Exclude-AllTCGA_chrX-HRC.txt --make-bed --impute-sex --out TEMP1
plink --bfile TEMP1 --update-map Chromosome-AllTCGA_chrX-HRC.txt --update-chr --make-bed --out TEMP2
plink --bfile TEMP2 --update-map Position-AllTCGA_chrX-HRC.txt --make-bed --out TEMP3
plink --bfile TEMP3 --flip Strand-Flip-AllTCGA_chrX-HRC.txt --make-bed --out TEMP4
plink --bfile TEMP4 --reference-allele Force-Allele1-AllTCGA_chrX-HRC.txt --set-hh-missing --chr 23 --chr-output M --recode vcf-iid bgz --out AllTCGA_chrX-updated
rm TEMP*

## Change annotation of imputated files (Directory with imputed files)
array=(1 2 3 6 7 8 11 12 14 16 17 21)
for i in "${array[@]}"
do
 zgrep "##" chr$i.dose.vcf.gz > header.hdr
 cat filter.txt >> header.hdr
  bcftools annotate --annotations  ../../Imputation/HRC.r1-1.GRCh37.wgs.mac5.sites.tab.gz --columns CHROM,POS,ID,REF,ALT \
  --output chr$i.dose.idsrs.vcf.gz --output-type z chr$i.dose.vcf.gz -h header.hdr
 tabix -p vcf chr$i.dose.idsrs.vcf.gz
done

array=(male female)
for i in "${array[@]}"
do
 zgrep "##" chrX.no.auto_$i.dose.vcf.gz > header.hdr
 cat filter.txt >> header.hdr
  bcftools annotate --annotations /SYNCRW10125/PROJECTS/cruizPhD/Imputation/HRC.r1-1.GRCh37.wgs.mac5.sites.tab.gz --columns CHROM,POS,ID,REF,ALT \
  --output chrX.no.auto_$i.dose.idsrs.vcf.gz --output-type z chrX.no.auto_$i.dose.vcf.gz -h header.hdr
 tabix -p vcf chrX.no.auto_$i.dose.idsrs.vcf.gz
done


### Filter chromosome 1 to breast cancer to run LDclassifier analysis
load("../../TCGA/colDataTumors.Rdata")
load("mapping.Rdata")

## Select breast samples
breast <- rownames(colDataList$BRCA)

## Create sample ID from barcode
minimap$BarcodeID <- substring(minimap$Barcode, 1, 12)

## Select normal samples
minimap <- subset(minimap, TissueCode == 10)
minimapBreast <- subset(minimap, BarcodeID %in% breast)

write.table(minimapBreast$ArrayId, quote = FALSE, row.names = FALSE, col.names = FALSE,
            file = "breast.txt")

vcftools --gzvcf AllTCGA_chr1.vcf.gz --keep breast.txt --recode --recode-INFO-all -c | vcf-sort | bgzip -c > AllTCGA_chr1_breast.vcf.gz 

