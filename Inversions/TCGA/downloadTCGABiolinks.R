# Download Gene Expression
library(TCGAbiolinks)
query.exp.h38 <- GDCquery(project = c("TCGA-COAD", "TCGA-READ"), 
                           data.category = "Transcriptome Profiling", 
                           data.type = "Gene Expression Quantification", 
                           workflow.type = "HTSeq - Counts")
GDCdownload(query.exp.h38)
RNA_colorect <- GDCprepare(query = query.exp.h38)
save(RNA_colorect, file = "RNA_colorect.Rdata")

# Download Methylation
query_met.hg38 <- GDCquery(project= c("TCGA-COAD", "TCGA-READ"), 
                           data.category = "DNA Methylation", 
                           platform = "Illumina Human Methylation 450")
GDCdownload(query_met.hg38)
Meth_colorect <- GDCprepare(query_met.hg38)
save(Meth_colorect, file = "Meth_colorect.Rdata")

# Download Clinical

projects <- list(Lung1 = "TCGA-LUAD", Lung2 = "TCGA-LUSC", Liver = "TCGA-LIHC", 
                 colorectal = c("TCGA-COAD","TCGA-READ"), Stomach = "TCGA-STAD", 
                 Breast = "TCGA-BRCA")

patientList <- lapply(projects, function(proj){
  query <- GDCquery(project = proj, 
                    data.category = "Clinical", 
                    file.type = "xml")
  GDCdownload(query)
  clinical <- GDCprepare_clinic(query, clinical.info = "patient")
})  

extraList <- lapply(projects, function(proj){
  query <- GDCquery(project = proj, 
                    data.category = "Clinical", 
                    file.type = "xml")
  follow <- GDCprepare_clinic(query, clinical.info = "follow_up")
  event <- GDCprepare_clinic(query, clinical.info = "new_tumor_event")
  list(follow, event)
})  

survList <- lapply(names(projects), function(proj){
  patdf <- patientList[[proj]]
  follow <- extraList[[proj]][[1]]
  res <- merge(patdf, follow, by = "bcr_patient_barcode")
})
names(survList) <- names(projects)

### Reduce data.frames
survList$Lung1 <- survList$Lung1[, c("bcr_patient_barcode", "days_to_birth", "gender", 
"stage_event_pathologic_stage", "vital_status.y", "days_to_last_followup.y", 
"days_to_death.y", "new_tumor_event_after_initial_treatment", "days_to_new_tumor_event_after_initial_treatment")]
                                         
survList$Lung2 <- survList$Lung2[, c("bcr_patient_barcode", "days_to_birth", "gender", 
                                     "stage_event_pathologic_stage", "vital_status.y", "days_to_last_followup.y", 
                                     "days_to_death.y", "new_tumor_event_after_initial_treatment", "days_to_new_tumor_event_after_initial_treatment")]

survList$Liver <- survList$Liver[, c("bcr_patient_barcode", "days_to_birth", "gender", 
                                     "stage_event_pathologic_stage", "vital_status.y", "days_to_last_followup.y", 
                                     "days_to_death.y", "new_tumor_events")]
survList$Liver <- merge(survList$Liver, extraList[[3]][[2]][, c("bcr_patient_barcode", "days_to_new_tumor_event_after_initial_treatment")], 
                        all.x = TRUE)


survList$colorectal <- survList$colorectal[, c("bcr_patient_barcode", "days_to_birth", "gender", 
                                               "stage_event_pathologic_stage", "vital_status.y", "days_to_last_followup.y", 
                                               "days_to_death.y", "new_tumor_events")]
survList$colorectal <- merge(survList$colorectal, extraList[["colorectal"]][[2]][, c("bcr_patient_barcode", "days_to_new_tumor_event_after_initial_treatment")],
                             all.x = TRUE)

survList$Stomach <- survList$Stomach[, c("bcr_patient_barcode", "days_to_birth", "gender", 
                                     "stage_event_pathologic_stage", "vital_status.y", "days_to_last_followup.y", 
                                     "days_to_death.y", "new_tumor_event_after_initial_treatment", "days_to_new_tumor_event_after_initial_treatment")]

survList$Breast <- survList$Breast[, c("bcr_patient_barcode", "days_to_birth", "gender", 
                                         "stage_event_pathologic_stage", "vital_status.y", "days_to_last_followup.y", 
                                         "days_to_death.y", "new_tumor_event_after_initial_treatment", "days_to_new_tumor_event_after_initial_treatment")]

### Homogeneize colnames
survList[] <- lapply(survList, function(clin){
  colnames(clin) <- c("patient", "age", "gender", "tum_stage", "death_event", "followUp_days", 
                    "death_days", "new_event", "new_tumor_days")
  
  ## Remove duplicated entries
  clin <- clin[!duplicated(clin), ]
  
  ## Select entries with longest follow_up & shortest new_tumor
  dfL <- lapply(unique(clin$patient), function(id){
    
    df <- clin[clin$patient == id, ]
    df$new_event <- as.character(df$new_event)
    df$death_event <- as.character(df$death_event)
    df[1, "followUp_days"] <- max(df$followUp_days, na.rm = TRUE)
    df[1, "new_tumor_days"] <- min(df$new_tumor_days, na.rm = TRUE)
    
    if (is.infinite(df[1, "new_tumor_days"])){
      df[1, "new_tumor_days"] <- NA
    }
    
    df[1, "death_days"] <- min(df$death_days, na.rm = TRUE)
    if (is.infinite(df[1, "death_days"])){
      df[1, "death_days"] <- NA
    }
    
    df[1, "death_event"] <- ifelse(!is.na(df[1, "death_days"]), "Dead", "Alive")
    df[1, "new_event"] <- ifelse(!is.na(df[1, "new_tumor_days"]), "YES", "NO")
    df[1, , drop = FALSE]
  })
  clin <- Reduce(rbind, dfL)
  
  
  clin$death_event <- clin$death_event == "Dead"
  clin$new_death <- ifelse(clin$death_event, clin$death_days, clin$followUp_days)
  
  clin$new_event <- clin$new_event == "YES"
  clin$new_time <- ifelse(clin$new_event, clin$new_tumor_days, 
                          pmax(clin$followUp_days, clin$death_days, na.rm = TRUE))
  
  ## Remove patients without prognosis data
  clin <- subset(clin, new_time != -Inf & new_death != -Inf )

  clin$age <- -clin$age/365
  
  clin$stage <- as.character(clin$tum_stage)
  clin$stage[clin$stage == ""] <- NA
  clin$stage[clin$stage %in% c("Stage IA", "Stage IB", "Stage IC")] <- "Stage I"
  clin$stage[clin$stage %in% c("Stage IIA", "Stage IIB", "Stage IIC")] <- "Stage II"
  clin$stage[clin$stage  %in% c("Stage IIIA", "Stage IIIB", "Stage IIIC")] <- "Stage III"
  clin$stage[clin$stage  %in% c("Stage IVA", "Stage IVB", "Stage X")] <- "Stage IV"
  
  rownames(clin) <- clin$patient
  clin
})
save(survList, file = "survivalBiolinks.Rdata")

