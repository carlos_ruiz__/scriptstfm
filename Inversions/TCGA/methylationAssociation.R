#'#################################################################################
#'#################################################################################
#' Run association of inversions against DNA methylation
#'#################################################################################
#'#################################################################################

# Load data and libraries ####
#'#################################################################################
library(MultiAssayExperiment)
library(RaggedExperiment)
library(GenomicRanges)
library(minfi)
library(IlluminaHumanMethylation450kanno.ilmn12.hg19)
library(MEAL)

# Load data and libraries ####
#'#################################################################################
load("badprobes.Rdata")


### Create list with all methylation datasets
maes <- dir("MAE/", full.names = TRUE)
tumors <- gsub(".Rdata", "", dir("MAE/"))

names(maes) <- tumors

getMeth <- function(file){
  message(file)
  load(file)
  
  maeFilt <- mae[, , grep("Methylation-20160128|Methylation_methyl450", names(mae))]
  methy <- assay(maeFilt[[1]])
  class(methy) <- "numeric"
  
  map <- sampleMap(maeFilt)
  rownames(map) <- map$colname
  colD <- colData(maeFilt)[map[colnames(methy), "primary"], ]
  rownames(colD) <- colnames(methy)
      
  methy <- makeGenomicRatioSetFromMatrix(methy, what = "B", pData = colD)
  methy <- dropMethylationLoci(methy)
  methy <- dropLociWithSnps(methy)
  methy <- methy[!rownames(methy) %in% crossProbes, ]
  rowData(methy) <-  IlluminaHumanMethylation450kanno.ilmn12.hg19::Other[featureNames(methy),]
  
  message("Process Succesful")
  methy
  
}

methylist <- lapply(maes, function(file)  
  tryCatch(getMeth(file), error = function(e) NULL))
save(methylist, file = "methyList.Rdata")


## Load inversions data
load("TCGAinversionClassificationGenos.Rdata")
load("TCGA_ancestry.Rdata")
load("InversionsGRanges.Rdata")

invs <- names(TCGA_inversions)
invs[c(3, 15)] <- "invX_006"
names(invs) <- names(TCGA_inversions)

## Remove bad inversions
invs <- invs[!invs %in% c("inv1_008", "inv21_005")]

seqlevels(ListRanges) <- paste0("chr", seqlevels(ListRanges))




## Create list with number of samples per inversion, tumor and cancer status ####
numSamples <- lapply(methylist, function(set) {
  set$status <- ifelse(as.numeric(substring(colnames(set), 14, 15)) < 10, "cancer", "normal")
  
  lapply(TCGA_inversions, function(inv){
    set$inv <- inv[set$patientID]
    table(set$inv, set$status)
  })
})
numSamples <- unlist(numSamples, recursive=FALSE)
selInterDatasets <- names(numSamples)[sapply(numSamples, function(x) ncol(x) == 2 & min(x) > 2)]


runMethy <- function(meth, invGenos, invRange, ...){
  meth$invpre <- invGenos[meth$patientID]
  
  meth <- meth[, !is.na(meth$invpre)]
  
  if (ncol(meth) < 10){
    return("Not enough samples to run the analysis")
  }
  
  meth$inv <- 0
  meth$inv[meth$invpre == "NI"] <- 1
  meth$inv[meth$invpre == "II"] <- 2
  
  meth$status <- ifelse(as.numeric(substring(colnames(meth), 14, 15)) < 10, "cancer", "normal")
  
  colData(meth) <- cbind(colData(meth) , ancestry[meth$patientID, ])
  
  covars <- c("PC1", "PC2", "PC3", "PC4")
  
  pvars <- colnames(colData(meth))
  
  if ("years_to_birth" %in% pvars){
    covars <- c(covars, "years_to_birth")
  }
  
  if ("gender" %in% pvars & !all(meth$gender == "female", na.rm = TRUE) & 
      !all(meth$gender == "male", na.rm = TRUE)){
    covars <- c(covars, "gender")
  }
  
  if("histological_type" %in% pvars){
    var <- meth$histological_type
    var <- var[!is.na(var)]
    if (length(unique(var)) > 1){
      covars <- c(covars, "histological_type")
    }
  }

  basic <- "~ inv +"
  if (!all(meth$status == "cancer")){
    basic <- "~ inv + status + inv:status +"
  }
  
  mod <- formula(paste(basic, paste(covars, collapse = " + ")))
  
  if (nrow(subsetByOverlaps(meth, invRange))){
    res <- runPipeline(meth, variable_names = "inv", model = mod,
                       range = invRange, region_methods = "none", num_vars = 2, verbose  = TRUE,...)
    
  }else {
    res <- runPipeline(meth, variable_names = "inv", model = mod,
                       region_methods = "none", num_vars = 2, verbose  = TRUE, ...)
  }
  
    
  res
}

## Sort list of methylation sets by size
methylist <- methylist[order(sapply(methylist, ncol), decreasing=FALSE)]

## Remove OV due to low sample size
methylist <- methylist[-1]

for (tumor in names(methylist)){
  
  message(tumor)
  
  ## Remove CpGs containing NAs
  meth <- methylist[[tumor]][rowSums(is.na(getBeta(methylist[[tumor]]))) == 0, ]
  
  methyAdditive <- mclapply(names(invs), function(inv) 
    tryCatch(runMethy(meth, TCGA_inversions[[inv]], 
             ListRanges[invs[inv]]), error = function(e) as.character(e))
  , mc.cores = 2)
  rm(meth)
  methylist <- methylist[names(methylist) != tumor]
  gc()
  names(methyAdditive) <- names(invs)
  save(methyAdditive, file = paste0("methyAdditive_", tumor, ".Rdata"))
}


# Process Results ####
## Function to get p.values filter CpGs in the inversion region
procRes <- function(RSet, invRange, coef = 2, rid = 1){
  res <- getProbeResults(RSet, rid = rid, coef = coef)
  GR <- GRanges(paste0(res$chromosome, ":", res$start, "-", res$start))
  names(GR) <- rownames(res)
  
  GR <- subsetByOverlaps(GR, invRange)
  res <- res[names(GR), ]
  res
}

methyAddFiles <- dir(pattern = "methyAdditive_.*Rdata")
names(methyAddFiles) <- gsub("methyAdditive_|.Rdata", "", methyAddFiles) 

## Inversion Effect - Mean change
sumList <- lapply(methyAddFiles, function(file){
  load(file)
  message(file)
  
  methyAdditive <- methyAdditive[sapply(methyAdditive, class) == "ResultSet"]
  filtRes <- lapply(names(methyAdditive), 
                    function(inv) 
    procRes(methyAdditive[[inv]], ListRanges[invs[inv]] + 2e5))
  names(filtRes) <- names(methyAdditive)
  filtRes
})

methyAdditiveMeanInv <- lapply(names(sumList[[1]]), function(inv){
  res <- lapply(sumList, function(x) x[[inv]][, c("logFC", "P.Value")])
  sumList <- sumList[!sapply(res, is.null)]
  res <- res[!sapply(res, is.null)]
  comCpgs <- Reduce(union, lapply(res, rownames))
  res <- Reduce(cbind, lapply(res, function(x) x[comCpgs, ]))
  colnames(res) <- paste(rep(names(sumList), each = 2), c("coef" ,"P.Value"), sep = "_")
  res
})
names(methyAdditiveMeanInv) <- names(sumList[[1]])
save(methyAdditiveMeanInv, file = "methyAdditiveMeanInv.Rdata")

## Interaction Inv-cancer Effect - Mean change
sumList <- lapply(methyAddFiles, function(file){
  load(file)
  print(file)
  methyAdditive <- methyAdditive[sapply(methyAdditive, class) == "ResultSet"]
  coef <- lapply(methyAdditive, function(x) which(colnames(x@results$DiffMean$result$coefficient) == "inv:statusnormal"))
  coef <- coef[lengths(coef) > 0]
  filtRes <- lapply(names(coef), 
                    function(inv) 
                      procRes(methyAdditive[[inv]], ListRanges[invs[inv]] + 2e5, coef = coef[[inv]]))
  names(filtRes) <- names(coef)
  filtRes
})

methyAdditiveMeanInter <- lapply(names(sumList[[2]]), function(inv){
  res <- lapply(sumList, function(x) x[[inv]][, c("logFC", "P.Value")])
  sumList <- sumList[!sapply(res, is.null)]
  res <- res[!sapply(res, is.null)]
  comCpgs <- Reduce(union, lapply(res, rownames))
  res <- Reduce(cbind, lapply(res, function(x) x[comCpgs, ]))
  colnames(res) <- paste(rep(names(sumList), each = 2), c("coef" ,"P.Value"), sep = "_")
  res
})
names(methyAdditiveMeanInter) <- names(sumList[[2]])
save(methyAdditiveMeanInter, file = "methyAdditiveMeanInter.Rdata")



## Inversion Effect - Variance change
sumList <- lapply(methyAddFiles, function(file){
  load(file)
  
  methyAdditive <- methyAdditive[sapply(methyAdditive, class) == "ResultSet"]
  filtRes <- lapply(names(methyAdditive), 
                    function(inv) 
                      procRes(methyAdditive[[inv]], ListRanges[invs[inv]] + 2e5, rid = 2))
  names(filtRes) <- names(methyAdditive)
  filtRes
})

methyAdditiveVarInv <- lapply(names(sumList[[1]]), function(inv){
  res <- lapply(sumList2, function(x) x[[inv]][, c("DiffLevene", "P.Value")])
  sumList <- sumList2[!sapply(res, is.null)]
  res <- res[!sapply(res, is.null)]
  comCpgs <- Reduce(union, lapply(res, rownames))
  res <- Reduce(cbind, lapply(res, function(x) x[comCpgs, ]))
  colnames(res) <- paste(rep(names(sumList), each = 2), c("coef" ,"P.Value"), sep = "_")
  res
})
names(methyAdditiveVarInv) <- names(sumList[[1]])
save(methyAdditiveVarInv, file = "methyAdditiveVarInv.Rdata")


plotVolcano <- function(df, thres, ...){
  pvals <- -log10(unlist(df[, -seq(1, length(colnames(df)), 2)]))
  eff <- unlist(df[seq(1, length(colnames(df)), 2)])
  tums <- gsub("_coef", "", colnames(df)[seq(1, length(colnames(df)), 2)])
  nams <- paste(rownames(df), rep(tums, each = nrow(df)))
  
  plot(eff, pvals, ...)
  abline(h = thres)
  
  sel <- pvals > thres
  selCpgs <- nams[sel]
  selCpgs <- selCpgs[!is.na(selCpgs)]
  selCpgs <- sapply(strsplit(selCpgs, " "), `[`, 1)
  
  if(length(selCpgs)){
    
    sel <- grepl(paste(selCpgs, collapse = "|"), nams)
    
    points(eff[sel], pvals[sel], col = "green", pch = 16)
    text(eff[sel], pvals[sel], label = nams[sel])
  }
}


# Export Results
## Create excels
methyAdditiveMeanInvFilt <- methyAdditiveMeanInv[sapply(methyAdditiveMeanInv, nrow) > 0]
methyAdditiveMeanInterFilt <- methyAdditiveMeanInter[sapply(methyAdditiveMeanInter, nrow) > 0]
methyAdditiveVarInvFilt <- methyAdditiveVarInv[sapply(methyAdditiveVarInv, nrow) > 0]

lapply(names(methyAdditiveMeanInvFilt), function(x) 
  write.csv(methyAdditiveMeanInvFilt[[x]], file = paste0("methyAdditiveMeanInv_", x, ".csv")))

lapply(names(methyAdditiveMeanInterFilt), function(x) 
  write.csv(methyAdditiveMeanInterFilt[[x]], file = paste0("methyAdditiveMeanInter_", x, ".csv")))

lapply(names(methyAdditiveVarInvFilt), function(x) 
  write.csv(methyAdditiveVarInvFilt[[x]], file = paste0("methyAdditiveVarInv_", x, ".csv")))

## Make plots
pdf("methyAdditiveMeanInvVolcanos.pdf")
lapply(names(methyAdditiveMeanInvFilt), function(x) 
  plotVolcano(methyAdditiveMeanInvFilt[[x]], thres = -log10(1e-5), main = x))
dev.off()

pdf("methyAdditiveMeanInterVolcanos.pdf")
lapply(names(methyAdditiveMeanInterFilt), function(x) 
  plotVolcano(methyAdditiveMeanInterFilt[[x]], thres = -log10(1e-5), main = x))
dev.off()

pdf("methyAdditiveVarInvVolcanos.pdf")
lapply(names(methyAdditiveVarInvFilt), function(x) 
  plotVolcano(methyAdditiveVarInvFilt[[x]], thres = -log10(1e-5), main = x))
dev.off()


# Global effect of inversions ####
### Get results
methyAdditiveMeanInvGlobal <- lapply(names(methyAddFiles), function(tumor){
  
  message(tumor)
  load(methyAddFiles[tumor])
  
  methyAdditive <- methyAdditive[sapply(methyAdditive, class) == "ResultSet"]
  filtRes <- lapply(names(methyAdditive), function(inv) {
    res <- getProbeResults(methyAdditive[[inv]], 
                           fNames = c("chromosome", "start", "UCSC_RefGene_Name", "UCSC_RefGene_Group"))
    res$tumor <- tumor
    res$inv <- inv
    res$feat <- rownames(res)
    
    res <- res[res$adj.P.Val < 0.05, ]
    res
  })
  Reduce(rbind, filtRes)
})
methyAdditiveMeanInvGlobal <- Reduce(rbind, methyAdditiveMeanInvGlobal)
save(methyAdditiveMeanInvGlobal, file = "methyAdditiveMeanInvGlobal.Rdata")
write.csv(methyAdditiveMeanInvGlobal, file = "methyAdditiveMeanInvGlobal.csv")


## Interaction Inv-cancer Effect - Mean change
methyAdditiveMeanInterGlobal <- lapply(names(methyAddFiles), function(tumor){
  message(tumor)
  load(methyAddFiles[tumor])
  
  methyAdditive <- methyAdditive[sapply(methyAdditive, class) == "ResultSet"]

    filtRes <- lapply(names(methyAdditive), function(inv) {
    res <- getProbeResults(methyAdditive[[inv]], 
                           coef = which(colnames(methyAdditive[[inv]]@results$DiffMean$result$coefficient) == "inv:statusnormal"),
                           fNames = c("chromosome", "start", "UCSC_RefGene_Name", "UCSC_RefGene_Group"))
    
    res <- res[!is.na(res$P.Value), ]
    if (nrow(res) > 0){
      res$tumor <- tumor
      res$inv <- inv
      res$feat <- rownames(res)
      
      res <- res[res$adj.P.Val < 0.05, ]
    }
    
    res
  })
  Reduce(rbind, filtRes)
})
methyAdditiveMeanInterGlobal <- Reduce(rbind, methyAdditiveMeanInterGlobal)
save(methyAdditiveMeanInterGlobal, file = "methyAdditiveMeanInterGlobal.Rdata")

methyAdditiveMeanInterGlobal$dataset <- paste(methyAdditiveMeanInterGlobal$tumor, methyAdditiveMeanInterGlobal$inv, sep = ".")
methyAdditiveMeanInterGlobal <- methyAdditiveMeanInterGlobal[methyAdditiveMeanInterGlobal$dataset %in% selInterDatasets, ]

write.csv(methyAdditiveMeanInterGlobal, file = "methyAdditiveMeanInterGlobal.csv")

## Inversion Effect - Variance change
methyAdditiveVarInvGlobal <- lapply(names(methyAddFiles), function(tumor){
  message(tumor)
  load(methyAddFiles[tumor])
  
  methyAdditive <- methyAdditive[sapply(methyAdditive, class) == "ResultSet"]
  filtRes <- lapply(names(methyAdditive), function(inv) {
    res <- getProbeResults(methyAdditive[[inv]], rid = 2,
                           fNames = c("chromosome", "start", "UCSC_RefGene_Name", "UCSC_RefGene_Group"))
    res <- res[!is.na(res$P.Value), ]
    if (nrow(res) > 0){
      res$tumor <- tumor
      res$inv <- inv
      res$feat <- rownames(res)
      
      res <- res[res$Adj.P.Val < 0.05, ]
    }
    
    res
  })
  Reduce(rbind, filtRes)
})
methyAdditiveVarInvGlobal <- Reduce(rbind, methyAdditiveVarInvGlobal)
write.csv(methyAdditiveVarInvGlobal, file = "methyAdditiveVarInvGlobal.csv")
save(methyAdditiveVarInvGlobal, file = "methyAdditiveVarInvGlobal.Rdata")

# Run SVA analysis ####
for (tumor in names(methylist)){
  
  message(tumor)
  
  ## Remove CpGs containing NAs
  meth <- methylist[[tumor]][rowSums(is.na(getBeta(methylist[[tumor]]))) == 0, ]
  
  methyAdditive <- mclapply(names(invs), function(inv) 
    tryCatch(runMethy(meth, TCGA_inversions[[inv]], 
                      ListRanges[invs[inv]], sva = TRUE), error = function(e) as.character(e))
    , mc.cores = 3)
  # rm(meth)
  # methylist <- methylist[names(methylist) != tumor]
  # gc()
  names(methyAdditive) <- names(invs)
  save(methyAdditive, file = paste0("methyAdditiveSVA_", tumor, ".Rdata"))
}


methyAddFilesSVA <- dir(pattern = "methyAdditiveSVA_.*Rdata")
names(methyAddFilesSVA) <- gsub("methyAdditiveSVA_|.Rdata", "", methyAddFilesSVA) 

# Local Effect ####
## Inversion Effect - Mean change
sumList <- lapply(methyAddFilesSVA, function(file){
  load(file)
  message(file)
  
  methyAdditive <- methyAdditive[sapply(methyAdditive, class) == "ResultSet"]
  filtRes <- lapply(names(methyAdditive), 
                    function(inv) 
                      procRes(methyAdditive[[inv]], ListRanges[invs[inv]] + 2e5))
  names(filtRes) <- names(methyAdditive)
  filtRes
})

methyAdditiveMeanInvSVA <- lapply(names(sumList[[1]]), function(inv){
  res <- lapply(sumList, function(x) x[[inv]][, c("logFC", "P.Value")])
  sumList <- sumList[!sapply(res, is.null)]
  res <- res[!sapply(res, is.null)]
  comCpgs <- Reduce(union, lapply(res, rownames))
  res <- Reduce(cbind, lapply(res, function(x) x[comCpgs, ]))
  colnames(res) <- paste(rep(names(sumList), each = 2), c("coef" ,"P.Value"), sep = "_")
  res
})
names(methyAdditiveMeanInvSVA) <- names(sumList[[1]])
save(methyAdditiveMeanInvSVA, file = "methyAdditiveMeanInvSVA.Rdata")

## Interaction Inv-cancer Effect - Mean change
sumList <- lapply(methyAddFilesSVA, function(file){
  load(file)
  print(file)
  methyAdditive <- methyAdditive[sapply(methyAdditive, class) == "ResultSet"]
  coef <- lapply(methyAdditive, function(x) which(colnames(x@results$DiffMean$result$coefficient) == "inv:statusnormal"))
  coef <- coef[lengths(coef) > 0]
  filtRes <- lapply(names(coef), 
                    function(inv) 
                      procRes(methyAdditive[[inv]], ListRanges[invs[inv]] + 2e5, coef = coef[[inv]]))
  names(filtRes) <- names(coef)
  filtRes
})

methyAdditiveMeanInterSVA <- lapply(names(sumList[[2]]), function(inv){
  res <- lapply(sumList, function(x) x[[inv]][, c("logFC", "P.Value")])
  sumList <- sumList[!sapply(res, is.null)]
  res <- res[!sapply(res, is.null)]
  comCpgs <- Reduce(union, lapply(res, rownames))
  res <- Reduce(cbind, lapply(res, function(x) x[comCpgs, ]))
  colnames(res) <- paste(rep(names(sumList), each = 2), c("coef" ,"P.Value"), sep = "_")
  res
})
names(methyAdditiveMeanInterSVA) <- names(sumList[[2]])
save(methyAdditiveMeanInterSVA, file = "methyAdditiveMeanInterSVA.Rdata")



## Inversion Effect - Variance change
sumList <- lapply(methyAddFilesSVA, function(file){
  load(file)
  
  methyAdditive <- methyAdditive[sapply(methyAdditive, class) == "ResultSet"]
  filtRes <- lapply(names(methyAdditive), 
                    function(inv) 
                      procRes(methyAdditive[[inv]], ListRanges[invs[inv]] + 2e5, rid = 2))
  names(filtRes) <- names(methyAdditive)
  filtRes
})

methyAdditiveVarInvSVA <- lapply(names(sumList[[1]]), function(inv){
  res <- lapply(sumList, function(x) x[[inv]][, c("DiffLevene", "P.Value")])
  sumList <- sumList[!sapply(res, is.null)]
  res <- res[!sapply(res, is.null)]
  comCpgs <- Reduce(union, lapply(res, rownames))
  res <- Reduce(cbind, lapply(res, function(x) x[comCpgs, ]))
  colnames(res) <- paste(rep(names(sumList), each = 2), c("coef" ,"P.Value"), sep = "_")
  res
})
names(methyAdditiveVarInvSVA) <- names(sumList[[1]])
save(methyAdditiveVarInvSVA, file = "methyAdditiveVarInvSVA.Rdata")



# Export Results
## Create excels
methyAdditiveMeanInvFiltSVA <- methyAdditiveMeanInvSVA[sapply(methyAdditiveMeanInvSVA, nrow) > 0]
methyAdditiveMeanInterFiltSVA <- methyAdditiveMeanInterSVA[sapply(methyAdditiveMeanInterSVA, nrow) > 0]
methyAdditiveVarInvFiltSVA <- methyAdditiveVarInvSVA[sapply(methyAdditiveVarInvSVA, nrow) > 0]

lapply(names(methyAdditiveMeanInvFiltSVA), function(x) 
  write.csv(methyAdditiveMeanInvFiltSVA[[x]], file = paste0("methyAdditiveMeanInvSVA_", x, ".csv")))

lapply(names(methyAdditiveMeanInterFiltSVA), function(x) 
  write.csv(methyAdditiveMeanInterFiltSVA[[x]], file = paste0("methyAdditiveMeanInterSVA_", x, ".csv")))

lapply(names(methyAdditiveVarInvFiltSVA), function(x) 
  write.csv(methyAdditiveVarInvFiltSVA[[x]], file = paste0("methyAdditiveVarInvSVA_", x, ".csv")))

## Make plots
pdf("methyAdditiveMeanInvVolcanosSVA.pdf")
lapply(names(methyAdditiveMeanInvFiltSVA), function(x) 
  plotVolcano(methyAdditiveMeanInvFiltSVA[[x]], thres = -log10(1e-5), main = x))
dev.off()

pdf("methyAdditiveMeanInterVolcanosSVA.pdf")
lapply(names(methyAdditiveMeanInterFiltSVA), function(x) 
  plotVolcano(methyAdditiveMeanInterFiltSVA[[x]], thres = -log10(1e-5), main = x))
dev.off()

pdf("methyAdditiveVarInvVolcanosSVA.pdf")
lapply(names(methyAdditiveVarInvFiltSVA), function(x) 
  plotVolcano(methyAdditiveVarInvFiltSVA[[x]], thres = -log10(1e-5), main = x))
dev.off()


# Global effect of inversions ####
### Get results
methyAdditiveMeanInvGlobalSVA <- lapply(names(methyAddFilesSVA), function(tumor){
  
  message(tumor)
  load(methyAddFilesSVA[tumor])
  
  methyAdditive <- methyAdditive[sapply(methyAdditive, class) == "ResultSet"]
  filtRes <- lapply(names(methyAdditive), function(inv) {
    res <- getProbeResults(methyAdditive[[inv]], 
                           fNames = c("chromosome", "start", "UCSC_RefGene_Name", "UCSC_RefGene_Group"))
    res$tumor <- tumor
    res$inv <- inv
    res$feat <- rownames(res)
    
    res <- res[res$adj.P.Val < 0.05, ]
    res
  })
  Reduce(rbind, filtRes)
})
methyAdditiveMeanInvGlobalSVA <- Reduce(rbind, methyAdditiveMeanInvGlobalSVA)
save(methyAdditiveMeanInvGlobalSVA, file = "methyAdditiveMeanInvGlobalSVA.Rdata")
write.csv(methyAdditiveMeanInvGlobalSVA, file = "methyAdditiveMeanInvGlobalSVA.csv")


## Interaction Inv-cancer Effect - Mean change
methyAdditiveMeanInterGlobalSVA <- lapply(names(methyAddFilesSVA), function(tumor){
  message(tumor)
  load(methyAddFilesSVA[tumor])
  
  methyAdditive <- methyAdditive[sapply(methyAdditive, class) == "ResultSet"]
  
  filtRes <- lapply(names(methyAdditive), function(inv) {
    res <- getProbeResults(methyAdditive[[inv]], 
                           coef = which(colnames(methyAdditive[[inv]]@results$DiffMean$result$coefficient) == "inv:statusnormal"),
                           fNames = c("chromosome", "start", "UCSC_RefGene_Name", "UCSC_RefGene_Group"))
    
    res <- res[!is.na(res$P.Value), ]
    if (nrow(res) > 0){
      res$tumor <- tumor
      res$inv <- inv
      res$feat <- rownames(res)
      
      res <- res[res$adj.P.Val < 0.05, ]
    }
    
    res
  })
  Reduce(rbind, filtRes)
})
methyAdditiveMeanInterGlobalSVA <- Reduce(rbind, methyAdditiveMeanInterGlobalSVA)
save(methyAdditiveMeanInterGlobalSVA, file = "methyAdditiveMeanInterGlobalSVA.Rdata")

methyAdditiveMeanInterGlobalSVA$dataset <- paste(methyAdditiveMeanInterGlobalSVA$tumor, methyAdditiveMeanInterGlobalSVA$inv, sep = ".")
methyAdditiveMeanInterGlobalSVA <- methyAdditiveMeanInterGlobalSVA[methyAdditiveMeanInterGlobalSVA$dataset %in% selInterDatasets, ]

save(methyAdditiveMeanInterGlobalSVA, file = "methyAdditiveMeanInterGlobalSVAFilt.Rdata")
write.csv(methyAdditiveMeanInterGlobalSVA, file = "methyAdditiveMeanInterGlobalSVA.csv")

## Inversion Effect - Variance change
methyAdditiveVarInvGlobalSVA <- lapply(names(methyAddFilesSVA), function(tumor){
  message(tumor)
  load(methyAddFilesSVA[tumor])
  
  methyAdditive <- methyAdditive[sapply(methyAdditive, class) == "ResultSet"]
  filtRes <- lapply(names(methyAdditive), function(inv) {
    res <- getProbeResults(methyAdditive[[inv]], rid = 2,
                           fNames = c("chromosome", "start", "UCSC_RefGene_Name", "UCSC_RefGene_Group"))
    res <- res[!is.na(res$P.Value), ]
    if (nrow(res) > 0){
      res$tumor <- tumor
      res$inv <- inv
      res$feat <- rownames(res)
      
      res <- res[res$Adj.P.Val < 0.05, ]
    }
    
    res
  })
  Reduce(rbind, filtRes)
})
methyAdditiveVarInvGlobalSVA <- Reduce(rbind, methyAdditiveVarInvGlobalSVA)
write.csv(methyAdditiveVarInvGlobalSVA, file = "methyAdditiveVarInvGlobalSVA.csv")
save(methyAdditiveVarInvGlobalSVA, file = "methyAdditiveVarInvGlobalSVA.Rdata")

# Extract RDA results ####
extractRDA <- function(RSet){
  if ("RDA" %in% names(RSet)){
    RSet@results <- RSet@results["RDA"]
    return(RSet)
  } else{
    return(NULL)
  }
}

methyAddFiles <- dir(pattern = "methyAdditive_.*Rdata")
names(methyAddFiles) <- gsub("methyAdditive_|.Rdata", "", methyAddFiles) 

methyRDA <- lapply(methyAddFiles, function(file){
  load(file)
  message(file)
  
  methyAdditive <- methyAdditive[sapply(methyAdditive, class) == "ResultSet"]
  filtList <- lapply(methyAdditive, extractRDA)
  filtList <- filtList[!sapply(filtList, is.null)]
  filtList
})
## Remove fDatas
methyRDA <- lapply(methyRDA, lapply, function(x) {
  x@fData <- data.frame()
  x
})
save(methyRDA, file = "methyRDA.Rdata")


  
methyAddFilesSVA <- dir(pattern = "methyAdditiveSVA_.*Rdata")
names(methyAddFilesSVA) <- gsub("methyAdditiveSVA_|.Rdata", "", methyAddFilesSVA) 

methyRDASVA <- lapply(methyAddFilesSVA, function(file){
  load(file)
  message(file)
  
  methyAdditive <- methyAdditive[sapply(methyAdditive, class) == "ResultSet"]
  filtList <- lapply(methyAdditive, extractRDA)
  filtList <- filtList[!sapply(filtList, is.null)]
  filtList
})
methyRDASVA <- lapply(methyRDASVA, lapply, function(x) {
  x@fData <- data.frame()
  x
})
save(methyRDASVA, file = "methyRDASVA.Rdata")
