#'#################################################################################
#'#################################################################################
#' Change assembly of SNPs and impute in Michigan server
#'#################################################################################
#'#################################################################################

#'#################################################################################
# AGP ####
#'#################################################################################
# Convert SNPs to bed format
~/SequencingSoftware/plink --bfile SubjectGenotypeFile.AGP.phs000267.v1.p1.20120806 --recode --out AGPsnps

# Change X chromosome name 
sed 's/^23/X/' AGPsnps.map  > AGPsnps18.map 
cp AGPsnps.ped AGPsnps18.ped

# Run liftOver
python2 ~/SequencingSoftware/liftOverPlink/liftOverPlink.py -m AGPsnps18.map -o lifted -c hg18ToHg19.over.chain.gz -e ~/SequencingSoftware/liftOver
cut -f 4 lifted.bed.unlifted | sed "/^#/d" > to_exclude.dat 
~/SequencingSoftware/plink --file AGPsnps18 --recode --out cot --exclude to_exclude.dat 
~/SequencingSoftware/plink --ped cot.ped --map lifted.map --make-bed --out AGPsnps19

## Make list of bad Samples
tail -n +2 /home/cruiz/InversionSequencing/SandersROIs/SNPsClassification/OldPlots/README_qc > badSamplesAGP.txt

### R code
R330

badSamps <- read.table("badSamplesAGP.txt")
fam <- read.table("AGPsnps19filt.fam")
removeFam <-fam[fam$V2 %in% badSamps$V1, 1:2]
write.table(removeFam, file = "removeSamplesAGP.txt", col.names = F, row.names = F, quote = F)
####

~/SequencingSoftware/plink --bfile AGPsnps19 --remove removeSamplesAGP.txt --chr 7,8,17,23 --make-bed --out AGPsnps19sel

# Check file prior imputation
~/SequencingSoftware/plink --bfile AGPsnps19sel --freq --out AGPsnps19sel
perl ~/SequencingSoftware/HRC-1000G-check-bim.pl -b AGPsnps19sel.bim -f AGPsnps19sel.frq -r ../HRC.r1-1.GRCh37.wgs.mac5.sites.tab -h 
chmod 766 Run-plink.sh 
## Edit Run-plink.sh to select only good chromosomes
./Run-plink.sh

# Split VCF in chromosomes
array=( 7 8 17 23)
for i in "${array[@]}"
do
  plink --bfile AGPsnps19sel-updated --chr $i --chr-output M --set-hh-missing --recode vcf-iid --out AGPsnps19sel-chr$i
  ~/SequencingSoftware/vcftools_0.1.13/bin/vcf-sort AGPsnps19sel-chr$i.vcf | /home/cruiz/SequencingSoftware/htslib-1.3.1/bgzip -c > AGPsnps19sel_chr$i.recode.vcf.gz
done

## Change annotation of imputated files (Directory with imputed files)
array=( 7 8 17 23)
for i in "${array[@]}"
do
 zgrep "##" chr$i.dose.vcf.gz > header.hdr
 cat filter.txt >> header.hdr
~/SequencingSoftware/bcftools/bcftools annotate --annotations ../HRC.r1-1.GRCh37.wgs.mac5.sites.tab.gz --columns CHROM,POS,ID,REF,ALT \
  --output chr$i.dose.idsrs.vcf.gz --output-type z chr$i.dose.vcf.gz -h header.hdr
 tabix -p vcf chr$i.dose.idsrs.vcf.gz
done


#'#################################################################################
# SSC 1Mv1 ####
#'#################################################################################
# Convert SNPs to bed format
plink --bfile  /SYNCRW10125/PROJECTS/cruizPhD/PLINK/1Mv1/SSC_1Mv1_binary --recode --out SSC1Mv1

# Change X chromosome name 
sed 's/^23/X/' SSC1Mv1.map  > SSC1Mv1_18.map 
cp SSC1Mv1.ped SSC1Mv1_18.ped

# Run liftOver
python2 ~/SequencingSoftware/liftOverPlink/liftOverPlink.py -m SSC1Mv1_18.map -o lifted -c ../../hg18ToHg19.over.chain.gz -e ~/SequencingSoftware/liftOver
cut -f 4 lifted.bed.unlifted | sed "/^#/d" > to_exclude.dat 
plink --file SSC1Mv1_18 --recode --out cot --exclude to_exclude.dat 
plink --ped cot.ped --map lifted.map --make-bed --out SSC1Mv1_19

plink --bfile SSC1Mv1_19 --chr 7,8,17,23 --make-bed --out SSC1Mv1_19sel

# Check file prior imputation
plink --bfile SSC1Mv1_19sel --freq --out SSC1Mv1_19sel
perl ~/SequencingSoftware/HRC-1000G-check-bim.pl -b SSC1Mv1_19sel.bim -f SSC1Mv1_19sel.frq -r ../../HRC.r1-1.GRCh37.wgs.mac5.sites.tab -h 
chmod 766 Run-plink.sh 
## Edit Run-plink.sh to select only good chromosomes
./Run-plink.sh

# Split VCF in chromosomes
array=( 7 8 17 23)
for i in "${array[@]}"
do
  plink --bfile SSC1Mv1_19sel-updated --chr $i --chr-output M --set-hh-missing --recode vcf-iid --out SSC1Mv1_19sel-chr$i
  ~/SequencingSoftware/vcftools_0.1.13/bin/vcf-sort SSC1Mv1_19sel-chr$i.vcf | /home/cruiz/SequencingSoftware/htslib-1.3.1/bgzip -c > SSC1Mv1_19sel_chr$i.recode.vcf.gz
done

#'#################################################################################
# SSC 1Mv3 ####
#'#################################################################################
# Convert SNPs to bed format
plink --bfile  /SYNCRW10125/PROJECTS/cruizPhD/PLINK/1Mv3/SSC_1Mv3_binary --recode --out SSC1Mv3

# Change X chromosome name 
sed 's/^23/X/' SSC1Mv3.map  > SSC1Mv3_18.map 
cp SSC1Mv3.ped SSC1Mv3_18.ped

# Run liftOver
python2 ~/SequencingSoftware/liftOverPlink/liftOverPlink.py -m SSC1Mv3_18.map -o lifted -c ../../hg18ToHg19.over.chain.gz -e ~/SequencingSoftware/liftOver
cut -f 4 lifted.bed.unlifted | sed "/^#/d" > to_exclude.dat 
plink --file SSC1Mv3_18 --recode --out cot --exclude to_exclude.dat 
plink --ped cot.ped --map lifted.map --make-bed --out SSC1Mv3_19

plink --bfile SSC1Mv3_19 --chr 7,8,17,23 --make-bed --out SSC1Mv3_19sel

# Check file prior imputation
plink --bfile SSC1Mv3_19sel --freq --out SSC1Mv3_19sel
perl ~/SequencingSoftware/HRC-1000G-check-bim.pl -b SSC1Mv3_19sel.bim -f SSC1Mv3_19sel.frq -r ../../HRC.r1-1.GRCh37.wgs.mac5.sites.tab -h 
chmod 766 Run-plink.sh 
## Edit Run-plink.sh to select only good chromosomes
./Run-plink.sh

# Split VCF in chromosomes
array=( 7 8 17 23)
for i in "${array[@]}"
do
  plink --bfile SSC1Mv3_19sel-updated --chr $i --chr-output M --set-hh-missing --recode vcf-iid --out SSC1Mv3_19sel-chr$i
  ~/SequencingSoftware/vcftools_0.1.13/bin/vcf-sort SSC1Mv3_19sel-chr$i.vcf | /home/cruiz/SequencingSoftware/htslib-1.3.1/bgzip -c > SSC1Mv3_19sel_chr$i.recode.vcf.gz
done


#'#################################################################################
# SSC Omni ####
#'#################################################################################
# Convert SNPs to bed format
plink --bfile  /SYNCRW10125/PROJECTS/cruizPhD/PLINK/Omni2.5/SSC_Omni2.5_binary --recode --out SSCOmni

# Change X chromosome name 
sed 's/^23/X/' SSCOmni.map  > SSCOmni_18.map 
cp SSCOmni.ped SSCOmni_18.ped

# Run liftOver
python2 ~/SequencingSoftware/liftOverPlink/liftOverPlink.py -m SSCOmni_18.map -o lifted -c ../../hg18ToHg19.over.chain.gz -e ~/SequencingSoftware/liftOver
cut -f 4 lifted.bed.unlifted | sed "/^#/d" > to_exclude.dat 
plink --file SSCOmni_18 --recode --out cot --exclude to_exclude.dat 
plink --ped cot.ped --allow-extra-chr  --map lifted.map --make-bed --out SSCOmni_19

plink --bfile SSCOmni_19 --allow-extra-chr --chr 7,8,17,23 --make-bed --out SSCOmni_19sel

# Check file prior imputation
plink --bfile SSCOmni_19sel --freq --out SSCOmni_19sel
perl ~/SequencingSoftware/HRC-1000G-check-bim.pl -b SSCOmni_19sel.bim -f SSCOmni_19sel.frq -r ../../HRC.r1-1.GRCh37.wgs.mac5.sites.tab -h 
chmod 766 Run-plink.sh 
## Edit Run-plink.sh to select only good chromosomes
./Run-plink.sh

# Split VCF in chromosomes
array=( 7 8 17 23)
for i in "${array[@]}"
do
  plink --bfile SSCOmni_19sel-updated --chr $i --chr-output M --set-hh-missing --recode vcf-iid --out SSCOmni_19sel-chr$i
  ~/SequencingSoftware/vcftools_0.1.13/bin/vcf-sort SSCOmni_19sel-chr$i.vcf | /home/cruiz/SequencingSoftware/htslib-1.3.1/bgzip -c > SSCOmni_19sel_chr$i.recode.vcf.gz
done


#'#################################################################################
# 1000 Genomes 
#'#################################################################################

## Select these SNPs from 1000 Genomes
## Select European individual in 1000 Genomes
### R code
R330
load("/SYNCRW10125/DATASETS/STUDY/1000GENOME/Samples_Pop1GK.Rdata")
EUR <- rownames(samp_pop)[samp_pop$superpop == "EUR"]
write.table(EUR, file = "EUR.txt", col.names = F, row.names = F, quote = F)
###

array=( 7 8 17 23)
for i in "${array[@]}"
do
  grep ^$i ../AGP/AGPsnps19sel-updated.bim | cut -f2 > snps.txt
  ~/SequencingSoftware/vcftools_0.1.13/bin/vcftools --gzvcf /SYNCRW10125/DATASETS/STUDY/1000GENOME/VCF/ALL.chr$i.phase3_shapeit2_mvncall_integrated_v5a.20130502.genotypes.vcf.gz --snps snps.txt --keep EUR.txt --recode --recode-INFO-all --out 1000Genomes_chr$i
  bgzip -c 1000Genomes_chr$i.recode.vcf > 1000Genomes_chr$i.recode.vcf.gz
done

## Recode chromosome X
grep ^23 ../AGP/AGPsnps19sel-updated.bim | cut -f2 > snps.txt
~/SequencingSoftware/vcftools_0.1.13/bin/vcftools --gzvcf /SYNCRW10125/DATASETS/STUDY/1000GENOME/VCF/ALL.chrX.phase3_shapeit2_mvncall_integrated_v1b.20130502.genotypes.vcf.gz --snps snps.txt --keep EUR.txt --recode --recode-INFO-all --out 1000Genomes_chrX
bgzip -c 1000Genomes_chrX.recode.vcf > 1000Genomes_chrX.recode.vcf.gz

## Change annotation of imputated files (Directory with imputed files)
array=( 7 8 17 23)
for i in "${array[@]}"
do
 zgrep "##" chr$i.dose.vcf.gz > header.hdr
 cat filter.txt >> header.hdr
~/SequencingSoftware/bcftools/bcftools annotate --annotations ../HRC.r1-1.GRCh37.wgs.mac5.sites.tab.gz --columns CHROM,POS,ID,REF,ALT \
  --output chr$i.dose.idsrs.vcf.gz --output-type z chr$i.dose.vcf.gz -h header.hdr
 tabix -p vcf chr$i.dose.idsrs.vcf.gz
done
