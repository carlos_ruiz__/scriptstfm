##################################################################################
##################################################################################
#' Get inversion classification from imputed data
##################################################################################
##################################################################################


##################################################################################
#' scoreInvHap ####
##################################################################################
# Load packages ####
#'#################################################################################
library(scoreInvHap)
library(VariantAnnotation)


## 1000 Genomes


# Define function ####
##################################################################################
getInvImputed <- function(){
  files <- c(paste0("chr", c(7, 8 , 17, "female"), ".dose.idsrs.vcf.gz"))
  invs <- c("HsInv0286", "ROIno.8.3", "ROIno.17.16", "HsInv0396")
  res <- lapply(1:length(files), function(i){
    print(invs[i])
    vcf <- readVcf(files[i], "hg19")
    inv <- scoreInvHap(vcf, SNPsR2 = SNPsR2[[invs[i]]], 
                       Refs = Refs[[invs[i]]], 
                       hetRefs = hetRefs[[invs[i]]], 
                       BPPARAM = BiocParallel::MulticoreParam(1),
                       imputed = TRUE, verbose = TRUE)
    inv
  })
  names(res) <- c("HsInv0286", "ROIno.8.3", "ROIno.17.16", "HsInv0396_f")
  res
}
imp1000Genomes <- getInvImputed()
save(imp1000Genomes, file = "1000GenomesImp.Rdata")




## AGP

# Define Function ####
#'#################################################################################
getInvImputed <- function(){
  files <- c(paste0("chr", c(7, 17, "female", "male"), ".dose.idsrs.vcf.gz"))
  invs <- c("HsInv0286", "ROIno.17.16", "HsInv0396", "HsInv0396")
  res <- lapply(1:length(files), function(i){
    print(invs[i])
    vcf <- readVcf(files[i], "hg19")
    inv <- scoreInvHap(vcf, SNPsR2 = SNPsR2[[invs[i]]], 
                              Refs = Refs[[invs[i]]], 
                              hetRefs = hetRefs[[invs[i]]], 
                              BPPARAM = BiocParallel::MulticoreParam(1),
                              imputed = TRUE, verbose = TRUE)
    inv
  })
  names(res) <- c("HsInv0286", "ROIno.17.16", "HsInv0396_f", "HsInv0396_m")
  res
}
gr <- GRanges("8:8055789-11980649")
vcf <- readVcf("chr8.dose.idsrs.vcf.gz", "hg19", ScanVcfParam(which = gr))
inv <- scoreInvHap(vcf, SNPsR2 = SNPsR2$ROIno.8.3, 
                          Refs = Refs$ROIno.8.3,  
                          hetRefs = hetRefs$ROIno.8.3, 
                          BPPARAM = BiocParallel::MulticoreParam(1),
                          imputed = TRUE, verbose = TRUE)

impAGP <- getInvImputed()
impAGP <- c(impAGP, ROIno.8.3 = inv)

save(impAGP, file = "AGPImp.Rdata")


# Define function ####
##################################################################################
getInvImputed <- function(){
  files <- c(paste0("chr", c(7, 8 , 17, "female", "male"), ".dose.idsrs.vcf.gz"))
  invs <- c("HsInv0286", "ROIno.8.3", "ROIno.17.16", "HsInv0396", "HsInv0396")
  res <- lapply(1:length(files), function(i){
    print(invs[i])
    vcf <- readVcf(files[i], "hg19")
    inv <- scoreInvHap(vcf, SNPsR2 = SNPsR2[[invs[i]]], 
                              Refs = Refs[[invs[i]]], 
                              hetRefs = hetRefs[[invs[i]]], 
                              BPPARAM = BiocParallel::MulticoreParam(1),
                              imputed = TRUE, verbose = TRUE)
    inv
  })
  names(res) <- c("HsInv0286", "ROIno.8.3", "ROIno.17.16", "HsInv0396_f", "HsInv0396_m")
  res
}

## SSC ####
#'#################################################################################

## 1Mv1
imp1Mv1 <- getInvImputed()
save(imp1Mv1, file = "SSC1Mv1Imp.Rdata")


## 1Mv3
imp1Mv3 <- getInvImputed()
save(imp1Mv3, file = "SSC1Mv3Imp.Rdata")

## Omni
impOmni <- getInvImputed()
save(impOmni, file = "OmniImp.Rdata")

#'#################################################################################
#' invClust / PFIDO ####
#'#################################################################################
library(VariantAnnotation)
library(GenomicRanges)
library(snpStats)
library(invClust)
source("/home/cruiz/InversionSequencing/InversionNGSutils.R")
source("/home/cruiz/InversionSequencing/PFIDO_module.r")

## Data.frame with population data
load("/home/cruiz/InversionSequencing/SandersROIs/SNPsClassification/OldPlots/popStructureSeldin.Rdata")

rownames(popStructureSeldin) <- popStructureSeldin$sampid


# Load VCFs ####
#'#################################################################################
grs <- GRanges(c("8:8055789-11980649", "17:43661775-44372665"))
names(grs) <- c("ROIno.8.3", "ROIno.17.16")

roi <- as.data.frame(grs)[, 1:3]
names(roi) <- c("chr", "LBP", "RBP")
roi$reg <- 1:2

files <- c(paste0("chr", c(8, 17), ".dose.idsrs.vcf.gz"))
invs <- c("ROIno.8.3", "ROIno.17.16")
genos <- lapply(1:length(files), function(i){
  print(invs[i])
  vcf <- readVcf(files[i], "hg19", ScanVcfParam(which = grs[invs[i]]))
  dupSNPs <- rownames(vcf)[duplicated(rownames(vcf))]
  vcf <- vcf[!rownames(vcf) %in% dupSNPs, popStructureSeldin[colnames(vcf), "EUR"]]
  vcf <- vcf[info(vcf)$R2 > 0.4, ]
  res <- genotypeToSnpMatrix(vcf)
  res$map$position <- start(rowRanges(vcf))
  res$map$chromosome <- as.character(seqnames(rowRanges(vcf)))
  res
})  
names(genos) <- invs

## 1000 Genomes ####
#'##############################################################################
genos <- lapply(1:length(files), function(i){
  print(invs[i])
  vcf <- readVcf(files[i], "hg19", ScanVcfParam(which = grs[invs[i]]))
  dupSNPs <- rownames(vcf)[duplicated(rownames(vcf))]
  vcf <- vcf[!rownames(vcf) %in% dupSNPs, ]
  vcf <- vcf[info(vcf)$R2 > 0.4, ]
  res <- genotypeToSnpMatrix(vcf)
  res$map$position <- start(rowRanges(vcf))
  res$map$chromosome <- as.character(seqnames(rowRanges(vcf)))
  res
})  
names(genos) <- invs

imp1000GenomesPFIDO <- lapply(genos, function(x) {
  tryCatch(PFIDO(x$genotypes, file.type = 0, GFORCE = TRUE), 
           error=function(x) return(NA))
})
names(imp1000GenomesPFIDO) <- invs
save(imp1000GenomesPFIDO, file = "1000GenomesImpPFIDO.Rdata")

i <- 0
imp1000GenomesinvClust <- lapply(genos, function(x) {
  i <<- i + 1
  tryCatch(invClust(roi = roi, wh = i, geno = x$genotypes, 
                    annot = x$map, dim = 2, tol = 1e-5), 
           error=function(x) return(NA))
})
names(imp1000GenomesinvClust) <- invs
save(imp1000GenomesinvClust, file = "1000GenomesImpinvClust.Rdata")



## AGP ####
#'##############################################################################
i <- 0
impAGPinvClust <- lapply(genos, function(x) {
  i <<- i + 1
  tryCatch(invClust(roi = roi, wh = i, geno = x$genotypes, 
                    annot = x$map, dim = 2, tol = 1e-5), 
           error=function(x) return(NA))
})
names(impAGPinvClust) <- invs
save(impAGPinvClust, file = "AGPImpinvClust.Rdata")



## Mv1 ####
#'##############################################################################
imp1Mv1PFIDO <- lapply(genos, function(x) {
  tryCatch(PFIDO(x$genotypes, file.type = 0, GFORCE = TRUE), 
           error=function(x) return(NA))
})
names(imp1Mv1PFIDO) <- invs
save(imp1Mv1PFIDO, file = "SSC1Mv1ImpPFIDO.Rdata")

i <- 0
imp1Mv1invClust <- lapply(genos, function(x) {
  i <<- i + 1
  tryCatch(invClust(roi = roi, wh = i, geno = x$genotypes, 
                    annot = x$map, dim = 2, tol = 1e-5), 
           error=function(x) return(NA))
})
names(imp1Mv1invClust) <- invs
save(imp1Mv1invClust, file = "SSC1Mv1ImpinvClust.Rdata")


## Mv3 ####
#'##############################################################################
imp1Mv3PFIDO <- lapply(genos, function(x) {
  tryCatch(PFIDO(x$genotypes, file.type = 0, GFORCE = TRUE), 
           error=function(x) return(NA))
})
names(imp1Mv3PFIDO) <- invs
save(imp1Mv3PFIDO, file = "SSC1Mv3ImpPFIDO.Rdata")

i <- 0
imp1Mv3invClust <- lapply(genos, function(x) {
  i <<- i + 1
  tryCatch(invClust(roi = roi, wh = i, geno = x$genotypes, 
                    annot = x$map, dim = 2, tol = 1e-5), 
           error=function(x) return(NA))
})
names(imp1Mv3invClust) <- invs
save(imp1Mv3invClust, file = "SSC1Mv3ImpinvClust.Rdata")


## Omni ####
#'##############################################################################
impOmniPFIDO <- lapply(genos, function(x) {
  tryCatch(PFIDO(x$genotypes, file.type = 0, GFORCE = TRUE), 
           error=function(x) return(NA))
})
names(impOmniPFIDO) <- invs
save(impOmniPFIDO, file = "SSCOmniImpPFIDO.Rdata")

i <- 0
impOmniinvClust <- lapply(genos, function(x) {
  i <<- i + 1
  tryCatch(invClust(roi = roi, wh = i, geno = x$genotypes, 
                    annot = x$map, dim = 2, tol = 1e-5), 
           error=function(x) return(NA))
})
names(impOmniinvClust) <- invs
save(impOmniinvClust, file = "SSCOmniImpinvClust.Rdata")

##################################################################################
##################################################################################
#' Get inversion classification from 1000 Genomes using AGP SNPs ####
##################################################################################
##################################################################################

library(snpfier)
library(VariantAnnotation)

##################################################################################
#' SNPfier ####
##################################################################################
# Define function ####
##################################################################################
getInvSNPfier <- function(){
  files <- c(paste0("1000Genomes_chr", c(7, 8 , 17, "X"), ".recode.vcf.gz"))
  invs <- c("HsInv0286", "ROIno.8.3", "ROIno.17.16", "HsInv0396")
  r2 <- c(0, 0.4, 0.4, 0)
  names(r2) <- c("HsInv0286", "ROIno.8.3", "ROIno.17.16", "HsInv0396")
  res <- lapply(1:length(files), function(i){
    print(invs[i])
    vcf <- readVcf(files[i], "hg19")
    geno(vcf)$GT <- sapply(1:ncol(geno(vcf)$GT), function(x) {
      col <-  geno(vcf)$GT[, x]
      if(nchar(col[1]) == 1){
        col <- paste0(col, "|", col)
      }
      col
    })
    inv <- classifierPipeline(vcf, SNPsR2 = SNPsR2[[invs[i]]], 
                              Refs = Refs[[invs[i]]], 
                              hetRefs = hetRefs[[invs[i]]], 
                              R2 = r2[invs[i]], mc.cores = 5,
                              imputed = FALSE, verbose = TRUE)
    inv
  })
  names(res) <- c("HsInv0286", "ROIno.8.3", "ROIno.17.16", "HsInv0396")
  res
}

filt1000Genomessnpfier <- getInvSNPfier()
save(filt1000Genomessnpfier, file = "filt1000Genomessnpfier.Rdata")

#'#################################################################################
#' invClust / PFIDO ####
#'#################################################################################
library(VariantAnnotation)
library(GenomicRanges)
library(snpStats)
library(invClust)
source("/home/cruiz/InversionSequencing/InversionNGSutils.R")
source("/home/cruiz/InversionSequencing/PFIDO_module.r")

# Load VCFs ####
#'#################################################################################
grs <- GRanges(c("8:8055789-11980649", "17:43661775-44372665"))
names(grs) <- c("ROIno.8.3", "ROIno.17.16")

roi <- as.data.frame(grs)[, 1:3]
names(roi) <- c("chr", "LBP", "RBP")
roi$reg <- 1:2

files <- c(paste0("1000Genomes_chr", c(8, 17), ".recode.vcf.gz"))
invs <- c("ROIno.8.3", "ROIno.17.16")
genos <- lapply(1:length(files), function(i){
  print(invs[i])
  vcf <- readVcf(files[i], "hg19", ScanVcfParam(which = grs[invs[i]]))
  dupSNPs <- rownames(vcf)[duplicated(rownames(vcf))]
  vcf <- vcf[!rownames(vcf) %in% dupSNPs, ]
  res <- genotypeToSnpMatrix(vcf)
  res$map$position <- start(rowRanges(vcf))
  res$map$chromosome <- as.character(seqnames(rowRanges(vcf)))
  res
})  
names(genos) <- invs

# Calling  ####
filt1000GenomesPFIDO <- lapply(genos, function(x) {
  tryCatch(PFIDO(x$genotypes, file.type = 0, GFORCE = TRUE), 
           error=function(x) return(NA))
})
names(filt1000GenomesPFIDO) <- invs
save(filt1000GenomesPFIDO, file = "filt1000GenomesPFIDO.Rdata")

i <- 0
filt1000GenomesinvClust <- lapply(genos, function(x) {
  i <<- i + 1
  tryCatch(invClust(roi = roi, wh = i, geno = x$genotypes, 
                    annot = x$map, dim = 2, tol = 1e-5), 
           error=function(x) return(NA))
})
names(filt1000GenomesinvClust) <- invs
save(filt1000GenomesinvClust, file = "filt1000GenomesinvClust.Rdata")


