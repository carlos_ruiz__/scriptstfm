#'#################################################################################
#'#################################################################################
#' Check Hardy-Weinberg Equilibrium in complex inversions
#' Run in PC
#'#################################################################################
#'#################################################################################

setwd("./Inversions/SNPfierTesting/")

## Load data
load("invFESTinvClustcomplex.Rdata")
sampsData <- read.delim("1000GenomesSamplesData.txt")
rownames(sampsData) <- sampsData$Sample.name

## Define function
haplo2Inv <- function(haplos){
  regmatches(levels(haplos), gregexpr("Na", levels(haplos)), invert = FALSE) <- "N"
  regmatches(levels(haplos), gregexpr("Nb", levels(haplos)), invert = FALSE) <- "N"
  regmatches(levels(haplos), gregexpr("Nc", levels(haplos)), invert = FALSE) <- "N"
  regmatches(levels(haplos), gregexpr("Ia", levels(haplos)), invert = FALSE) <- "I"
  regmatches(levels(haplos), gregexpr("Ib", levels(haplos)), invert = FALSE) <- "I"
  regmatches(levels(haplos), gregexpr("Ic", levels(haplos)), invert = FALSE) <- "I"
  haplos
}


# HsInv0286 ####
## Inversion status
inv7_inv <- haplo2Inv(factor(invFestGenotypescomplex$HsInv0286))
inv7_inv <- inv7_inv[!is.na(inv7_inv)]
n <- length(inv7_inv)

props <- c((sum(inv7_inv == "II")*2 + sum(inv7_inv == "NI"))/(n*2), 
            (sum(inv7_inv == "NN")*2 + sum(inv7_inv == "NI"))/(n*2))

chisq.test(table(inv7_inv), p = c(props[1]^2, props[1]*props[2]*2, props[2]^2))
# Chi-squared test for given probabilities
# 
# data:  table(inv7_inv)
# X-squared = 0.64779, df = 2, p-value = 0.7233

## Haplotype status
inv7_haplo <- invFestGenotypescomplex$HsInv0286
inv7_haplo <- inv7_haplo[!is.na(inv7_haplo)]
n <- length(inv7_haplo)

props <- c(Ia = (sum(inv7_haplo == "IaIa")*2 + sum(inv7_haplo %in% c("IaIb", "NaIa", "NbIa")))/(n*2), 
           Ib = (sum(inv7_haplo == "IbIb")*2 + sum(inv7_haplo %in% c("IaIb", "NaIb", "NbIb")))/(n*2), 
           Na = (sum(inv7_haplo == "NaNa")*2 + sum(inv7_haplo %in% c("NaIa", "NaIb", "NaNb")))/(n*2), 
           Nb = (sum(inv7_haplo == "NbNb")*2 + sum(inv7_haplo %in% c("NbIa", "NbIb", "NaNb")))/(n*2)) 

chisq.test(table(inv7_haplo), 
           p = c(props[1]^2, props[1]*props[2]*2, props[2]^2, props[3]*props[1]*2, 
                 props[3]*props[2]*2, props[3]^2, props[3]*props[4]*2, 
                 props[4]*props[1]*2, props[4]*props[2]*2, props[4]^2))
# Chi-squared test for given probabilities
# 
# data:  table(inv7_haplo)
# X-squared = 4.8137, df = 9, p-value = 0.8502

# HsInv0396 ####

invX_haplo <- invFestGenotypescomplex$HsInv0396

# Select only females
invX_haplo <- invX_haplo[sampsData[names(invX_haplo), "Sex"] == "female"]


## Inversion status
invX_inv <- haplo2Inv(factor(invX_haplo))
invX_inv <- invX_inv[!is.na(invX_inv)]
n <- length(invX_inv)

props <- c((sum(invX_inv == "II")*2 + sum(invX_inv == "NI"))/(n*2), 
           (sum(invX_inv == "NN")*2 + sum(invX_inv == "NI"))/(n*2))

chisq.test(table(invX_inv), p = c(props[1]^2, props[1]*props[2]*2, props[2]^2))
# Chi-squared test for given probabilities
# 
# data:  table(invX_inv)
# X-squared = 0.026697, df = 2, p-value = 0.9867

## Haplotype status
invX_haplo <- invX_haplo[!is.na(invX_haplo)]
n <- length(invX_haplo)

props <- c(I = (sum(invX_haplo == "II")*2 + sum(invX_haplo %in% c("NaI", "NbI", "NcI")))/(n*2), 
           Na = (sum(invX_haplo == "NaNa")*2 + sum(invX_haplo %in% c("NaI", "NaNb", "NaNc")))/(n*2), 
           Nb = (sum(invX_haplo == "NbNb")*2 + sum(invX_haplo %in% c("NbI", "NbNc", "NaNb")))/(n*2),
           Nc = (sum(invX_haplo == "NcNc")*2 + sum(invX_haplo %in% c("NcI", "NaNc", "NbNc")))/(n*2))

chisq.test(table(invX_haplo), 
           p = c(props[1]^2, props[1]*props[2]*2, props[2]^2, props[3]*props[2]*2, 
                 props[4]*props[2]*2, props[3]*props[1]*2, props[3]^2, 
                 props[4]*props[3]*2, props[4]*props[1]*2, props[4]^2))
# Chi-squared test for given probabilities
# 
# data:  table(invX_haplo)
# X-squared = 10.816, df = 9, p-value = 0.2885

