#'#################################################################################
#'#################################################################################
#' Post processing of imputed files
#'#################################################################################
#'#################################################################################

## Run in all imputation folders
## Filter regions
### chr7
~/SequencingSoftware/vcftools_0.1.13/bin/vcftools --chr 7 --gzvcf chr7.dose.vcf.gz --recode --recode-INFO-all --out chr7.dose.filt --from-bp 54190974 --to-bp 54486821
### chr8
~/SequencingSoftware/vcftools_0.1.13/bin/vcftools --chr 8 --gzvcf chr8.dose.vcf.gz --recode --recode-INFO-all --out chr8.dose.filt --from-bp 7955789 --to-bp 12080649
### chr17
~/SequencingSoftware/vcftools_0.1.13/bin/vcftools --chr 17 --gzvcf chr17.dose.vcf.gz --recode --recode-INFO-all --out chr17.dose.filt --from-bp 43561775 --to-bp 44472665
### chrX
~/SequencingSoftware/vcftools_0.1.13/bin/vcftools --chr X --gzvcf chrX.no.auto_female.dose.vcf.gz --recode --recode-INFO-all --out chrX.female.dose.filt --from-bp 72115927 --to-bp 72406774
~/SequencingSoftware/vcftools_0.1.13/bin/vcftools --chr X --gzvcf chrX.no.auto_male.dose.vcf.gz --recode --recode-INFO-all --out chrX.male.dose.filt --from-bp 72115927 --to-bp 72406774


## Change annotation of imputated files (Directory with imputed files)
array=(7 8 17)
for i in "${array[@]}"
do
grep "##" chr$i.dose.filt.recode.vcf > header.hdr
cat filter.txt >> header.hdr
~/SequencingSoftware/bcftools/bcftools annotate --annotations ../../../HRC.r1-1.GRCh37.wgs.mac5.sites.tab.gz --columns CHROM,POS,ID,REF,ALT \
--output chr$i.dose.idsrs.vcf.gz --output-type z chr$i.dose.filt.recode.vcf -h header.hdr
~/SequencingSoftware/htslib-1.3.1/tabix -p vcf chr$i.dose.idsrs.vcf.gz
done

## Chr X
array=(male female)
for i in "${array[@]}"
do
grep "##" chrX.$i.dose.filt.recode.vcf > header.hdr
cat filter.txt >> header.hdr
~/SequencingSoftware/bcftools/bcftools annotate --annotations ../../../HRC.r1-1.GRCh37.wgs.mac5.sites.tab.gz --columns CHROM,POS,ID,REF,ALT \
--output chr$i.dose.idsrs.vcf.gz --output-type z chrX.$i.dose.filt.recode.vcf -h header.hdr
~/SequencingSoftware/htslib-1.3.1/tabix -p vcf chr$i.dose.idsrs.vcf.gz
done


#'#################################################################################
# AGP ####
#'#################################################################################
