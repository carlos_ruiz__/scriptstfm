# Get Dataset Ancestry
# Run in /DATA/DATAW5/Carlos/Inversions/UK10KAnalysis

# UK10K ASD datasets
## Create Symbolic links to vcf.gz
ln -s ../NEURO_ASD_FI/_EGAZ00001014929_UK10K_NEURO_ASD_FI.REL-2013-04-20.filt.vcf.gz NEURO_ASD_FI.vcf.gz

## Get list of SNPs from all the VCF
for ii in /SYNCRW10125/DATASETS/STUDY/EGA/UK10K/ASD_studies/*.vcf.gz
do 
  echo $ii
  gzip -cdf $ii | cut -f3 | tail -n +179 | sort -u > ${ii}_SNPlist
done
mv /SYNCRW10125/DATASETS/STUDY/EGA/UK10K/ASD_studies/*SNPlist .

## Get the intersection of all SNPs
cat NEURO_ASD_BIONED.vcf_SNPlist > SNPlist_final
i=0
for ii in *SNPlist
do 
  i=$((i+1))
  sort $ii SNPlist_final | uniq -d > cot${i}
  cat cot${i} > SNPlist_final
done
rm cot*

# Control datasets (NBD and BBC)
## Run plink to convert tped to plink format 
for ii in *.tfam
do 
  echo $ii
  file=${ii%%.*}
  plink --noweb --tfile $file --make-bed \
--missing-genotype N --out $file
done

# Get list SNPs BBC 
for ii in /SYNCRW10125/DATASETS/STUDY/EGA/BBC/*.bim
do 
  echo $ii
  cut -f2 $ii |  grep -o "rs[0-9]*" >> BBC_SNPlist
done

# Get list SNPs NBD
for ii in /SYNCRW10125/DATASETS/STUDY/EGA/NBD/*.bim
do 
  echo $ii
  cut -f2 $ii |  grep -o "rs[0-9]*" >> NBD_SNPlist
done

# Intersection All SNPs
sort BBC_SNPlist SNPlist_final | uniq -d > cot
sort cot NBD_SNPlist | uniq -d > commonSNPs


# Filter ASD VCFs
for ii in ~/data/PublicData/STUDY/EGA/UK10K/ASD_studies/*.vcf.gz
do 
  cot=`basename $ii`
  name=${cot%%.*}
  echo $name
  vcftools --gzvcf $ii --recode --recode-INFO-all --snps commonSNPs --stdout | bgzip -c > $name.vcf.gz
done

for ii in *.vcf.gz
do 
 tabix -p vcf $ii
done

# Merge VCFs
vcf-merge `ls *.vcf.gz` > ASD.vcf

# Convert to plink
plink --vcf ASD.vcf --make-bed --out ASD

## Do not include Finish datasets
vcf-merge `ls NEURO*.vcf.gz | grep -v "FI\|TAMPERE"` > ASDUK.vcf 
plink --vcf ASDUK.vcf --make-bed --out ASDUK


# Filter SCH VCFs (Manually remove finnish)
for ii in ~/data/PublicData/STUDY/EGA/UK10K/SCHIZOPHRENIA_studies/*.vcf.gz
do 
  cot=`basename $ii`
  name=${cot%%.*}
  echo $name
  vcftools --gzvcf $ii --recode --recode-INFO-all --snps commonSNPs --stdout | bgzip -c > $name.vcf.gz
done

for ii in *.vcf.gz
do 
 tabix -p vcf $ii
done

# Merge VCFs
vcf-merge `ls *.vcf.gz` > SCH.vcf

# Convert to plink
plink --vcf SCH.vcf --make-bed --out SCH


# Merge bed
for ii in *.fam
do
  name=${ii%%.*}
  echo $name.bed $name.bim $name.fam >> allfiles.txt
done

# Filter BEDs
## Merge BEDs
plink --merge-list allfiles.txt --make-bed --out BBCmerged

## Change SNPs names to match those in VCFs
cp BBCmerged.bim BBCmergedcopy.bim
cut -f2 BBCmergedcopy.bim | grep -o "^[a-Z0-9]*" > test2
paste BBCmergedcopy.bim test2 | awk -v OFS='\t' '{print $1,$7,$3,$4,$5,$6}' > BBCmerged.bim

## Filter SNPs
~/SequencingSoftware/plink --bfile BBCmerged --extract /DATA/DATAW5/Carlos/Inversions/UK10KAnalysis/commonSNPs --make-bed --out BBCfiltered

# NBD
## Merge BEDs
~/SequencingSoftware/plink --noweb  --merge-list allfiles.txt --make-bed --out NBDmerged

cp NBDmerged.bim NBDmergedcopy.bim
cut -f2 NBDmergedcopy.bim | grep -o "^[a-Z0-9]*" > goodNames
paste NBDmergedcopy.bim goodNames | awk -v OFS='\t' '{print $1,$7,$3,$4,$5,$6}' > NBDmerged.bim

## Filter SNPs
~/SequencingSoftware/plink --bfile NBDmerged --extract /DATA/DATAW5/Carlos/Inversions/UK10KAnalysis/commonSNPs --make-bed --out NBDfiltered


# Merge datasets
## Merge BED datasets
plink --bfile NBDfiltered --bmerge BBCfiltered --out controlMerged

# Merge ASD and control datasets
plink --bfile ASD --bmerge controlMerged --make-bed --out allMerged
#plink --bfile ASD --bmerge NBDfiltered --make-bed --out allMerged
## UK
plink --bfile ASDUK --bmerge controlMerged --make-bed --out UKMerged

## Flip SNPs to ensure that they are in the same strand
plink --bfile ASD --flip allMerged-merge.missnp --make-bed --allow-no-sex --out ASDFlipped
plink --bfile ASDFlipped --bmerge controlMerged --make-bed --out allMerged
#plink --bfile ASDFlipped --bmerge NBDfiltered --make-bed --out allMerged

## UK
plink --bfile ASDUK --flip UKMerged-merge.missnp --make-bed --allow-no-sex --out ASDUKFlipped
plink --bfile ASDUKFlipped --bmerge controlMerged --make-bed --out UKMerged


## Remove discordant SNPs
plink --bfile ASDFlipped --exclude allMerged-merge.missnp --make-bed --out ASDFlippedFilt
plink --bfile controlMerged --exclude allMerged-merge.missnp --make-bed --out controlMergedFilt
#plink --bfile NBDfiltered --exclude allMerged-merge.missnp --make-bed --out NBDfilteredFilt

## UK
plink --bfile ASDUKFlipped --exclude UKMerged-merge.missnp --make-bed --out ASDUKFlippedFilt

## Final merge
plink --bfile ASDFlippedFilt --allow-no-sex --bmerge controlMergedFilt --make-bed --out allMerged
#plink --bfile ASDFlippedFilt --allow-no-sex --bmerge NBDfilteredFilt --make-bed --out allMerged
## UK
plink --bfile ASDUKFlippedFilt --allow-no-sex --bmerge controlMergedFilt --make-bed --out UKMerged

## Run peddy
plink --bfile allMerged --recode vcf-iid bgz --out allMerged
tabix -p vcf allMerged.vcf.gz

python -m peddy --prefix UK10K allMerged.vcf.gz allMerged.fam

## UK
plink --bfile UKMerged --recode vcf-iid bgz --out UKMerged
tabix -p vcf UKMerged.vcf.gz

python -m peddy --prefix UK10K_UK UKMerged.vcf.gz UKMerged.fam



# Merge SCH and control datasets 
plink --bfile SCH --bmerge controlMerged --make-bed --out allSCHMerged

## Flip SNPs to ensure that they are in the same strand
plink --bfile SCH --flip allSCHMerged-merge.missnp --make-bed --allow-no-sex --out SCHFlipped
plink --bfile SCHFlipped --bmerge controlMerged --make-bed --out allSCHMerged

## Remove discordant SNPs
plink --bfile SCHFlipped --exclude allSCHMerged-merge.missnp --make-bed --out SCHFlippedFilt
plink --bfile controlMerged --exclude allSCHMerged-merge.missnp --make-bed --out controlMergedSCHFilt

## Final merge
plink --bfile SCHFlippedFilt --allow-no-sex --bmerge controlMergedSCHFilt --make-bed --out SCHMerged

## Run peddy
plink --bfile SCHMerged --recode vcf-iid bgz --out SCHMerged
tabix -p vcf SCHMerged.vcf.gz

python -m peddy --prefix UK10KSCH SCHMerged.vcf.gz SCHMerged.fam

## UK
plink --bfile UKMerged --recode vcf-iid bgz --out UKMerged
tabix -p vcf UKMerged.vcf.gz

python -m peddy --prefix UK10K_UK UKMerged.vcf.gz UKMerged.fam


## A partir de aquí es viejo
plink --bfile allMerged --allow-no-sex --flip-scan --flip-scan-verbose
cat plink.flipscan | tr -s " " > plink.flipscan.mod


## Repeat flipping
~/SequencingSoftware/plink --noweb --bfile ASD --bmerge controlMerged --make-bed --out allMerged

cat allMerged-merge.missnp >> flipSNPs.list
~/SequencingSoftware/plink --noweb --bfile ASD --flip flipSNPs.list --make-bed --allow-no-sex --out ASDFlipped
~/SequencingSoftware/plink --noweb --bfile ASDFlipped --bmerge controlMerged --make-bed --out allMerged


## Remove discordant SNPs
~/SequencingSoftware/plink --bfile ASDFlipped --exclude allMerged-merge.missnp --make-bed --out ASDFlippedFilt
~/SequencingSoftware/plink --bfile controlMerged --exclude allMerged-merge.missnp --make-bed --out controlMergedFilt

## Final merge
~/SequencingSoftware/plink --bfile ASDFlippedFilt --allow-no-sex --bmerge controlMergedFilt --make-bed --out allMerged

~/SequencingSoftware/plink --bfile allMerged --allow-no-sex --flip-scan --flip-scan-verbose --out flip2
cat flip2.flipscan | tr -s " " > flip2.flipscan.mod



## Repeat flipping (2nd round)
~/SequencingSoftware/plink --noweb --bfile ASD --bmerge controlMerged --make-bed --out allMerged

cat flipSNPs.list >> flipSNPs2.list
cat allMerged-merge.missnp >> flipSNPs2.list
sort -u flipSNPs2.list > flipSNPs.list

~/SequencingSoftware/plink --noweb --bfile ASD --flip flipSNPs.list --make-bed --allow-no-sex --out ASDFlipped
~/SequencingSoftware/plink --noweb --bfile ASDFlipped --bmerge controlMerged --make-bed --out allMerged


## Remove discordant SNPs
~/SequencingSoftware/plink --bfile ASDFlipped --exclude allMerged-merge.missnp --make-bed --out ASDFlippedFilt
~/SequencingSoftware/plink --bfile controlMerged --exclude allMerged-merge.missnp --make-bed --out controlMergedFilt

## Final merge
~/SequencingSoftware/plink --bfile ASDFlippedFilt --allow-no-sex --bmerge controlMergedFilt --make-bed --out allMerged

~/SequencingSoftware/plink --bfile allMerged --allow-no-sex --flip-scan --flip-scan-verbose --out flip3
cat flip3.flipscan | tr -s " " > flip3.flipscan.mod

## Exclude SNPs with problems in LD
~/SequencingSoftware/plink --bfile allMerged --exclude SNPsexclude.list --make-bed --out allMergedFilt

## Exclude SNPs with very different MAFs
~/SequencingSoftware/plink --bfile allMergedFilt --allow-no-sex --freq case-control
~/SequencingSoftware/plink --bfile allMergedFilt --exclude SNPsexcludeMAF.list --make-bed --out allMergedFiltMAF



# Run PCA
~/SequencingSoftware/plink --bfile allMergedFiltMAF --pca --out pca

# Remove outlier samples
~/SequencingSoftware/plink --bfile allMergedFiltMAF --remove Sampsexclude.list --make-bed --allow-no-sex --out allMergedFiltMAFSamps
~/SequencingSoftware/plink --bfile allMergedFiltMAFSamps --pca --out pca2

~/SequencingSoftware/plink --bfile allMergedFiltMAFSamps --remove Sampsexclude2.list --make-bed --allow-no-sex --out allMergedFiltMAFSamps2
~/SequencingSoftware/plink --bfile allMergedFiltMAFSamps2 --pca --out pca3

~/SequencingSoftware/plink --bfile allMergedFiltMAFSamps2 --remove Sampsexclude3.list --make-bed --allow-no-sex --out allMergedFiltMAFSamps3
~/SequencingSoftware/plink --bfile allMergedFiltMAFSamps3 --pca --out pca4
