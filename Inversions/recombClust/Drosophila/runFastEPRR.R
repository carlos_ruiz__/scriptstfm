#'#################################################################################
#'#################################################################################
#' Run FastEPRR in inversions in Drosophila using recombClust classification ####
#'#################################################################################
#'#################################################################################

# Prepare folder
fast=results/FEPPR
mkdir $fast

## IN2L
mkdir $fast/IN2L
mkdir $fast/IN2L/I/
mkdir $fast/IN2L/I/step1
mkdir $fast/IN2L/I/step1/step1
mkdir $fast/IN2L/I/step2

mkdir $fast/IN2L/N/
mkdir $fast/IN2L/N/step1
mkdir $fast/IN2L/N/step1/step1
mkdir $fast/IN2L/N/step2

## IN3R
mkdir $fast/IN3R
mkdir $fast/IN3R/I/
mkdir $fast/IN3R/I/step1
mkdir $fast/IN3R/I/step1/step1
mkdir $fast/IN3R/I/step2

mkdir $fast/IN3R/N/
mkdir $fast/IN3R/N/step1
mkdir $fast/IN3R/N/step1/step1
mkdir $fast/IN3R/N/step2

# Preprocess data
### Create plink removing non SNPs variants
plink --allow-extra-chr --vcf data/dgrp2.vcf.gz --make-bed --out $fast/dgpr2

## Remove Individuals call rate < 90%
plink --bfile $fast/dgpr2 --mind 0.10 --allow-extra-chr --make-bed --out $fast/dgpr2.ind

## Remove SNPs call rate < 100%
plink --bfile $fast/dgpr2.ind --geno 0 --allow-extra-chr --make-bed --out $fast/dgpr2.ind.snps

## Remove SNPs MAF < 5%
plink --bfile $fast/dgpr2.ind.snps --maf 0.01 --allow-extra-chr --make-bed --out $fast/dgpr2.ind.snps.MAF

# Remove heterozygous snps (No hay)
# plink --bfile $fast/dgpr2.MAF.snps --freqx --allow-extra-chr --out $fast/dgpr2.MAF.snps
# awk '$6 != 0 {print $1}' $fast/dgpr2.MAF.snps.frqx > $fast/hetSNPs.tab

## Split files by chromosome
array=(2L 2R 3R)
for ch in "${array[@]}"
do
  plink --bfile $fast/dgpr2.ind.snps.MAF --chr $ch --allow-extra-chr --recode vcf --out $fast/dgpr2.$ch
  sed -i -e 's#/#|#g' $fast/dgpr2.$ch.vcf 
done



# Run R
## Prepare Session  ####
library(FastEPRR)
library(parallel)
library(VariantAnnotation)
library(Gviz)
library(GenomicRanges)

# Data for plots
library(TxDb.Dmelanogaster.UCSC.dm6.ensGene)
dm6genes <- genes(TxDb.Dmelanogaster.UCSC.dm6.ensGene)

fold <- "results/models/"
fast <- "results/FEPPR/"
invReg <- read.csv2("data/ranges.csv", header = TRUE)

## Compute recombClust patterns
getRecombProb <- function(classvec, class, indsmat, overLaps){
  
  ## Select cluster individuals
  indsmat1 <- indsmat[classvec == class, ]
  
  ## Compute cluster Recomb freq by mean of voting
  sapply(seq_len(overLaps@nLnode), function(chunk){
    
    sel <- to(overLaps)[from(overLaps) == chunk]
    mat <- indsmat1[, sel, drop = FALSE]
    val <- mean(rowMeans(mat > 0.5) > 0.5)
  })
}


## Run IN2L ####
inv <- paste0(fast, "IN2L/")
load(paste0(fold, "In(2L)trecombRes.Rdata"))

samps <- samples(scanVcfHeader(paste0(fast, "dgpr2.2L.vcf")))

class <- recomb$class
I <- names(class[class == 2])
I <- unique(substring(I, 1, nchar(I) - 2))
I <- intersect(I, samps)
## Select first chromosome
I <- paste0(I, "[1:0]")
I <- paste(I, collapse = ";")

N <- names(class[class == 1])
N <- unique(substring(N, 1, nchar(N) - 2))
N <- intersect(N, samps)

## Select first chromosome
N <- paste0(N, "[1:0]")
N <- paste(N, collapse = ";")

# Run step 1
## I
FastEPRR_VCF_step1(vcfFilePath = paste0(fast, "dgpr2.2L.vcf"), 
                   winLength="400", stepLength = "150", 
                   idvlConsidered= I,
                   erStart = "2225",
                   erEnd = "13154",
                   srcOutputFilePath= paste0(inv, "I/step1/step1"))

## N
FastEPRR_VCF_step1(vcfFilePath =  paste0(fast, "dgpr2.2L.vcf"), 
                   winLength="400", stepLength = "150", 
                   idvlConsidered= N,
                   erStart = "2225",
                   erEnd = "13154",
                   srcOutputFilePath= paste0(inv, "N/step1/step1"))

# Run step 2
## I
mclapply(1:24, FastEPRR_VCF_step2, 
         srcFolderPath=paste0(inv, "I/step1"),
         jobNumber = 24, mc.cores = 24,
         DXOutputFolderPath = paste0(inv, "I/step2"))


## N
mclapply(1:24, FastEPRR_VCF_step2, 
         srcFolderPath=paste0(inv, "N/step1"),
         jobNumber = 24, mc.cores = 24,
         DXOutputFolderPath = paste0(inv, "N/step2"))


## Run step3
## I
FastEPRR_VCF_step3(srcFolderPath = paste0(inv, "I/step1/"),
                   DXFolderPath=paste0(inv, "I/step2"), 
                   finalOutputFolderPath=paste0(inv, "I/"))

## N
FastEPRR_VCF_step3(srcFolderPath = paste0(inv, "N/step1/"),
                   DXFolderPath=paste0(inv, "N/step2"), 
                   finalOutputFolderPath=paste0(inv, "N/"))


## Run python parser (out of R)
python ~/data/CarlosRuiz/Inversions/InversionSequencing/FASTEPPR_parser.py -i $fast/IN2L/N/chr_2L -o $fast/IN2L/N/chr_2L.txt
python ~/data/CarlosRuiz/Inversions/InversionSequencing/FASTEPPR_parser.py -i $fast/IN2L/I/chr_2L -o $fast/IN2L/I/chr_2L.txt


### Make plot ####
range <- GRanges("chr2L:2225744-13154180")

mat <- recomb$mat
width <- 150e3
starts <- seq(start(range), end(range), width)
chunks <- GRanges(seqnames = as.character(seqnames(range)), 
                  IRanges(start = starts, width = width))

coords <- data.frame(t(sapply(recomb$models, function(x) x$annot)))
coords$chr <- as.character(seqnames(range))
grMods <- makeGRangesFromDataFrame(coords)

overNum <- countOverlaps(chunks, grMods)
overAll <- findOverlaps(chunks, grMods)

clusProbs <- lapply(unique(recomb$class), getRecombProb, 
                    classvec = recomb$class, 
                    indsmat = mat, overLaps = overAll)

## Load base track
basetracks <- list(IdeogramTrack(genome = "dm6", chromosome = "chr2L"), 
                   Gviz::GenomeAxisTrack())

## Select only protein coding genes
txs <- subsetByOverlaps(dm6genes, range)

genes <- Gviz::GeneRegionTrack(txs, name = "Genes", 
                               symbol = txs$gene_name, 
                               fill = "lightblue", 
                               gene = txs$GENEID,
                               showId = TRUE, geneSymbol = TRUE, cex.title = 0.7,
                               shape = "arrow", transcriptAnnotation = "symbol",
                               collapseTranscripts = TRUE, rotation.title = 0)

## FastEPPR track
al1 <- read.table("results/FEPPR/IN2L/N/chr_2L.txt", header = TRUE)
al2 <- read.table("results/FEPPR/IN2L/I/chr_2L.txt", header = TRUE)

rownames(al1) <- al1$Start
rownames(al2) <- al2$Start
allNames <- union(rownames(al1), rownames(al2))
al1 <- al1[allNames, ]
rownames(al1) <- allNames
al1$al2Rho <- al2[rownames(al1), "Rho"]
al1$Start <- al1$Start * 1000
al1$End <- al1$End * 1000
al1$Chr <- "chr2L"

FGR <- makeGRangesFromDataFrame(al1[, -(4:5)], keep.extra.columns = TRUE)
FastEPPRtrack <- DataTrack(FGR, type ="a", name = "FastEPPR\nrecombination rate\n(cM/Mb)",
                           groups = c("Standard", "Inverted"),
                           cex.title = 0.7)

FGR$Diff <- FGR$al2Rho - FGR$Rho
FEPPRDifftrack <- DataTrack(FGR, type =c("a", "mountain"), baseline = 0, data =  FGR$Diff, 
                            name = "Difference in\nrecombination rate\n(Inv-Std)", 
                            col = "black",
                            cex.title = 0.7)


## RecombClust track
df <- data.frame(chr = "chr2L", start = starts, end = starts + 5e4, 
                 RhoAl1 = clusProbs[[1]], RhoAl2 = clusProbs[[2]])
recombGR <- makeGRangesFromDataFrame(df, keep.extra.columns = TRUE)
recombTrack <- DataTrack(recombGR, type ="a", name = "recombClust \nrecombination frequency\n(%)",
                         groups = c("Standard", "Inverted"),
                         cex.title = 0.7)


recombGR$Diff <- recombGR$RhoAl2 - recombGR$RhoAl1
recombDifftrack <- DataTrack(recombGR, type =c("a", "mountain"),  baseline = 0,
                             data =  recombGR$Diff, 
                             name = "Difference in\nrecombination frequency\n(Inv-Std)", 
                             col = "black", 
                             cex.title = 0.7)


## Make plot
png(paste0(fast, "IN2L.png"), width = 30, height = 25, units = 'cm', res = 300)
Gviz::plotTracks(c(basetracks, genes, FastEPPRtrack, FEPPRDifftrack, 
                   recombTrack, recombDifftrack), 
                 sizes = c(1, 1, 2, 4, 4, 4, 4), 
                 from = start(range), to = end(range), 
                 groupAnnotation = "group",
                 std = "purple", inv = "blue", 
                 fontcolor.title = "black",
                 col.axis = "black",
                 background.title = "grey90")
dev.off()

## Run IN3R ####
inv <- paste0(fast, "IN3R/")
load(paste0(fold, "In(3R)MorecombRes.Rdata"))

samps <- samples(scanVcfHeader(paste0(fast, "dgpr2.3R.vcf")))

class <- recomb$class
I <- names(class[class == 1])
I <- unique(substring(I, 1, nchar(I) - 2))
I <- intersect(I, samps)
## Select first chromosome
I <- paste0(I, "[1:0]")
I <- paste(I, collapse = ";")

N <- names(class[class == 2])
N <- unique(substring(N, 1, nchar(N) - 2))
N <- intersect(N, samps)

## Select first chromosome
N <- paste0(N, "[1:0]")
N <- paste(N, collapse = ";")

# Run step 1
## I
FastEPRR_VCF_step1(vcfFilePath = paste0(fast, "dgpr2.3R.vcf"), 
                   winLength="400", stepLength = "100", 
                   idvlConsidered= I,
                   erStart = "17232",
                   erEnd = "24857",
                   srcOutputFilePath= paste0(inv, "I/step1/step1"))

## N
FastEPRR_VCF_step1(vcfFilePath =  paste0(fast, "dgpr2.2L.vcf"), 
                   winLength="400", stepLength = "150", 
                   idvlConsidered= N,
                   erStart = "2225",
                   erEnd = "13154",
                   srcOutputFilePath= paste0(inv, "N/step1/step1"))

# Run step 2
## I
mclapply(1:24, FastEPRR_VCF_step2, 
         srcFolderPath=paste0(inv, "I/step1"),
         jobNumber = 24, mc.cores = 24,
         DXOutputFolderPath = paste0(inv, "I/step2"))


## N
mclapply(1:24, FastEPRR_VCF_step2, 
         srcFolderPath=paste0(inv, "N/step1"),
         jobNumber = 24, mc.cores = 24,
         DXOutputFolderPath = paste0(inv, "N/step2"))


## Run step3
## I
FastEPRR_VCF_step3(srcFolderPath = paste0(inv, "I/step1/"),
                   DXFolderPath=paste0(inv, "I/step2"), 
                   finalOutputFolderPath=paste0(inv, "I/"))

## N
FastEPRR_VCF_step3(srcFolderPath = paste0(inv, "N/step1/"),
                   DXFolderPath=paste0(inv, "N/step2"), 
                   finalOutputFolderPath=paste0(inv, "N/"))


## Run python parser (out of R)
python ~/data/CarlosRuiz/Inversions/InversionSequencing/FASTEPPR_parser.py -i $fast/IN2L/N/chr_2L -o $fast/IN2L/N/chr_2L.txt
python ~/data/CarlosRuiz/Inversions/InversionSequencing/FASTEPPR_parser.py -i $fast/IN2L/I/chr_2L -o $fast/IN2L/I/chr_2L.txt


### Make plot ####
range <- GRanges("chr3R:17232639-24857019")

mat <- recomb$mat
width <- 150e3
starts <- seq(start(range), end(range), width)
chunks <- GRanges(seqnames = as.character(seqnames(range)), 
                  IRanges(start = starts, width = width))

coords <- data.frame(t(sapply(recomb$models, function(x) x$annot)))
coords$chr <- as.character(seqnames(range))
grMods <- makeGRangesFromDataFrame(coords)

overNum <- countOverlaps(chunks, grMods)
overAll <- findOverlaps(chunks, grMods)

clusProbs <- lapply(unique(recomb$class), getRecombProb, 
                    classvec = recomb$class, 
                    indsmat = mat, overLaps = overAll)

## Load base track
basetracks <- list(IdeogramTrack(genome = "dm6", chromosome = "chr2L"), 
                   Gviz::GenomeAxisTrack())

## Select only protein coding genes
txs <- subsetByOverlaps(dm6genes, range)

genes <- Gviz::GeneRegionTrack(txs, name = "Genes", 
                               symbol = txs$gene_name, 
                               fill = "lightblue", 
                               gene = txs$GENEID,
                               showId = TRUE, geneSymbol = TRUE, cex.title = 0.7,
                               shape = "arrow", transcriptAnnotation = "symbol",
                               collapseTranscripts = TRUE, rotation.title = 0)

## FastEPPR track
al1 <- read.table("results/FEPPR/IN2L/N/chr_2L.txt", header = TRUE)
al2 <- read.table("results/FEPPR/IN2L/I/chr_2L.txt", header = TRUE)

rownames(al1) <- al1$Start
rownames(al2) <- al2$Start
allNames <- union(rownames(al1), rownames(al2))
al1 <- al1[allNames, ]
rownames(al1) <- allNames
al1$al2Rho <- al2[rownames(al1), "Rho"]
al1$Start <- al1$Start * 1000
al1$End <- al1$End * 1000
al1$Chr <- "chr2L"

FGR <- makeGRangesFromDataFrame(al1[, -(4:5)], keep.extra.columns = TRUE)
FastEPPRtrack <- DataTrack(FGR, type ="a", name = "FastEPPR\nrecombination rate\n(cM/Mb)",
                           groups = c("Standard", "Inverted"),
                           cex.title = 0.7)

FGR$Diff <- FGR$al2Rho - FGR$Rho
FEPPRDifftrack <- DataTrack(FGR, type =c("a", "mountain"), baseline = 0, data =  FGR$Diff, 
                            name = "Difference in\nrecombination rate\n(Inv-Std)", 
                            col = "black",
                            cex.title = 0.7)


## RecombClust track
df <- data.frame(chr = "chr2L", start = starts, end = starts + 5e4, 
                 RhoAl1 = clusProbs[[1]], RhoAl2 = clusProbs[[2]])
recombGR <- makeGRangesFromDataFrame(df, keep.extra.columns = TRUE)
recombTrack <- DataTrack(recombGR, type ="a", name = "recombClust \nrecombination frequency\n(%)",
                         groups = c("Standard", "Inverted"),
                         cex.title = 0.7)


recombGR$Diff <- recombGR$RhoAl2 - recombGR$RhoAl1
recombDifftrack <- DataTrack(recombGR, type =c("a", "mountain"),  baseline = 0,
                             data =  recombGR$Diff, 
                             name = "Difference in\nrecombination frequency\n(Inv-Std)", 
                             col = "black", 
                             cex.title = 0.7)


## Make plot
png(paste0(fast, "IN2L.png"), width = 30, height = 25, units = 'cm', res = 300)
Gviz::plotTracks(c(basetracks, genes, FastEPPRtrack, FEPPRDifftrack, 
                   recombTrack, recombDifftrack), 
                 sizes = c(1, 1, 2, 4, 4, 4, 4), 
                 from = start(range), to = end(range), 
                 groupAnnotation = "group",
                 std = "purple", inv = "blue", 
                 fontcolor.title = "black",
                 col.axis = "black",
                 background.title = "grey90")
dev.off()


