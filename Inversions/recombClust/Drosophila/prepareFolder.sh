#'#################################################################################
#'#################################################################################
#' Prepare folder
#'#################################################################################
#'#################################################################################

mkdir data
mkdir results
mkdir results/models
mkdir results/iHS

## Add VCF
ln -s ~/data/abareas/data/dgrp2.vcf data/dgrp2.vcf

## Index VCF
bgzip data/dgrp2.vcf
tabix -p vcf data/dgrp2.vcf.gz

## Add ranges
cp ~/data/abareas/data/inversions-data.csv data/ranges.csv
cp ~/data/abareas/data/inversion.xlsx data/inv_genos.xlsx

