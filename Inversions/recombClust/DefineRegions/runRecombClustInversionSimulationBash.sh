#'#################################################################################
#'#################################################################################
#' Check recombClust clustering in simulated inversions
#'#################################################################################
#'#################################################################################


## These lines can be run when applying to all inversions
# lengths=(50000 100000 250000 500000 1000000)
# freqs=(0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9)
# sim=($(seq 1 1 100))
# for t in $(seq 1 1 100) 
#   do  
#   for f in ${freqs[@]}
#     do
#     for l in ${lengths[@]}
#       do
#       ## Make blocks of 20 simulation to parallelize
#       echo l_$l.f_$f.$s
#       Rscript runLDModelSimulation.R ./l_$l.f_$f/l_$l.f_$f.$s $l 2> log.txt
#       done 
#     done 
# done

## Run in inversions with lenght 250Kb and freq: 0.5
for i in $(seq 1 1 100)
do
  Rscript runRecombClustInversionSimulation.R data/l_250000.f_0.5/l_250000.f_0.5.$i results/invSim/l_250000.f_0.5.$i 2> results/invSim/l_250000.f_0.5.$i.log.txt
done

## Run in inversions with length 100Kb and freq: 0.5
for i in $(seq 1 1 100)
do
  Rscript runRecombClustInversionSimulation.R data/l_100000.f_0.5/l_100000.f_0.5.$i results/invSim/l_100000.f_0.5.$i 2> results/invSim/l_100000.f_0.5.$i.log.txt
done

## Run in inversions with length 1Mb and freq: 0.5
for i in $(seq 1 1 100)
do
  Rscript runRecombClustInversionSimulation.R data/l_1000000.f_0.5/l_1000000.f_0.5.$i results/invSim/l_1000000.f_0.5.$i 2> results/invSim/l_1000000.f_0.5.$i.log.txt
done

## Run region detection
N=15
lengths=(100000 250000 1000000)
for l in ${lengths[@]}
  do
  for i in $(seq 1 1 100)
  do
    ((j=j%N)); ((j++==0)) && wait
    Rscript clusterFeaturesSimInversionScript.R results/invSim/l_${l}.f_0.5.$i 2> results/invSim/l_${l}.f_0.5.$i.calling.log.txt &
  done
done

## Run region detection (only TAD detection)
lengths=(100000 250000 1000000)
for l in ${lengths[@]}
  do
  for i in $(seq 1 1 100)
  do
    Rscript TADcallingSimInversionScript.R results/invSim/l_${l}.f_0.5.$i 2> results/invSim/l_${l}.f_0.5.$i.TADcalling.log.txt
  done
done

## Run region detection based on classification
N=15
lengths=(100000 250000 1000000)
for l in ${lengths[@]}
  do
  for i in $(seq 1 1 100)
  do
    ((j=j%N)); ((j++==0)) && wait
    Rscript clusterFeaturesIndClassificationScript.R results/invSim/l_${l}.f_0.5.$i 2> results/invSim/l_${l}.f_0.5.$i.calling.log.txt &
  done
done


## Run region detection based on classification
N=31
lengths=(100000 250000 1000000)
for l in ${lengths[@]}
  do
  for i in $(seq 1 1 100)
  do
    ((j=j%N)); ((j++==0)) && wait
    Rscript clusterFeaturesSimInversionRecursiveScript.R results/invSim/l_${l}.f_0.5.$i 2> results/invSim/l_${l}.f_0.5.$i.calling.log.txt &
  done
done