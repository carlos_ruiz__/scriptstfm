/*
 * Run method to detect regions on stable populations
 */

params.slimScript = ""
slimScript = file(params.slimScript)

// Create channels with simulations
sims = Channel.from( 1..100 )

// Create population with SLiM
process createPopulationSLiM {

  publishDir "results/SLiMregions/VCFs/", mode: 'copy'


  input:
  file (script) from slimScript
  val (sim) from sims

  output:
  set val(sim), file("random_sim${sim}.vcf.gz"), file("random_sim${sim}.vcf.gz.tbi") into pop

  script:
  """
  slim -s $sim $script > res.out
  tail -n +15 res.out > random_sim${sim}.vcf
  bgzip random_sim${sim}.vcf
  tabix random_sim${sim}.vcf.gz
  """
}

process runRecombClust {

  label 'recombClust'

  publishDir "results/SLiMregions/recombClustModels/", mode: 'copy'

  input:
  set val(name), file(vcf), file(tbi) from pop

  output:
  set val(name), file("random_sim.${name}.wholeRegionModels.Rdata") into recombModels

  """
  runRecombClustVCF.R $vcf random_sim.${name}.wholeRegionModels.Rdata
  """

}


// Collapse models in 500 bases windows
process collapseWindows {

  publishDir "results/SLiMregions/windowProbs/", mode: 'copy'

  input:
  set val(name), file(model) from recombModels

  output:
  set val(name), file("random_sim.${name}.windowProbs.Rdata") into probs

  script:
  """
  collapseModelsPerWindow.R ${model} random_sim.${name}.windowProbs.Rdata
  """
}

// Define regions using recursive algorithm
process defineRegions {

  publishDir "results/SLiMregions/recombClustRegions/", mode: 'copy'

  input:
  set val(name), file(model) from probs

  output:
  set val(name), file("random_sim.${name}.LDalgorithm_Ranges.Rdata") into regions

  script:
  """
  bigLDbasedAlgorithmScript.R random_sim.${name} "chr1"
  """
}
