/*
 * Define regions inversions with recombClust
 */

params.windowFiles = ""

// Create channels from files
windows = Channel.fromPath( "${params.windowFiles}" ).map { name -> tuple(name.baseName.replaceFirst(/..windowProbs/, ""), file(name)) }

// Define regions using recursive algorithm
process defineRegions {

  publishDir "results/recombClustRegions/", mode: 'copy'

  input:
  set val(name), file(model) from windows

  output:
  set val(name), file("${name}..LDalgorithm_Ranges.Rdata") into regions

  script:
  """
  bigLDbasedAlgorithmScript.R ${name}. "chr1"
  """
}
