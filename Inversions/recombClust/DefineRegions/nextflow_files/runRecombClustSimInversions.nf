/*
 * Extract SNPs present in Illumina 450K from WGS data
 */

// Override baseDir from nextflow repository to current directory
baseDir = "$PWD"
date = java.time.LocalDate.now()

params.outdir = "$baseDir/results"
params.inFold = baseDir

genos = Channel
                .fromPath("${params.inFold}/**.haplotypes_0.dat")
                .map { file -> tuple(file.baseName.replaceAll(/.haplotypes_0/, ""), file) }

// Run recombClust models
process runRecombClust {

  publishDir "${params.outdir}/models/", mode: 'copy'

  input:
  set datasetID, file(datasetFile) from genos

  output:
  set datasetID, file("${datasetID}.wholeModels.Rdata") into models

  script:
  """
  Rscript $baseDir/runRecombClustInversionSimulation.R ${datasetFile} ${datasetID}.wholeModels.Rdata
  """
}

// Collapse models in 500 bases windows
process collapseWindows {

  publishDir "${params.outdir}/windowProbs/", mode: 'copy'

  input:
  set datasetID, file(model) from models

  output:
  set datasetID, file("${datasetID}.windowProbs.Rdata") into probs

  script:
  """
  Rscript $baseDir/collapseModelsPerWindow.R ${model} ${datasetID}.windowProbs.Rdata
  """
}
