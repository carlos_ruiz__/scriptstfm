#!/bin/bash
#SBATCH -J runNextflow
#SBATCH -N 1
#SBATCH --time=7-00:00:00
#SBATCH -o nextflow.out # STDOUT
#SBATCH -e nextflow.err # STDERR
#SBATCH --mem=1g
#SBATCH --tasks-per-node=1

module load java

#export TOWER_ACCESS_TOKEN=8a44f08560037abafb3c11d478d6aa10fb05e237
#export NXF_VER=20.04.1

#~/nextflow run runRecombClust.nf --haploFiles "data/l_1000000.*.dat.gz" -with-report recombClust.html -resume
#~/nextflow run recombClustDefineRegionsEmptyRegions.nf --initialxml in.xml --paramFile par.xml -resume
#~/nextflow run recombClustDefineRegionsSimInversions.nf --windowFiles "results/windowProbs/*" -resume
#~/nextflow run recombClustDefineRegionsEmptyRegionsLD.nf --initialxml in.xml --paramFile par.xml -resume
#~/nextflow run recombClustDefineRegionsSimInversionsLDalgorithm.nf --windowFiles "results/windowProbs/*" -resume
~/nextflow run recombClustDefineRegionsSLiMRegionsLD.nf --slimScript neutralSimulationHuman.sim -resume

