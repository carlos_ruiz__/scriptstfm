/*
 * Run method to detect regions on stable populations
 */

params.initialxml = ""
params.paramFile = ""

paramFile = file(params.paramFile)
initialxml = file(params.initialxml)

// Create channels with simulations
sims = Channel.from( 1..100 )

// Create base population
process runBasePopulation {

  label 'simulation'

  input:
  file (ini) from initialxml
  file (pars) from paramFile
  val (sim) from sims

  output:
  set val(sim), file("popini.xml") into inipop

  script:
  """
  invertFREGENE -i $ini -p $pars -gn 10000 -sd $sim -recombsd $sim -o popini.xml -s -freq
  """
}

process outputResults {

  label 'simulation'

  publishDir "results/randomRegions/haplotypeFiles/", mode: 'copy'


  input:
  set sim, file(invxml) from inipop

  output:
  set sim, file ("random_sim.${sim}.haplotypes_0.dat.gz") into haplotypes

  script:
  """
  SAMPLE -noshuffle -i $invxml -oh random_sim.${sim}.haplotypes -og random_sim.${sim}.genotypes \
    -sd $sim -controls 1000

  gzip random_sim.${sim}.haplotypes_0.dat
  """
}

process runRecombClust {

  label 'recombClust'

  publishDir "results/randomRegions/recombClustModels/", mode: 'copy'

  input:
  set val(name), file(haplo) from haplotypes

  output:
  set val(name), file("random_sim.${name}.wholeRegionModels.Rdata") into recombModels

  """
  runRecombClust.R $haplo
  """

}


// Collapse models in 500 bases windows
process collapseWindows {

  publishDir "results/randomRegions/windowProbs/", mode: 'copy'

  input:
  set val(name), file(model) from recombModels

  output:
  set val(name), file("random_sim.${name}.windowProbs.Rdata") into probs

  script:
  """
  collapseModelsPerWindow.R ${model} random_sim.${name}.windowProbs.Rdata
  """
}

// Define regions using recursive algorithm
process defineRegions {

  publishDir "results/randomRegions/recombClustRegions/", mode: 'copy'

  input:
  set val(name), file(model) from probs

  output:
  set val(name), file("random_sim.${name}.chclust_Calinhara_Ranges_recursive.Rdata") into regions

  script:
  """
  clusterFeaturesSimInversionRecursiveScript.R random_sim.${name} "chr1"
  """
}

