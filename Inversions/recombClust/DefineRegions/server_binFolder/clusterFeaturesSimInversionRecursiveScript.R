#!/usr/local/bin/Rscript
## Code to run recombClust to simulated inversions

args <- commandArgs(trailingOnly=TRUE)
root <- args[1]
chrom <- args[2]

## Load libraries
library(rioja)
library(GenomicRanges)
library(fpc)
library(clValid)

## Load data
load( paste0(root, ".windowProbs.Rdata"))

## Remove windows with >5% missing data
mats <- probTab[, colMeans(is.na(probTab)) < 0.05]
corMat <- 1 - abs(cor(mats))

getRegions <- function(corMat, meanCor = 0.5) {
  
  if (median(as.vector(1 - corMat)) > meanCor){
    region <- GRanges(paste0(chrom, ":", gsub("-.*", "", rownames(corMat)[1]), "-", gsub(".*-", "", rownames(corMat)[nrow(corMat)])))
    return(region)
  }
  
  if (nrow(corMat) < 10 | sum(var(corMat)) == 0){
    region <- GRanges(paste0(chrom, ":", gsub("-.*", "", rownames(corMat)[1]), "-", gsub(".*-", "", rownames(corMat)[nrow(corMat)])))
    return(region)
  }
  
  pcs <- prcomp(corMat, rank. = 60)
  pc <- pcs$x
  pcdist <- dist(pc)
  clust <- chclust(pcdist)
  clusMat <- sapply(2:(min(15, nrow(corMat) - 2)), function(k) {
    l <- cluster.stats(pcdist, cutree(clust, k = k), wgap = FALSE, sepindex = FALSE)
    unlist(l[c("ch", "avg.silwidth")])
  })
  
  k <- which.max(clusMat[1, ]) + 1
  sil_constr <- clusMat[2, k - 1]
  
  class <- cutree(clust, k = k)
    
  unlist(GRangesList(lapply(unique(class), function(i){
        getRegions(corMat[class == i, class == i, drop = FALSE], meanCor = meanCor)
  })))
}

regions_010 <- getRegions(corMat, meanCor = 0.1)
regions_015 <- getRegions(corMat, meanCor = 0.15)
regions_020 <- getRegions(corMat, meanCor = 0.2)
regions_025 <- getRegions(corMat, meanCor = 0.25)
regions_030 <- getRegions(corMat, meanCor = 0.3)
regions_040 <- getRegions(corMat, meanCor = 0.4)
regions_050 <- getRegions(corMat, meanCor = 0.5)
regions_060 <- getRegions(corMat, meanCor = 0.6)
regions_070 <- getRegions(corMat, meanCor = 0.7)
regions_080 <- getRegions(corMat, meanCor = 0.8)
regions_090 <- getRegions(corMat, meanCor = 0.9)
save(regions_010, regions_015, regions_020, regions_025, regions_030, regions_040,regions_050,regions_060,regions_070, regions_080, regions_090,
     file = paste0(root, ".chclust_Calinhara_Ranges_recursive.Rdata"))

