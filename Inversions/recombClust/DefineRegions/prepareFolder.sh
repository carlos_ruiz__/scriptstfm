#'#################################################################################
#'#################################################################################
#' Prepare folder for regions definition
#'#################################################################################
#'#################################################################################

## Set folder
ln -s ~/data/CarlosRuiz/Inversions/LDclassifier/InversionSimulation data
mkdir results
mkdir results/invSim
mkdir results/invSim/archive
mkdir results/EUR1KG

## Add 1KG files
cp ~/PublicData/STUDY/1000GENOME/Samples_Pop1GK.Rdata data

ln -s ~/PublicData/STUDY/1000GENOME/VCF/ALL.chr17.phase3_shapeit2_mvncall_integrated_v5a.20130502.genotypes.vcf.gz data/1000G_chr17.vcf.gz
ln -s ~/PublicData/STUDY/1000GENOME/VCF/ALL.chr17.phase3_shapeit2_mvncall_integrated_v5a.20130502.genotypes.vcf.gz.tbi data/1000G_chr17.vcf.gz.tbi

ln -s ~/PublicData/STUDY/1000GENOME/VCF/ALL.chr8.phase3_shapeit2_mvncall_integrated_v5a.20130502.genotypes.vcf.gz data/1000G_chr8.vcf.gz
ln -s ~/PublicData/STUDY/1000GENOME/VCF/ALL.chr8.phase3_shapeit2_mvncall_integrated_v5a.20130502.genotypes.vcf.gz.tbi data/1000G_chr8.vcf.gz.tbi

ln -s ~/PublicData/STUDY/1000GENOME/VCF/ALL.chr1.phase3_shapeit2_mvncall_integrated_v5a.20130502.genotypes.vcf.gz data/1000G_chr1.vcf.gz
ln -s ~/PublicData/STUDY/1000GENOME/VCF/ALL.chr1.phase3_shapeit2_mvncall_integrated_v5a.20130502.genotypes.vcf.gz.tbi data/1000G_chr1.vcf.gz.tbi

ln -s ~/PublicData/STUDY/1000GENOME/VCF/ALL.chr2.phase3_shapeit2_mvncall_integrated_v5a.20130502.genotypes.vcf.gz data/1000G_chr2.vcf.gz
ln -s ~/PublicData/STUDY/1000GENOME/VCF/ALL.chr2.phase3_shapeit2_mvncall_integrated_v5a.20130502.genotypes.vcf.gz.tbi data/1000G_chr2.vcf.gz.tbi


## Save results of first version
mkdir results/invSim/archive/20200123
mv results/invSim/l* results/invSim/archive/20200123/
mv results/invSim/defineRegion_Inversions_Resolutions_accuracy.png results/invSim/archive/20200123/
mv results/invSim/archive/20200123/*whole* results/invSim/

## Save results of second version
mkdir results/invSim/archive/20200129
mv results/invSim/*called*  results/invSim/archive/20200129/

## Add original recombClust classifications
cp ~/data/CarlosRuiz/RecombClust/HumanPopScan/results/models/reg24recombRes.Rdata data/LCTmodels.Rdata
cp ~/data/CarlosRuiz/RecombClust/Chr1_region/results/models/recombClust_1000G_short.Rdata data/TAR_reg_models.Rdata
cp ~/data/CarlosRuiz/Inversions/LDclassifier/EuropeanSamples/PCsinvs8_17_EUR_pruned0.4.Rdata data/HumanInvsPCs.Rdata

