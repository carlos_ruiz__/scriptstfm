#'#################################################################################
#'#################################################################################
#' Run iHS on selected regions
#'#################################################################################
#'#################################################################################

# Convert population to txt
iHS=results/iHS

echo 'load("data/Samples_Pop1GK.Rdata")
write.table(samp_pop, file = "'$iHS/samp_pop.tab'", quote=F, row.names=FALSE, col.names=FALSE, sep="\t")
' | Rscript --vanilla -

## Make lists by population
arr=(EUR TSI IBS CEU FIN GBR)
for sel in "${arr[@]}"
do
  grep $sel $iHS/samp_pop.tab | cut -f1 > $iHS/$sel.tab
done


# LCT (chr2:135792491-136822774)

## Select European and biallellic sites in whole chromosome
vcftools --gzvcf ~/PublicData/STUDY/1000GENOME/VCF/ALL.chr2.phase3_shapeit2_mvncall_integrated_v5a.20130502.genotypes.vcf.gz --min-alleles 2 --max-alleles 2 --remove-indels --keep $iHS/EUR.tab --recode --stdout | gzip -c > $iHS/LCT.vcf.gz

## Remove snps MAF < 0.05
vcftools --gzvcf $iHS/LCT.vcf.gz --maf 0.05 --recode --stdout | bgzip -c > $iHS/LCT.filt.vcf.gz
tabix -p vcf results/iHS/LCT.filt.vcf.gz

## Add genetic position and make map file
plink --vcf $iHS/LCT.filt.vcf.gz --cm-map ~/PublicData/REFERENCES/reference_panels/genetic_map_chr2_combined_b37.txt 2 --recode --out $iHS/LCT.EUR

## Remove ped
rm $iHS/LCT.EUR.ped

## Subset plink per group population
arr=(TSI IBS CEU FIN GBR)
for sel in "${arr[@]}"
do
  vcftools --gzvcf $iHS/LCT.filt.vcf.gz --keep $iHS/$sel.plink.tab --recode --stdout | gzip -c >  $iHS/LCT.$sel.vcf.gz
done

## Run selScan
for sel in "${arr[@]}"
do
  selscan --ihs --vcf $iHS/LCT.$sel.vcf.gz --map $iHS/LCT.EUR.map --out $iHS/LCT.$sel --threads 16
  ~/software/selscan/bin/linux/norm --ihs --files $iHS/LCT.$sel.ihs.out
done


## Run in recombClust alleles
arr=(cl1 cl2)
for sel in "${arr[@]}"
do
  vcftools --gzvcf $iHS/LCT.filt.vcf.gz --keep $iHS/$sel.tab --recode --stdout | gzip -c >  $iHS/LCT.$sel.vcf.gz
done

## Run selScan
for sel in "${arr[@]}"
do
  selscan --ihs --vcf $iHS/LCT.$sel.vcf.gz --map $iHS/LCT.EUR.map --out $iHS/LCT.$sel --threads 16
  ~/software/selscan/bin/linux/norm --ihs --files $iHS/LCT.$sel.ihs.out
done

## Compute allele frequencies
arr=(cl1 cl2 TSI IBS CEU FIN GBR)
for sel in "${arr[@]}"
do
  plink --vcf $iHS/LCT.$sel.vcf.gz --chr 2 --from-bp 135700000 --to-bp 136900000 --freq --out $iHS/LCT.$sel 
  sed 's/ \+/ /g' $iHS/LCT.$sel.frq > $iHS/LCT.$sel.good.frq
done



# SMARCC1 (3:47675294-47777551)

## Select region 
### Seleccionar region más grande que la objetivo (En regiones pequeñas, selscan da error porque el eHH no cae suficiente)
vcftools --gzvcf ~/PublicData/STUDY/1000GENOME/VCF/ALL.chr3.phase3_shapeit2_mvncall_integrated_v5a.20130502.genotypes.vcf.gz --chr 3 --from-bp 46000000 --to-bp 49000000 --recode --stdout | gzip -c > $iHS/SMARCC1.vcf.gz

## Select biallelic sites
vcftools --gzvcf $iHS/SMARCC1.vcf.gz --min-alleles 2 --max-alleles 2 --remove-indels --recode --stdout | gzip -c > $iHS/SMARCC1.filt.vcf.gz

## Make map file
plink --vcf $iHS/SMARCC1.filt.vcf.gz --recode --out $iHS/SMARCC1.filt
## Remove ped
rm $iHS/SMARCC1.filt.ped

## Add physical as genetic distance 
cat $iHS/SMARCC1.filt.map | awk '{print $1, $2, $4, $4}' > $iHS/SMARCC1.mod.map

## Subset VCF per group population
arr=(TSI IBS CEU FIN GBR)
for sel in "${arr[@]}"
do
  vcftools --gzvcf $iHS/SMARCC1.filt.vcf.gz --keep $iHS/$sel.tab --recode --stdout | gzip -c >  $iHS/SMARCC1.$sel.vcf.gz
done

## Run selScan
for sel in "${arr[@]}"
do
  selscan --ihs --vcf $iHS/SMARCC1.$sel.vcf.gz --map $iHS/SMARCC1.mod.map --out $iHS/$sel --threads 16
  ~/software/selscan/bin/linux/norm --ihs --bp-win --bins 20 --winsize 10000 --files $iHS/$sel.ihs.out
done