#'#################################################################################
#'#################################################################################
#' Prepare UMSGARD files (phs000436.v1.p1) for scoreInvHap
#'#################################################################################
#'#################################################################################

## Define vars
preproc=./results/preprocess

## Link files to folder
ln -s ~/PublicData/STUDY/Luis_Autism/phs000436.v1.p1.University_of_Miami_Study_on_Genetics_of_Autism_and_Related_Disorders/GenotypeFiles/phg000191.v1.NIMH_AutismDisorders.genotype-calls-matrixfmt.Illumina.c1.AARR/Miami_autism_genotypes.ped data/UMSGARD.ped
ln -s ~/PublicData/STUDY/Luis_Autism/phs000436.v1.p1.University_of_Miami_Study_on_Genetics_of_Autism_and_Related_Disorders/GenotypeFiles/phg000191.v1.NIMH_AutismDisorders.genotype-calls-matrixfmt.Illumina.c1.AARR/Miami_autism_genotypes.map data/UMSGARD.map
ln -s ~/PublicData/STUDY/Luis_Autism/phs000436.v1.p1.University_of_Miami_Study_on_Genetics_of_Autism_and_Related_Disorders/PhenotypeFiles/phs000436.v1.pht002833.v1.p1.Autism_Disorders_Pedigree.MULTI.txt data/UMSGARD_pedigree.txt
ln -s ~/PublicData/STUDY/Luis_Autism/phs000436.v1.p1.University_of_Miami_Study_on_Genetics_of_Autism_and_Related_Disorders/PhenotypeFiles/phs000436.v1.pht002832.v1.p1.Autism_Disorders_Subject.MULTI.txt data/UMSGARD_subject.txt

## Convert hg18 to hg19 
python ~/liftOverPlink/liftOverPlink.py --map data/UMSGARD.map --out $preproc/UMSGARD.lifted --chain ~/liftOverPlink/hg18ToHg19.over.chain.gz
cut -f 4 $preproc/UMSGARD.lifted.bed.unlifted | sed "/^#/d" > $preproc/UMSGARD.to_exclude.dat 
plink --file data/UMSGARD --recode ped --out $preproc/UMSGARD_int --exclude $preproc/UMSGARD.to_exclude.dat 
plink --ped $preproc/UMSGARD_int.ped --map $preproc/UMSGARD.lifted.map --make-bed --out $preproc/UMSGARD_noFam

## Add pedigree data
### Add family ids and remove duplicates
grep -E '20-0721085|20-0720012|20-0720004' $preproc/UMSGARD_noFam.fam | cut -f1,2 -d' ' > $preproc/dup.txt
join -1 1 -2 3 -o 1.1,1.2,2.2,2.3 <(grep -Ev '20-0721085|20-0720012|20-0720004' $preproc/UMSGARD_noFam.fam | sort -k 1,1 ) <(tail -n +12 data/UMSGARD_pedigree.txt | sed 's/\t/ /g' | sort -k 3,3) > $preproc/mapIDs.txt | 
plink --bfile $preproc/UMSGARD_noFam --remove $preproc/dup.txt --update-ids $preproc/mapIDs.txt --make-bed --out $preproc/UMSGARD_idsOK
tail -n +12 data/UMSGARD_pedigree.txt | cut -f 2-5 | grep -Pv '\t\t' > $preproc/parentID.txt 
plink --bfile $preproc/UMSGARD_idsOK --update-parents $preproc/parentID.txt --make-bed --out $preproc/UMSGARD

## Prepare files for peddy
plink --bfile $preproc/UMSGARD --recode vcf-iid bgz --out $preproc/UMSGARD
tabix -p vcf  $preproc/UMSGARD.vcf.gz

sed -i 's/ /\t/g' $preproc/UMSGARD.fam 

## Run peddy
python -m peddy -p 32 --prefix  $preproc/UMSGARD $preproc/UMSGARD.vcf.gz  $preproc/UMSGARD.fam

## Impute data ####
### All
~/data/software/imputeInversion/imputeinversion.sh -d $preproc/UMSGARD --family Yes -t 10 -i all

## Compute allele frequencies to speed up computations
study=UMSGARD
for i in `ls $preproc/${study}_imputed_files`
do 
bcftools query -f '%CHROM %POS %ID %AF\n' $preproc/${study}_imputed_files/$i/${i}_${study}_imputed_final.vcf.gz > $preproc/${study}_imputed_files/$i/${i}_${study}_imputed_final.frq
done

## Create folder to store intermediate files
mkdir $preproc/UMSGARD_pre
mv $preproc/*UMSGARD*.* $preproc/UMSGARD_pre