#'#################################################################################
#'#################################################################################
#' Prepare folder's project
#'#################################################################################
#'#################################################################################

mkdir /scratch/cruiz/Autism_Mar
cd /scratch/cruiz/Autism_Mar/
mkdir data
mkdir results
mkdir results/preprocess
mkdir results/genotypes
mkdir results/tdt
mkdir reports

## Add imputed genotypes
ln -s ~/data/CONSULTORIAS/26_19_inversions_dbGAP/results/genotypes/ data/invGenotypes
ln -s ~/data/CarlosRuiz/Inversions/Autism_Mar/results/genotypes/ data/invGenotypesMar

## Add fam files for selected studies
ln -s ~/data/CONSULTORIAS/26_19_inversions_dbGAP/results/preprocess/SSC_pre/SSC_3M.fam data/SSC_3M.fam
ln -s ~/data/CONSULTORIAS/26_19_inversions_dbGAP/results/preprocess/SSC_pre/SSC_1M.fam data/SSC_1M.fam
ln -s ~/data/CONSULTORIAS/26_19_inversions_dbGAP/results/preprocess/SSC_pre/SSC_Omni.fam data/SSC_Omni.fam
ln -s ~/data/CONSULTORIAS/26_19_inversions_dbGAP/results/preprocess/IMAGE_pre/IMAGE.fam data/IMAGE.fam
ln -s ~/data/CONSULTORIAS/26_19_inversions_dbGAP/results/preprocess/SHARP_pre/SHARP.fam data/SHARP.fam
ln -s ~/data/CONSULTORIAS/26_19_inversions_dbGAP/results/preprocess/AGP_pre/AGP.fam data/AGP.fam 
ln -s ~/data/CarlosRuiz/Inversions/Autism_Mar/results/preprocess/UMSGARD_pre/UMSGARD.fam data/UMSGARD.fam
ln -s ~/data/CarlosRuiz/Inversions/Autism_Mar/results/preprocess/GEDI_pre/GEDI.fam data/GEDI.fam

## Add ancestry files for selected studies
ln -s ~/data/CONSULTORIAS/26_19_inversions_dbGAP/results/preprocess/AGP_pre/AGP.het_check.csv data/AGP_ancestry.csv 
ln -s ~/data/CONSULTORIAS/26_19_inversions_dbGAP/results/preprocess/SSC_pre/SSC_3M.het_check.csv data/SSC_3M_ancestry.csv 
ln -s ~/data/CONSULTORIAS/26_19_inversions_dbGAP/results/preprocess/SSC_pre/SSC_1M.het_check.csv data/SSC_1M_ancestry.csv 
ln -s ~/data/CONSULTORIAS/26_19_inversions_dbGAP/results/preprocess/SSC_pre/SSC_Omni.het_check.csv data/SSC_Omni_ancestry.csv 
ln -s ~/data/CONSULTORIAS/26_19_inversions_dbGAP/results/preprocess/IMAGE_pre/IMAGE.het_check.csv data/IMAGE_ancestry.csv 
ln -s ~/data/CONSULTORIAS/26_19_inversions_dbGAP/results/preprocess/SHARP_pre/SHARP.het_check.csv data/SHARP_ancestry.csv 
ln -s ~/data/CarlosRuiz/Inversions/Autism_Mar/results/preprocess/UMSGARD_pre/UMSGARD.het_check.csv data/UMSGARD_ancestry.csv
ln -s ~/data/CarlosRuiz/Inversions/Autism_Mar/results/preprocess/GEDI_pre/GEDI.het_check.csv data/GEDI_ancestry.csv


## Sync with data folder
rsync -zavh /scratch/cruiz/Autism_Mar ~/data/CarlosRuiz/Inversions/
