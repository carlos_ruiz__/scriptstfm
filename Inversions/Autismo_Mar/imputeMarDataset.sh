#'#################################################################################
#'#################################################################################
#' Impute dataset from MAR
#'#################################################################################
#'#################################################################################

## Define vars
preproc=./results/preprocess

# Convert to plink
plink --vcf data/RNAseq_reg8.final.vcf.gz --const-fid  --make-bed --out $preproc/marData

## Impute data ####
~/data/software/imputeInversion/imputeinversion.sh -d $preproc/marData -t 10 -i inv8_001

## Compute allele frequencies to speed up computations
study=GEDI.EUR
study=GEDI
for i in `ls $preproc/${study}_imputed_files`
do 
bcftools query -f '%CHROM %POS %ID %AF\n' $preproc/${study}_imputed_files/$i/${i}_${study}_imputed_final.vcf.gz > $preproc/${study}_imputed_files/$i/${i}_${study}_imputed_final.frq
done

## Create folder to store intermediate files
mkdir $preproc/GEDI_pre
mv $preproc/*GEDI*.* $preproc/GEDI_pre