#'#################################################################################
#'#################################################################################
#' Prepare GEDI files (phs000620.v1.p1) for scoreInvHap
#'#################################################################################
#'#################################################################################

## Define vars
preproc=./results/preprocess

## Link files to folder
ln -s ~/PublicData/STUDY/dbGaP/RootStudyConsentSet_phs000620.GEDI_SubstanceAbuse.v1.p1.c1.GRU/GenotypeFiles/phg000485.v1.GEDI_SubstanceAbuse.genotype-calls-matrixfmt.Human660W-Quad_v1_A.c1.GRU.tar.gz data/GEDI.tar.gz

## Extract and decompress files
tar zxvf data/GEDI.tar.gz -C $preproc

## Convert hg18 to hg19 
plink --bfile $preproc/MCTFR_clean/MCTFR_clean --output-chr MT --recode ped --out $preproc/GEDI
python ~/liftOverPlink/liftOverPlink.py --map $preproc/GEDI.map --out $preproc/GEDI.lifted --chain ~/liftOverPlink/hg18ToHg19.over.chain.gz
cut -f 4 $preproc/GEDI.lifted.bed.unlifted | sed "/^#/d" > $preproc/GEDI.to_exclude.dat 
plink --file $preproc/GEDI --recode ped --out $preproc/GEDI_int --exclude $preproc/GEDI.to_exclude.dat 
plink --ped $preproc/GEDI_int.ped --map $preproc/GEDI.lifted.map --make-bed --out $preproc/GEDI

## Prepare files for peddy
plink --bfile $preproc/GEDI --recode vcf-iid bgz --out $preproc/GEDI
tabix -p vcf  $preproc/GEDI.vcf.gz

sed -i 's/ /\t/g' $preproc/GEDI.fam 

## Run peddy
python -m peddy -p 32 --prefix  $preproc/GEDI $preproc/GEDI.vcf.gz  $preproc/GEDI.fam

## Seleccionar Individuos Europeos
awk '$16 == "EUR" && $17 >= 0.9 {print $1}' FS="," $preproc/GEDI.het_check.csv > $preproc/GEDI.EUR.txt
for i in $(cat $preproc/GEDI.EUR.txt)
do
  cut -f1,2 $preproc/GEDI.fam | grep -w "$i"  - >> $preproc/GEDI.include
done
plink --bfile $preproc/GEDI --keep $preproc/GEDI.include --make-bed --out $preproc/GEDI.EUR

## Seleccionar Individuos Europeos sin probabilidad
awk '$16 == "EUR" {print $1}' FS="," $preproc/GEDI.het_check.csv > $preproc/GEDI.EUR2.txt
for i in $(cat $preproc/GEDI.EUR2.txt)
do
  cut -f1,2 $preproc/GEDI.fam | grep -w "$i"  - >> $preproc/GEDI.include2
done
plink --bfile $preproc/GEDI --keep $preproc/GEDI.include2 --make-bed --out $preproc/GEDI.EUR2



## Impute data ####
### European
~/data/software/imputeInversion/imputeinversion.sh -d $preproc/GEDI.EUR --family Yes -t 10 -i all

### All
~/data/software/imputeInversion/imputeinversion.sh -d $preproc/GEDI --family Yes -t 10 -i all

## Compute allele frequencies to speed up computations
study=GEDI.EUR
study=GEDI
for i in `ls $preproc/${study}_imputed_files`
do 
bcftools query -f '%CHROM %POS %ID %AF\n' $preproc/${study}_imputed_files/$i/${i}_${study}_imputed_final.vcf.gz > $preproc/${study}_imputed_files/$i/${i}_${study}_imputed_final.frq
done

## Create folder to store intermediate files
mkdir $preproc/GEDI_pre
mv $preproc/*GEDI*.* $preproc/GEDI_pre