#'#################################################################################
#' Impute alzheimer region ####
#'#################################################################################

mkdir Alzheimer
mkdir Alzheimer/data
cd Alzheimer/data


## Copy Genotype data
cp ~/data/PublicData/STUDY/dbGaP/Alzheimer/phs000372.v1.p1_ADGC_Alzheimer/PhenoGenotypeFiles/RootStudyConsentSet_phs000372.AlzheimersDiseaseGeneticsConsortium.v1.p1.c1.ADR/GenotypeFiles/phg000339.v1.NIA_AlzheimersDiseaseGeneticsConsortium-Human660W-Quad_v1_A.genotype-calls-matrixfmt.c1.ADR/ADC* .
cp ~/data/PublicData/STUDY/dbGaP/Alzheimer/phs000372.v1.p1_ADGC_Alzheimer/PhenoGenotypeFiles/RootStudyConsentSet_phs000372.AlzheimersDiseaseGeneticsConsortium.v1.p1.c1.ADR/GenotypeFiles/phg000339.v1.NIA_AlzheimersDiseaseGeneticsConsortium-HumanOmniExpress.genotype-calls-matrixfmt.c1.ADR/ADC3_full_062513.fwd.hg19.* .
cp ~/PublicData/STUDY/dbGaP/Alzheimer/phs000372.v1.p1_ADGC_Alzheimer/PhenoGenotypeFiles/RootStudyConsentSet_phs000372.AlzheimersDiseaseGeneticsConsortium.v1.p1.c1.ADR/PhenotypeFiles/phs000372.v1.pht002856.v1.p1.ADGC_Subject.MULTI.txt.gz .
cp ~/PublicData/STUDY/dbGaP/Alzheimer/phs000372.v1.p1_ADGC_Alzheimer/PhenoGenotypeFiles/RootStudyConsentSet_phs000372.AlzheimersDiseaseGeneticsConsortium.v1.p1.c1.ADR/PhenotypeFiles/phs000372.v1.pht002858.v1.p1.c1.ADGC_Subject_Phenotypes.ADR.txt.gz .

gzip -d phs000372.v1.pht002856.v1.p1.ADGC_Subject.MULTI.txt.gz
gzip -d phs000372.v1.pht002858.v1.p1.c1.ADGC_Subject_Phenotypes.ADR.txt.gz

mkdir results
mkdir results/preprocessedData

ADC1=data/ADC1_full_062513.fwd.hg19.r
ADC2=data/ADC2_full_062513.fwd.hg19
ADC3=data/ADC3_full_062513.fwd.hg19
preproc=results/preprocessedData

# Filter European ####
## Prepare files for peddy
### ADC1
plink --bfile $ADC1 --recode vcf-iid bgz --out $preproc/ADC1
tabix -p vcf $preproc/ADC1.vcf.gz

#el fam hay que cambiarle el " " por tab.
sed 's/ /\t/g' $ADC1.fam > $preproc/ADC1_mod.fam


### ADC2
plink --bfile $ADC2 --recode vcf-iid bgz --out $preproc/ADC2
tabix -p vcf $preproc/ADC2.vcf.gz

#el fam hay que cambiarle el " " por tab.
sed 's/ /\t/g' $ADC2.fam > $preproc/ADC2_mod.fam

## ADC3
plink --bfile $ADC3 --recode vcf-iid bgz --out $preproc/ADC3
tabix -p vcf $preproc/ADC3.vcf.gz

#el fam hay que cambiarle el " " por tab.
sed 's/ /\t/g' $ADC3.fam > $preproc/ADC3_mod.fam

## Run peddy
cd $preproc

python -m peddy -p 32 --prefix ADC1 ADC1.vcf.gz ADC1_mod.fam
python -m peddy -p 32 --prefix ADC2 ADC2.vcf.gz ADC2_mod.fam
python -m peddy -p 32 --prefix ADC3 ADC3.vcf.gz ADC3_mod.fam


## Remove non-European individuals
removeEur()
{
  plink=$1
  prefix=$2
  
  shift; shift;
  
  awk '$16 == "EUR" && $17 >= 0.9 {print $1}' FS="," $preproc/$prefix.het_check.csv > results/preprocessedData/$prefix.EUR.txt
  
  for i in $(cat $preproc/$prefix.EUR.txt)
  do
    grep $i $plink.fam | awk '{print $1, $2}' >> $preproc/$prefix.include
  done

  plink --bfile $plink --keep $preproc/$prefix.include --make-bed --out $preproc/${prefix}_EUR
}

removeEur $ADC1 ADC1
removeEur $ADC2 ADC2
removeEur $ADC3 ADC3

# Run imputeInversion
## Define region using PMID: 30617256
### chr19:45251156-45412955 - Largest range between independent lead SNPs in APOE region (hg19)
### Increase region 100Kb per band

echo "19:45151156-45512955" > $preproc/range.txt

cd $preproc

~/data/software/imputeInversion-master/imputeinversion_v2.sh -d ADC1_EUR -k yes -i range.txt -t 40
~/data/software/imputeInversion-master/imputeinversion_v2.sh -d ADC2_EUR -k yes -i range.txt -t 40
~/data/software/imputeInversion-master/imputeinversion_v2.sh -d ADC3_EUR -k yes -i range.txt -t 40

### chr8:27195121-27464929 - 2nd Largest range between independent lead SNPs in CLU region (hg19)
### Increase region 100Kb per band

echo "8:27095121-27564929" > $preproc/rangeCLU.txt

cd $preproc

~/data/software/imputeInversion-master/imputeinversion_v2.sh -d ADC1_EUR -i rangeCLU.txt -t 40
~/data/software/imputeInversion-master/imputeinversion_v2.sh -d ADC2_EUR -i rangeCLU.txt -t 40
~/data/software/imputeInversion-master/imputeinversion_v2.sh -d ADC3_EUR -i rangeCLU.txt -t 40

### chr21:27100000-27800000 - APP region - involved in Alzheimer but without GWAS hits
### Increase region 100Kb per band
cd $preproc

runImputation()
{
  path=~/data/software/imputeInversion
  source $path/prephasing.sh
  source $path/phasing_shapeit.sh
  source $path/minimac3_imputation.sh
  source $path/postimputation.sh

  data=$1
  prefix=$2
  chr=$3
  start=$4
  end=$5
  HRCref=$6
  refsFolder=$7
  minimacRefs=$8
  cpus=$9
  family=$10
  
  dir=`dirname $data`
  base=`basename $data`
  
  
  ## Run prephasing in all Chromosomes
  prephase $data $chr $HRCref 
  
  echo "Start phasing"
  if [ ! -d $dir/phased_files ]; then
    mkdir $dir/phased_files # Folder to store all the files generated during phasing process
  fi   
  
  ## Apply phasing to each chromosome
  phase $chr $dir $base $refsFolder $cpus $family

  if [ ! -d $dir/phased_files/shapeit ]; then
    mkdir $dir/phased_files/shapeit #to store all shapeit intermediate files
  fi

  if [ ! -d $dir/pimputed_files ]; then
    mkdir $dir/pimputed_files # Folder to store all the files generated during phasing process
  fi
  
  if [ ! -d $dir/postimputation_files ]; then
    mkdir $dir/postimputation_files # Folder to store all the files generated during the postimputation process
  fi
  
  if [ ! -d $dir/${base}_imputed_files ]; then
    mkdir $dir/${base}_imputed_files # Folder to store the final files (this one will NOT be removed)
  fi

  impute $chr $dir $base $prefix $start $end $minimacRefs $cpus
  if [[ $? == 0 ]]; then ## If inversion worked, marked
    postimpute $chr $dir $base $prefix $cpus $annotRef
    if [[ $? == 0 ]]; then ## If inversion worked, marked
      g=$(($g + 1))
    fi
  fi
  if [[ $g == 1 ]]; then 
    # By default, the intermediate files will be deleted if all inversion worked
    if [[ -z "$keep_files" ]] || [[ $keep_files != Yes ]] && [[ $keep_files != YES ]] && [[ $keep_files != yes ]] # If user did not indicate 'YES' to keep the intermediate files 
    then
      rm -rf $dir/prephasing_files # Remove folder containing pre-phasing files (remove files at this point to avoid having to many files at the end of the process if we want to impute all the chromosomes and cause potential memory problems) 
      rm -rf $dir/phased_files # Remove folder containing phased files 
      rm -rf $dir/pimputed_files # Remove folder containing imputed files
      rm -rf $dir/postimputation_files # Remove folder containing postimputed files (intermediate files between the imputed ones and the final ones with the correct ids)
    fi
  fi
}
refs=~/PublicData/REFERENCES/reference_panels/
runImputation ADC1_EUR APP 21 27100000 27800000 \
  $refs/1000GP_Phase3_combined.legend $refs $refs/ALL.chr21.phase3_v5.shapeit2_mvncall_integrated.noSingleton.genotypes.vcf.gz \
  40 NO
~/data/software/imputeInversion/imputeinversion.sh -d ADC2_EUR -i rangeAPP.txt -t 40
~/data/software/imputeInversion/imputeinversion.sh -d ADC3_EUR -i rangeAPP.txt -t 40

