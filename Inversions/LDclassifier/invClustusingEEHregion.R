#'#################################################################################
#'#################################################################################
#' Run invClust on 1000 Genomes in inv8p23 EEH region
#' Run in /SYNCRW10125/DATASETS/STUDY/1000GENOME/VCF
#'#################################################################################
#'#################################################################################

# Load packages and scripts  ####
#'#################################################################################
library(invClust)
library(GenomicRanges)
library(VariantAnnotation)
library(parallel)

source("/home/cruiz/InversionSequencing/InversionNGSutils.R")
load("/SYNCRW10125/DATASETS/STUDY/1000GENOME/Samples_Pop1GK.Rdata")

range <- GRanges("8:9000000-10250000") ## Region defined in Mohajeri et al, 2016
names(range) <- "inv8"

# Select samples by population  ####
#'#################################################################################

AFR <- rownames(samp_pop)[samp_pop$superpop == "AFR"]
AMR <- rownames(samp_pop)[samp_pop$superpop == "AMR"]
EAS <- rownames(samp_pop)[samp_pop$superpop == "EAS"]
SAS <- rownames(samp_pop)[samp_pop$superpop == "SAS"]
EUR <- rownames(samp_pop)[samp_pop$superpop == "EUR"]

pops <- list(EUR = EUR, AFR = AFR, AMR = AMR, EAS = EAS, SAS = SAS)

# Run invClust ####
#'#################################################################################

# Chr per pop
invhapChrpops <- lapply(pops, function(pop) computeInvClustChr(range, pop))

# geno per pop
invgenopops <- lapply(pops, function(pop) computeInvClust(range, pop))

# Chr All
invhapChrAll <- computeInvClustChr(range, rownames(samp_pop))

# Geno All
invgenoAll <-  computeInvClust(range, rownames(samp_pop))

save(invhapChrpops, invgenopops, invhapChrAll, invgenoAll, 
     file = "/home/cruiz/InversionSequencing/SNPclassifier/EEHinvClustObjects.Rdata")

# Classify haplotypes in invhapChrAll using mclust ####
#'#################################################################################
library(mclust)
class1 <- factor(Mclust(invhapChrAll$datin$y[, 1], 2)$classification, labels = c("I", "N"))
class2 <- factor(Mclust(invhapChrAll$datin$y[, 1:2], 2)$classification, labels = c("I", "N"))
class5 <- factor(Mclust(invhapChrAll$datin$y, 2)$classification, labels = c("I", "N"))
classman <- factor(
  ifelse(invhapChrAll$datin$y[, 2] > - 0.8*invhapChrAll$datin$y[, 1], "I", "N"))
names(class1) <- names(class2) <- names(class5) <- names(classman) <- invhapChrAll$datin$ids

# Plots ####
#'#################################################################################
setwd("/home/cruiz/InversionSequencing/SNPclassifier")


pdf("invClustEEHchrpops.pdf")
lapply(names(invhapChrpops), function(x){
  plotInv(invhapChrpops[[x]], title = x, classification = invGenotypes(invhapChrpops[[x]]))
  plotInv(invhapChrpops[[x]], title = paste(x, "1 comp"), classification = class1[invhapChrpops[[x]]$datin$ids])
  plotInv(invhapChrpops[[x]], title = paste(x, "2 comp"), classification = class2[invhapChrpops[[x]]$datin$ids])
  plotInv(invhapChrpops[[x]], title = paste(x, "5 comp"), classification = class5[invhapChrpops[[x]]$datin$ids])
  plotInv(invhapChrpops[[x]], title = paste(x, "manual class"), classification = classman[invhapChrpops[[x]]$datin$ids])
  })
dev.off()


pdf("invClustEEHgenopops.pdf")
lapply(names(invgenopops), function(x){
  plotInv(invgenopops[[x]], title = x, classification = invGenotypes(invgenopops[[x]]))
})
dev.off()

pdf("invClustEEHchrAll.pdf")
plotInv(invhapChrAll, title = "EEH haplos", classification = invGenotypes(invhapChrAll))
plotInv(invhapChrAll, title = "Populations", 
        classification = factor(samp_pop[substring(invhapChrAll$datin$ids, 1, 7), "superpop"]))
plotInv(invhapChrAll, title = "Mclust 1 comp", classification = class1)
plotInv(invhapChrAll, title = "Mclust 2 comp", classification = class2)
plotInv(invhapChrAll, title = "Mclust 5 comp", classification = class5)
plotInv(invhapChrAll, title = "manual class", classification = classman)
dev.off()

pdf("invClustEEHgenoAll.pdf")
plotInv(invgenoAll, title = "EEH genos", classification = invGenotypes(invgenoAll))
plotInv(invgenoAll, title = "Populations", 
        classification = factor(samp_pop[substring(invgenoAll$datin$ids, 1, 7), "superpop"]))
dev.off()

haplosAll <- invGenotypes(invhapChrAll)

save(class1, class2, haplosAll, file = "/home/cruiz/InversionSequencing/SNPclassifier/EEHhaplos.Rdata")

 

# Compare classification with invClust whole region ####
#'#################################################################################
load("/home/cruiz/InversionSequencing/SandersROIs/invClust/invClustALL_final.Rdata")

inds1 <- classifInds(class1)
inds2 <- classifInds(class2)
indsman <- classifInds(classman)
inv <- invsGenoFinal$ROIno.8.3

table(haplo = inds1, geno = inv[names(inds1)])
# geno
# haplo   I/I NI/I NI/NI
# NI/NI 182   96   279
# NI/I  185  599   330
# I/I   130  273   430

table(haplo = inds2, geno = inv[names(inds2)])
# geno
# haplo   I/I NI/I NI/NI
# NI/NI 415  279   518
# NI/I   33  654   204
# I/I    49   35   317

table(haplo = indsman, geno = inv[names(indsman)])
# geno
# haplo   I/I NI/I NI/NI
# NI/NI  79  229   565
# NI/I   61  641    35
# I/I   357   98   439


lapply(pops, function(x){
  table(haplo = inds2[x], geno = inv[x])
})
# $EUR
# geno
# haplo   I/I NI/I NI/NI
# NI/NI  89   20     0
# NI/I    5  206    29
# I/I     0    9   145
# 
# $AFR
# geno
# haplo   I/I NI/I NI/NI
# NI/NI  43   99    59
# NI/I    7  160   143
# I/I     0   15   135
# 
# $AMR
# geno
# haplo   I/I NI/I NI/NI
# NI/NI   1   23   138
# NI/I   15  119     2
# I/I    48    1     0
# 
# $EAS
# geno
# haplo   I/I NI/I NI/NI
# NI/NI   3   71   313
# NI/I    0   16     2
# I/I     0    0     0
# 
# $SAS
# geno
# haplo   I/I NI/I NI/NI
# NI/NI 279   66     8
# NI/I    6  153    28
# I/I     1   10    37

lapply(pops, function(x){
  table(haplo = indsman[x], geno = inv[x])
})
# $EUR
# geno
# haplo   I/I NI/I NI/NI
# NI/NI   0   23   170
# NI/I   18  207     4
# I/I    76    5     0
# 
# $AFR
# geno
# haplo   I/I NI/I NI/NI
# NI/NI  16  162   333
# NI/I   20  112     4
# I/I    14    0     0
# 
# $AMR
# geno
# haplo   I/I NI/I NI/NI
# NI/NI  62   13     0
# NI/I    2  122    11
# I/I     0    8   129
# 
# $EAS
# geno
# haplo   I/I NI/I NI/NI
# NI/NI   0    0     0
# NI/I    2   20     5
# I/I     1   67   310
# 
# $SAS
# geno
# haplo   I/I NI/I NI/NI
# NI/NI   1   31    62
# NI/I   19  180    11
# I/I   266   18     0

