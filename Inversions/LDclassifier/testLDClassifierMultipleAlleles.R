#'#################################################################################
#'#################################################################################
#' Run LDclassifier in multi allele inversions
#' Run in /DATA/DATAW5/Carlos/Inversions/LDClassifier
#'#################################################################################
#'#################################################################################

## Out of R
plink --vcf ALL.chr7.phase3_shapeit2_mvncall_integrated_v5a.20130502.genotypes.vcf.gz --keep EUR2.txt --indep-pairwise 50 5 0.4 --out inv7EURR0.4
plink --vcf ALL.chrX.phase3_shapeit2_mvncall_integrated_v1b.20130502.genotypes.vcf.gz --keep EUR2.txt --indep-pairwise 50 5 0.4 --out invXEURR0.4
plink --vcf ALL.chrX.phase3_shapeit2_mvncall_integrated_v1b.20130502.genotypes.vcf.gz --keep EUR2.txt --indep-pairwise 50 5 0.8 --out invXEURR0.8



#'#################################################################################
# Load and prepare data  ####
#'#################################################################################

# Load packages and scripts  ####
#'#################################################################################
library(GenomicRanges)
library(VariantAnnotation)
library(BiocParallel)
library(snpStats)
library(gtools)
library(LDclassifier)
library(invClust)

source("~/InversionNGSutils.R")
load("Samples_Pop1GK.Rdata")
load("invFestgenos.Rdata")



# Load SNP data  ####
#'#################################################################################
ranges <- GRanges(c("7:54290974-54386821", "X:72215927-72306774"))
names(ranges) <- c("7", "X")

EUR <- rownames(samp_pop)[samp_pop$superpop == "EUR"]

VCF_chr7 <- getVCFmatrixChr(ranges[1], EUR, minmaf = 0.05)
VCF_chrX <- getVCFmatrixChr(ranges[2], EUR, 
                         vcffile = "ALL.chrX.phase3_shapeit2_mvncall_integrated_v1b.20130502.genotypes.vcf.gz", 
                         minmaf = 0.05)


### Prune SNPs ####
#'#################################################################################
snps7 <- read.delim("inv7EURR0.4.prune.in", header = FALSE, as.is = TRUE)
VCF_chr7$genotypes <- VCF_chr7$genotypes[, colnames(VCF_chr7$genotypes) %in% snps7[, 1]]
VCF_chr7$map <- VCF_chr7$map[colnames(VCF_chr7$genotypes), ]
GRchr7 <- makeGRangesFromDataFrame(VCF_chr7$map, start.field = "position", 
                                      end.field = "position")

snpsX <- read.delim("invXEURR0.4.prune.in", header = FALSE, as.is = TRUE)
VCF_chrX$genotypes <- VCF_chrX$genotypes[, colnames(VCF_chrX$genotypes) %in% snpsX[, 1]]
VCF_chrX$map <- VCF_chrX$map[colnames(VCF_chrX$genotypes), ]
GRchrX <- makeGRangesFromDataFrame(VCF_chrX$map, start.field = "position", 
                                   end.field = "position")


# Run method ####
#'#################################################################################
models7 <- runLDclassifier(as(VCF_chr7$genotypes, "numeric")/2, 
                           GRchr7, BPPARAM = MulticoreParam(15))

indsmat7 <- do.call(cbind, lapply(models7, `[[`, "r1"))
pc7 <- prcomp(indsmat7)
pc7mat <- pc7$x
rownames(pc7mat) <- rownames(VCF_chr7$genotypes)
ind_ids7 <- rownames(VCF_chr7$genotypes)
  
# Filter second chromosome in male samples 
matX <- as(VCF_chrX$genotypes, "numeric")/2
matX <- matX[rowMeans(!is.na(matX)) == 1, ]

modelsX <- runLDclassifier(matX, GRchrX, BPPARAM = MulticoreParam(15))

indsmatX <- do.call(cbind, lapply(modelsX, `[[`, "r1"))
pcX <- prcomp(indsmatX)
pcXmat <- pcX$x
rownames(pcXmat) <- rownames(matX)
ind_idsX <- rownames(matX)


save(pcX, pc7, models7, modelsX, ind_ids7, ind_idsX, file = "MultipleHaplosResultsEUR.Rdata")


inv7_genos <- genos[[1]]
inv7_genos <- inv7_genos[inv7_genos != "ND"]
inv7_chr <- factor(c(inv7_genos, inv7_genos))
names(inv7_chr) <- paste(names(inv7_chr), rep(1:2, each = length(inv7_genos)), sep = "_")
comm <- intersect(rownames(pc7mat), names(inv7_chr))


invX_genos <- genos[[2]]
invX_genos <- invX_genos[invX_genos != "ND"]
invX_genos[invX_genos == "STD"] <- "STD/STD"
invX_genos[invX_genos == "INV"] <- "INV/INV"

invX_chr <- factor(c(invX_genos, invX_genos))
names(invX_chr) <- paste(names(invX_chr), rep(1:2, each = length(invX_genos)), sep = "_")
commX <- intersect(rownames(pcXmat), names(invX_chr))

pdf("InvMultipleHaplosR2.04.pdf")
plot(pc7$x, main = "inversion 7")
points(pc7mat[comm, ], col = as.numeric(inv7_chr[comm]), pch = 16)

plot(pcX$x, main = "inversion X")
points(pcXmat[commX, ], col = as.numeric(invX_chr[commX]), pch = 16)
dev.off()


## Plot with inferred genos
inv7_genos_inf <- invFestGenotypes$HsInv0286
inv7_chr_inf <- factor(c(inv7_genos_inf, inv7_genos_inf))
names(inv7_chr_inf) <- paste(names(inv7_chr_inf), rep(1:2, each = length(inv7_genos_inf)), sep = "_")
comm_inf <- intersect(rownames(pc7mat), names(inv7_chr_inf))

invX_genos_inf <- invFestGenotypes$HsInv0396
invX_chr_inf <- factor(c(invX_genos_inf, invX_genos_inf))
names(invX_chr_inf) <- paste(names(invX_chr_inf), rep(1:2, each = length(invX_genos_inf)), sep = "_")
comm_infX <- intersect(rownames(pcXmat), names(invX_chr_inf))


pdf("InvMultipleHaplosInferredGenosR2.04.pdf")
plot(pc7mat[comm_inf, ], main = "inversion 7", col = as.numeric(inv7_chr_inf[comm_inf]), pch = 16)

plot(pcXmat[comm_infX, ], main = "inversion X", col = as.numeric(invX_chr_inf[comm_infX]), pch = 16)
dev.off()


## Rerun inversion X with softer pruning
snpsX <- read.delim("invXEURR0.8.prune.in", header = FALSE, as.is = TRUE)
VCF_chrX$genotypes <- VCF_chrX$genotypes[, colnames(VCF_chrX$genotypes) %in% snpsX[, 1]]
VCF_chrX$map <- VCF_chrX$map[colnames(VCF_chrX$genotypes), ]
GRchrX <- makeGRangesFromDataFrame(VCF_chrX$map, start.field = "position", 
                                   end.field = "position")

matXb <- as(VCF_chrX$genotypes, "numeric")/2
matXb <- matXb[rowMeans(!is.na(matXb)) == 1, ]

modelsXb <- runLDclassifier(matXb, GRchrX, BPPARAM = MulticoreParam(15))

indsmatXb <- do.call(cbind, lapply(modelsXb, `[[`, "r1"))
pcXb <- prcomp(indsmatXb)
pcXbmat <- pcXb$x
rownames(pcXbmat) <- rownames(matXb)

pdf("InvMultipleHaplosInferredGenosR2.04.pdf")
plot(pc7mat[comm_inf, ], main = "inversion 7", col = as.numeric(inv7_chr_inf[comm_inf]), pch = 16)

plot(pcXmat[comm_infX, ], main = "inversion X", col = as.numeric(invX_chr_inf[comm_infX]), pch = 16)

plot(pcXbmat[comm_infX, ], main = "inversion X - R.2 = 0.8", col = as.numeric(invX_chr_inf[comm_infX]), pch = 16)

dev.off()

pdf("InvMultipleHaplosR2.04.pdf")
plot(pc7$x, main = "inversion 7")
points(pc7mat[comm, ], col = as.numeric(inv7_chr[comm]), pch = 16)

plot(pcX$x, main = "inversion X")
points(pcXmat[commX, ], col = as.numeric(invX_chr[commX]), pch = 16)

plot(pcXb$x, main = "inversion X - R2 = 0.8")
points(pcXbmat[commX, ], col = as.numeric(invX_chr[commX]), pch = 16)

dev.off()

save(pcX, pcXb, modelsXb, pc7, models7, modelsX, file = "MultipleHaplosResults.Rdata")

