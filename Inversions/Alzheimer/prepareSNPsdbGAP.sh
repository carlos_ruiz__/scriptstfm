#'#################################################################################
#'#################################################################################
#' Change assembly of SNPs and impute in Michigan server
#'#################################################################################
#'#################################################################################


#'#################################################################################
# phs000168.v2 ####
#'#################################################################################
plink --bfile LOAD-610K_FORWARD_strand_consent_1 --recode --out phs000168

sed 's/^23/X/' phs000168.map  > phs00016818.map 
cp phs000168.ped phs00016818.ped

# Convert hg18 to hg19
python /home/isglobal.lan/cruiz/liftOverPlink/liftOverPlink.py --map phs00016818.map --out lifted --chain ~/liftOverPlink/hg18ToHg19.over.chain.gz
cut -f 4 lifted.bed.unlifted | sed "/^#/d" > to_exclude.dat 
plink --file phs00016818 --recode ped --out lifted2 --exclude to_exclude.dat 
plink --ped lifted2.ped --map lifted.map --make-bed --out final

## Create files for peddy
plink --bfile final --recode vcf-iid bgz --out merged
tabix -p vcf merged.vcf.gz

## Run peddy
python -m peddy --prefix alz merged.vcf.gz peddy.fam

# Create files for imputation
plink --bfile final --freq --out final
perl5.26.1 ~/HRC-1000G-check-bim.pl -b final.bim -f final.frq -r ../HRC.r1-1.GRCh37.wgs.mac5.sites.tab -h
## Execute first lines of Run_plink.sh

## Remove samples with bad sex determination
plink --bfile final-updated --check-sex

#### R
a <- read.table("plink.sexcheck", header = T)
write.table(a[a$STATUS == "PROBLEM", 1:2], row.names = FALSE, col.names = FALSE, quote = FALSE, file = "ExcludeSex.txt")
####

## Create files for imputation
array=(1 2 3 6 7 8 16 17 21 X)
for i in "${array[@]}"
do
plink --bfile final-updated --chr $i --remove ExcludeSex.txt --chr-output M --set-hh-missing --recode vcf-iid --out AZH_phs000168-chr$i
vcf-sort AZH_phs000168-chr$i.vcf | bgzip -c > AZH_phs000168-chr$i.recode.vcf.gz
done


## Post process files
array=(1 2 3 6 7 8 16 17 21)
for i in "${array[@]}"
do
zgrep "##" chr$i.dose.vcf.gz > header.hdr
cat ../../filter.txt >> header.hdr
bcftools annotate --annotations ../../HRC.r1-1.GRCh37.wgs.mac5.sites.tab.gz --columns CHROM,POS,ID,REF,ALT \
--output chr$i.dose.idsrs.vcf.gz --output-type z chr$i.dose.vcf.gz -h header.hdr
tabix -p vcf chr$i.dose.idsrs.vcf.gz
done

array=(male female)
for i in "${array[@]}"
do
zgrep "##" chrX.no.auto_$i.dose.vcf.gz > header.hdr
cat ../../filter.txt >> header.hdr
bcftools annotate --annotations ../../HRC.r1-1.GRCh37.wgs.mac5.sites.tab.gz --columns CHROM,POS,ID,REF,ALT \
--output chrX.$i.dose.idsrs.vcf.gz --output-type z chrX.no.auto_$i.dose.vcf.gz -h header.hdr
tabix -p vcf chrX.$i.dose.idsrs.vcf.gz
done


#'#################################################################################
# phs000219.v1 ####
#'#################################################################################
plink --bfile GWAS_Statistics_plink --recode --out phs000219

sed 's/^23/X/' phs000219.map  > phs00021918.map 
cp phs000219.ped phs00021918.ped

# Convert hg18 to hg19
python /home/isglobal.lan/cruiz/liftOverPlink/liftOverPlink.py --map phs00021918.map --out lifted --chain ~/liftOverPlink/hg18ToHg19.over.chain.gz
cut -f 4 lifted.bed.unlifted | sed "/^#/d" > to_exclude.dat 
plink --file phs00021918 --recode ped --out lifted2 --exclude to_exclude.dat 
plink --ped lifted2.ped --map lifted.map --make-bed --out final

## Create files for peddy
plink --bfile final --recode vcf-iid bgz --out merged
tabix -p vcf merged.vcf.gz

# Change annotation
bcftools annotate --annotations ~/data/PROJECTS/cruizPhD/Imputation/HRC.r1-1.GRCh37.wgs.mac5.sites.tab.gz --columns CHROM,POS,ID \
--output merged.annotated.vcf.gz --output-type z merged.vcf.gz

plink --vcf merged.annotated.vcf.gz --make-bed --out finalannotated

## Run peddy
python -m peddy --prefix alz merged.vcf.gz peddy.fam

# Create files for imputation
plink --bfile finalannotated --freq --out finalannotated
perl5.26.1 ~/HRC-1000G-check-bim.pl -b finalannotated.bim -f finalannotated.frq -r ../HRC.r1-1.GRCh37.wgs.mac5.sites.tab -h

## Execute first lines of Run_plink.sh
## Create files for imputation
array=(1 2 3 6 7 8 16 17 21 X)
for i in "${array[@]}"
do
plink --bfile final-updated --chr $i --chr-output M --set-hh-missing --recode vcf-iid --out AZH_phs000168-chr$i
vcf-sort AZH_phs000168-chr$i.vcf | bgzip -c > AZH_phs000168-chr$i.recode.vcf.gz
done
