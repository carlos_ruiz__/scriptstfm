#' Script To Download WGS data from HapMap

library(data.table)
dat <- fread("/SYNCRW10125/DATASETS/STUDY/1000GENOME/1000Genomes.Sequence")

FTPpath <- "ftp://ftp.1000genomes.ebi.ac.uk/vol1/ftp/phase3/"

### Filter WGS
dat2 <- dat[ANALYSIS_GROUP %in% c("low coverage", "high coverage")]

## Filter EUROPEAN
dat3 <- dat2[POPULATION %in% c("GBR", "FIN", "TSI", "IBS", "CEU")]

## Remove Withdrawn
dat4 <- as.data.frame(dat3)
dat4 <- dat4[!dat4$WITHDRAWN, ]

runsid <- unique(substring(dir(pattern = ".fq.gz"), 1, 9))

## Exclude already downloaded
dat5 <- dat4[!dat4$RUN_ID %in% runsid, ]

## Download TSI
TSI <- dat5[dat5$POPULATION == "TSI", ]
for (i in 1:nrow(TSI)){
        system(paste0("wget ", FTPpath, TSI$"FASTQ_FILE"[i], " --limit-rate=2m -Nc"))
}
