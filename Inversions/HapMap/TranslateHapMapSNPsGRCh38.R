#' Script to obtain the positions of the SNPs in GRCh38

# Bash code
## Download dbSNP142 hg38
curl -O ftp://hgdownload.soe.ucsc.edu//apache/htdocs/goldenPath/hg38/database/snp144.txt.gz

## Filter file 
gunzip snp144.txt.gz 
cut -f2,3,4,5,7,8,12,23,25 snp144.txt > snp144_small.txt

# R code
library(Biostrings)
df <- read.delim("snp144_small.txt", header = FALSE, stringsAsFactors = FALSE)
colnames(df) <- c("chr", "start", "end", "name", "strand", "refNCBI","class", "alleles", "alleleFreqs")
## Conserve only SNPs
df <- df[df$class == "single",]
## Commented code extracts the proportion of the two main alleles. Alleles frequency is the same for the standard
## and alternative references so it is not needed to run this part. 
# alleles <- strsplit(df$alleles, ",")
# freqs <- strsplit(df$alleleFreqs, ",")
# orderfreqs <- lapply(freqs, order, decreasing = TRUE)
# freqs <- lapply(1:length(freqs), function(x) freqs[[x]][orderfreqs[[x]]])
# alleles <- lapply(1:length(alleles), function(x) alleles[[x]][orderfreqs[[x]]])
# df$freq.1 <- sapply(freqs, "[", 1)
# df$freq.2 <- sapply(freqs, "[", 2)
# df$allele.1 <- sapply(alleles, "[", 1)
# df$allele.2 <- sapply(alleles, "[", 2)
# ## Change Strings in the strand - to complementary
# df[df$strand == "-", "allele.1"] <- as.character(complement(DNAStringSet(df[df$strand == "-", "allele.1"])))
# df[df$strand == "-" & df$allele.2 != "0", "allele.2"] <- as.character(complement(
#         DNAStringSet(df[df$strand == "-" & df$allele.2 != "0", "allele.2"])))
# df[df$strand == "-", "strand"] <- "+"

save(df, file = "dbSNP144hg38.Rdata")

# Load HapMap SNPs from all samples
library(snpStats)
hapmap <- read.plink("/NewLacie_CRW10082/DATASETS/STUDY/Hapmap/hapmap3_r2_b36_fwd.consensus.qc.poly")

## Create a reference with only the SNPs placed on the classical chromosomes
refdf <- df[df$chr %in% paste0("chr", c(1:22, "X", "Y")),]
commonsnps <- intersect(colnames(hapmap$genotypes), refdf$name)
hapmap$genotypes <- hapmap$genotypes[, commonsnps]
map <- refdf[refdf$name %in% commonsnps, ]
## Duplicated SNPs are only in sexual chromosome (present in X and Y)
map <- map[!duplicated(map$name), ]
rownames(map) <- map$name
map <- map[commonsnps, ]
hapmap$map <- hapmap$map[commonsnps, ]

## Change coordinates to hg38
hapmap$map$chr <- map$chr
hapmap$map$position <- map$start
hapmap$map$names <- hapmap$map$snp.name
save(hapmap, file = "HapMapALLhg38.RData")

# Code to download inversion data from the database
# library(biomaRt)
# ensembl <- useMart("ENSEMBL_MART_SNP", host = "www.ensembl.org")
# ensemblStruct <- useDataset("hsapiens_structvar",mart=ensembl)
# ensembl <- useDataset("hsapiens_snp",mart=ensembl)
# 
# ###########
# map <- getBM(attributes = c("sv_variant_type", "chr_name", "chrom_start", "chrom_end", "dgva_study_accession"), 
#                    mart = ensemblStruct)
