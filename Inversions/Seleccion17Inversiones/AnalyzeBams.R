#' Script that used alignment of reads in the whole genome to filter reads on the inversion
#' /SYNCRW10125/DATASETS/TYPE/NGS/1000Genomes/filtered

library(Rsamtools)
library(parallel)
library(ggplot2)
library(stringr)

source("/home/cruiz/InversionSequencing/InversionNGSutils.R")
load("/home/cruiz/InversionSequencing/17GoodInversions/invClust/invClustEUR.Rdata")
load("/home/cruiz/InversionSequencing/17GoodInversions/GRangeshg19inveRsion.Rdata")

ids <-  substring(dir(pattern = "inv17_1KG.bam"), 1, 9)


### Convert ids to inversion ids
library(data.table)

colPop <- fread("/NewLacie_CRW10082/DATASETS/STUDY/Hapmap/relationships_w_pops_121708.txt")
tsi <- colPop[population == "TSI", IID]

dat <-  fread("/SYNCRW10125/DATASETS/STUDY/1000GENOME/1000GenomesAnalysis")
dat2 <- dat[ANALYSIS_GROUP == "low coverage", , ]
dat2 <- dat2[SAMPLE_NAME %in% tsi, ]

dat3 <- as.data.frame(dat2)
dat3 <- do.call(rbind, lapply(unique(dat3$SAMPLE_NAME), function(nm) {
  mm <- max(dat3[dat3$SAMPLE_NAME == nm, "READ_COUNT"])
  dat3[dat3$SAMPLE_NAME == nm & dat3$READ_COUNT == mm, ]
}))

map <- unique(dat3[, c("RUN_ID", "SAMPLE_NAME")])
rownames(map) <- map$RUN_ID


props <- lapply(names(invsGeno)[-2], function(x){
  res <- lapply(ids, function(y) 
    processBam(paste0(y, x, "_1KG.bam"), paste0(y, "hg19.bam"), 
                                            as.character(runValue(seqnames(inversionRanges[x])))))
  propsdf <- data.frame(Reduce(function(...) rbind(...), res))
  rownames(propsdf) <- map[ids, "SAMPLE_NAME"]
  propsdf
})
names(props) <- names(invsGeno)[-2]
save(props, file = "/home/cruiz/InversionSequencing/17GoodInversions/invClust/NGSprops.Rdata")
setwd("/home/cruiz/InversionSequencing/17GoodInversions/NGS")

lapply(names(props), function(x){
  df <- props[[x]]
  colnames(df) <- "prop"
  df$inv <- invsGeno[[x]][rownames(df)]
  pdf(paste0("Props", x, ".pdf"))
  p <- ggplot(df, aes(x = prop, fill = inv)) + geom_histogram(binwidth=0.01) + 
    ggtitle(paste("Inverted haplotype proportions:", x)) + scale_x_continuous(limits = c(0, 1), name = "prop")
  print(p)
  dev.off()
})

options(bitmapType='cairo')
png("Propsinv8p23.png")
ggplot(propsdf8, aes(x = prop, fill = inv)) + geom_histogram(binwidth=0.01) + 
  ggtitle("Inverted haplotype proportions") + scale_x_continuous(limits = c(0, 1), name = "prop")
dev.off()

png("Propsinv8p23Cov.png")
ggplot(propsdf8, aes(x = prop, fill = inv)) + geom_histogram(binwidth=0.01) + facet_grid(coverage ~ .) +
  ggtitle("Inverted haplotype proportions") + scale_x_continuous(limits = c(0, 1), name = "prop")
dev.off()


png("Propsinv16p11.png")
ggplot(propsdf16, aes(x = prop, fill = inv)) + geom_histogram(binwidth=0.01) + 
  ggtitle("Inverted haplotype proportions") + scale_x_continuous(limits = c(0, 1), name = "prop")
dev.off()

png("Propsinv16p11Cov.png")
ggplot(propsdf16, aes(x = prop, fill = inv)) + geom_histogram(binwidth=0.01) + facet_grid(coverage ~ .) +
  ggtitle("Inverted haplotype proportions") + scale_x_continuous(limits = c(0, 1), name = "prop")
dev.off()


png("Propsinv17q21.31.png")
ggplot(propsdf17, aes(x = prop, fill = inv)) + geom_histogram(binwidth=0.01) + 
  ggtitle("Inverted haplotype proportions") + scale_x_continuous(limits = c(0, 1), name = "prop")
dev.off()

png("Propsinv17q21.31Cov.png")
ggplot(propsdf17, aes(x = prop, fill = inv)) + geom_histogram(binwidth=0.01) + facet_grid(coverage ~ .) +
  ggtitle("Inverted haplotype proportions") + scale_x_continuous(limits = c(0, 1), name = "prop")
dev.off()