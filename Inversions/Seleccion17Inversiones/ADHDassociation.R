#' Script to analyze association to ADHD

library(data.table)
library(Biostrings)
library(invClust)
library(snpStats)

source("/home/cruiz/InversionSequencing/InversionNGSutils.R")
load("/home/cruiz/InversionSequencing/17GoodInversions/SNPsClassification/ClassificationSNPsData.RData")


# CASOS ----
#   Genotipos

geno.cases <- read.plink("/NewLacie_CRW10082/DATASETS/STUDY/dbGaP/eMERGE_Pediatric_Disorders_phs000490.v1.p1/GenotypeFiles/phg000273.v1.NHGRI_eMERGE_CHOP_Pediatric_Hakonarson-ADHD_Human610-Quadv1.genotype-calls-matrixfmt.c1.MSRD/ADHD_Human610-Quadv1")


# Fenotipos

feno.cases <- read.delim("/NewLacie_CRW10082/DATASETS/STUDY/dbGaP/eMERGE_Pediatric_Disorders_phs000490.v1.p1/PhenotypeFiles/phs000490.v1.pht002968.v1.p1.c1.ADHD_Phenotype.HR_CHOP.txt", comment.char="#", as.is=TRUE, row.names = NULL)
coln <- colnames(feno.cases)
feno.cases <- feno.cases[, 1:10]
colnames(feno.cases) <- coln[2:11]
rownames(feno.cases) <- feno.cases$dbGaP.SubjID

# CONTROLES ----
#   
#   Genotipos

geno.controls <- read.plink("/NewLacie_CRW10082/DATASETS/STUDY/dbGaP/eMERGE_Pediatric_Disorders_phs000490.v1.p1/GenotypeFiles/phg000273.v1.NHGRI_eMERGE_CHOP_Pediatric_Hakonarson-Controls_Human610-Quadv1.genotype-calls-matrixfmt.c1.MSRD/Controls_Human610-Quadv1")

# Fenotipos

feno.controls <- read.delim("/NewLacie_CRW10082/DATASETS/STUDY/dbGaP/eMERGE_Pediatric_Disorders_phs000490.v1.p1/PhenotypeFiles/phs000490.v1.pht002966.v1.p1.c1.Controls_Phenotype.HR_CHOP.txt", comment.char="#", as.is=TRUE, row.names = NULL)
coln <- colnames(feno.controls)
feno.controls <- feno.controls[, 1:10]
colnames(feno.controls) <- coln[2:11]
rownames(feno.controls) <- feno.controls$dbGaP.SubjID



# MAP IDS

ids <- read.delim("/NewLacie_CRW10082/DATASETS/STUDY/dbGaP/eMERGE_Pediatric_Disorders_phs000490.v1.p1/PhenotypeFiles/phs000490.v1.pht002963.v1.p1.eMERGE_Complex_Pediatric_Disorders_Sample.MULTI.txt", comment.char="#", as.is=TRUE)
rownames(ids) <- ids$dbGaP.SubjID

feno.cases$SAMPLE_ID <- ids[rownames(feno.cases), "SAMPLE_ID"]
feno.controls$SAMPLE_ID <- ids[rownames(feno.controls), "SAMPLE_ID"]

feno <- rbind(feno.cases, feno.controls)
feno$Status <- factor(rep(c("control", "case"), c(nrow(feno.controls), nrow(feno.cases))))


# Classify individuals
usedSNPs <- unlist(lapply(SNPsR2, names))
refSNPs <- Refs
names(refSNPs) <- NULL
refSNPs <- unlist(lapply(refSNPs, "[[", "het"))

## Controls
map.control <- geno.controls$map
rownames(map.control) <- map.control$snp.name

## Filter objects to only those included in the references
commonSNPs <- intersect(usedSNPs, rownames(map.control))
map.control <- map.control[commonSNPs, ]

alleletable.control <- getAlleleTable(map.control)

alleletable.control$InvRef <- refSNPs[rownames(alleletable.control)]
# Select SNPs that the alleles on the array are not in our reference
wrongSNPs <- rownames(alleletable.control)[sapply(rownames(alleletable.control), 
                                          function(x) !alleletable.control[x, 5] %in% alleletable.control[x, 1:3])]

badMap <- map.control[wrongSNPs, ]

badMap$allele.1 <- as.character(complement(DNAStringSet(badMap[, "allele.1"])))
badMap$allele.2 <- as.character(complement(DNAStringSet(badMap[, "allele.2"])))
alleletable[wrongSNPs, ] <- getAlleleTable(badMap)
alleletable$InvRef <- refSNPs[rownames(alleletable)]
wrongSNPs <- rownames(alleletable)[sapply(rownames(alleletable), 
                                          function(x) !alleletable[x, 5] %in% alleletable[x, 1:3])]

geno <- AGP$genotypes[, commonSNPs]
genos <- getGenotypesTable(geno, alleletable)


