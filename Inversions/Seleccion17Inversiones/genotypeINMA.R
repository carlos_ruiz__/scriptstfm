#' Script to genotype INMA samples
#' Maquina 3

library(snpStats)
library(foreign)
library(SNPassoc)
library(Biostrings)
library(snpStats)

source("/home/cruiz/Inversions/InversionNGSutils.R")
load("/home/cruiz/Inversions/ClassificationSNPsData.RData")


object <- read.plink("/DATA/GoDMC/data_chr17_bis")
"rs2131049" %in% rownames(object$map)
# TRUE
genos <- object$genotypes
ids <- as.numeric(substring(rownames(genos), 7, 10))
genos <- genos[!is.na(ids), ]
rownames(genos) <- paste("04", ids[!is.na(ids)], "0", sep = "_")

map <- object$map
rownames(map) <- map$snp.name

commonSNPs <- intersect(names(SNPsR2$inv141b), rownames(map))
map <- map[commonSNPs, ]

alleletable <- getAlleleTable(map)

refSNPs <- Refs
names(refSNPs) <- NULL
refSNPs <- unlist(lapply(refSNPs, "[[", "het"))
alleletable$InvRef <- refSNPs[rownames(alleletable)]
# Select SNPs that the alleles on the array are not in our reference
wrongSNPs <- rownames(alleletable)[sapply(rownames(alleletable), 
                                          function(x) !alleletable[x, 5] %in% alleletable[x, 1:3])]

badMap <- map[wrongSNPs, ]

badMap$allele.1 <- as.character(complement(DNAStringSet(badMap[, "allele.1"])))
badMap$allele.2 <- as.character(complement(DNAStringSet(badMap[, "allele.2"])))
alleletable[wrongSNPs, ] <- getAlleleTable(badMap)
alleletable$InvRef <- refSNPs[rownames(alleletable)]
wrongSNPs <- rownames(alleletable)[sapply(rownames(alleletable), 
                                          function(x) !alleletable[x, 5] %in% alleletable[x, 1:3])]
geno <- genos[, commonSNPs]
genos <- getGenotypesTable(geno, alleletable)


classifProps <- classifSNPs(genos, SNPsR2$inv141b, Refs$inv141b, 0.6)
classification <- getInvStatus(classifProps)
