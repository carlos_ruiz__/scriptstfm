#' Script to run invClust on 1000 Genomes using new hg19 references and tagSNPs
#' Run in /SYNCRW10125/DATASETS/STUDY/1000GENOME/VCF
library(invClust)
library(GenomicRanges)
library(VariantAnnotation)
library(parallel)

load("/home/cruiz/InversionSequencing/17GoodInversions/GRangeshg19inveRsion.Rdata")
source("/home/cruiz/InversionSequencing/InversionNGSutils.R")
load("/SYNCRW10125/DATASETS/STUDY/1000GENOME/Samples_Pop1GK.Rdata")
load("/home/cruiz/InversionSequencing/SeqDupGR.RData")
load("/home/cruiz/InversionSequencing/17GoodInversions/invClust/invClustEUR.Rdata")
load("/home/cruiz/InversionSequencing/17GoodInversions/invClust/pops/tagSNPsEUR.Rdata")

EUR <- rownames(samp_pop)[samp_pop$pop %in% c("GBR", "FIN", "IBS", "TSI", "CEU")]

invhapEURtag <- mclapply(names(inversionRanges), function(x) 
  computeInvClust(inversionRanges[x], EUR, snps.names = names(tagSNPsEUR[[x]])), mc.cores = 10)
names(invhapEURtag) <- names(inversionRanges)

setwd("/home/cruiz/InversionSequencing/17GoodInversions/invClust/")

lapply(names(invhapEURtag), function(x){
  pdf(paste0(x, "tag.pdf"))
  plotInv(invhapEURtag[[x]], title = x)
  dev.off()
})





invsGenoMergeTag <- lapply(names(invsGenoAFRtag), function(x) {
  res <- factor(c(as.character(invsGenoAFRtag[[x]]), as.character(invsGenoAMRtag[[x]]), 
                  as.character(invsGeno[[x]]), as.character(invsGenoEAStag[[x]]), as.character(invsGenoSAStag[[x]])))
  names(res) <- c(names(invsGenoAFRtag[[x]]), names(invsGenoAMRtag[[x]]), 
                  names(invsGeno[[x]]), names(invsGenoEAStag[[x]]), names(invsGenoSAStag[[x]]))
  res
})
names(invsGenoMergeTag) <- names(invsGenoAFRtag)

lapply(names(invhapALL), function(x){
  pdf(paste0(x, "ALLpopClass.pdf"))
  plotInv(invhapALL[[x]], title = x, classification = invsGenoMerge[[x]][invhapALL[[x]]$datin$ids])
  dev.off()
})

lapply(names(invhapALLtagEUR)[-c(4, 5, 8)], function(x){
  pdf(paste0(x, "ALLtagEURpopClass.pdf"))
  plotInv(invhapALLtagEUR[[x]], title = x, classification = invsGenoMergeTag[[x]][invhapALLtagEUR[[x]]$datin$ids])
  dev.off()
})

lapply(names(invhapNoAFRtagEUR)[-c(4, 8)], function(x){
  pdf(paste0(x, "NoAFRtagEURpopClass.pdf"))
  plotInv(invhapNoAFRtagEUR[[x]], title = x, classification = invsGenoMergeTag[[x]][invhapNoAFRtagEUR[[x]]$datin$ids])
  dev.off()
})




### No AFR
lapply(names(invhapNoAFR), function(x){
  pdf(paste0(x, "NoAFR.pdf"))
  plotInv(invhapNoAFR[[x]], title = x)
  dev.off()
})

lapply(names(invhapNoAFR), function(x){
  pdf(paste0(x, "NoAFRpop.pdf"))
  plotInv(invhapNoAFR[[x]], title = x, classification = factor(samp_pop[invhapNoAFR[[x]]$datin$ids, "superpop"]))
  dev.off()
})


lapply(names(invhapNoAFR), function(x){
  pdf(paste0(x, "NoAFRpopClass.pdf"))
  plotInv(invhapNoAFR[[x]], title = x, classification = invsGenoMerge[[x]][invhapNoAFR[[x]]$datin$ids])
  dev.off()
})




lapply(names(invhapNoSeqDup), function(x){
  pdf(paste0(x, "NoseqDup.pdf"))
  plotInv(invhapNoSeqDup[[x]], title = x)
  dev.off()
})

lapply(names(invhapFamNoSeqDup), function(x){
  pdf(paste0(x, "FamNoseqDup.pdf"))
  plotInv(invhapFamNoSeqDup[[x]], title = x)
  dev.off()
})

library(snpStats)

invcall_genotypes <- lapply(invhap, function(x){
  res <- as.numeric(invGenotypes(x)) # Genero un vector numérico de los genotipos. Normalmente 1 -> NN; 2 -> NI; 3 -> II
  names(res) <- x$datin$ids # Les asigno el nombre que les corresponde
  res.SM <- new("SnpMatrix", as.matrix(res)) # A partir del vector numérico, creo una SnpMatrix
  res.SM
})

setwd("/SYNCRW10125/DATASETS/STUDY/1000GENOME/VCF")
invcall_ldExtnd_res <- mclapply(names(invcall_genotypes), function(x){
  range <- inversionRanges[x]
  start(range) <- start(range) - 100e3
  end(range) <- end(range) + 100e3
  snpsVCF <- getVCFmatrix(range, EUR)
  ld <- ld(invcall_genotypes[[x]], snpsVCF$genotypes, stats=c("R.squared", "D.prime")) # Con la SnpMatrix, llamo a la función ld de snpStats, y  utilizo la SnpMatrix de genotipos para hacer la asociación. No utilices la SnpMatrix de todo el genoma ya que tardará mucho y no tiene sentido.
  sum <- col.summary(snpsVCF$genotypes)
  res <- list(res = ld, annot = snpsVCF$map, sum = sum)
  res
}, mc.cores = 5)
names(invcall_ldExtnd_res) <- names(invcall_genotypes) 

getLD <- function(x){
  print(x)
  range <- inversionRanges[x]
  start(range) <- start(range) - 100e3
  end(range) <- end(range) + 100e3
  snpsVCF <- getVCFmatrix(range, EUR)
  ld <- ld(snpsVCF$genotypes, stats=c("R.squared", "D.prime"), depth = ncol(snpsVCF$genotypes))
  res <- list(res = ld, annot = snpsVCF$map)
  res
}

invregion_ld_res <- lapply(names(inversionRanges), function(x) try(getLD(x)))
names(invregion_ld_res) <- names(inversionRanges)


setwd("/home/cruiz/InversionSequencing/17GoodInversions/invClust")

save(invcall_ldExtnd_res, file = "inversionSNPsLD.Rdata")

invsGenoALL <- lapply(invhapALL, invGenotypes)
save(invhapALL, invsGenoALL, file = "invClustALL.Rdata")

invsGenoNoSeqDup <- lapply(invhapNoSeqDup, invGenotypes)
save(invhapNoSeqDup, invsGenoNoSeqDup, file = "invClustEURNoSeqDup.Rdata")

invhapFamNoSeqDupGeno <- lapply(invhapFamNoSeqDup, invGenotypes)
save(invhapFamNoSeqDup, invhapFamNoSeqDupGeno, file = "invClustEURFamNoSeqDup.Rdata")

## Compare haplotypes (all SNPs and Samples vs filtered samples and SNPs)
filteredsamples <- names(invhapFamNoSeqDupGeno[[1]])
comparison <- lapply(names(invhapFamNoSeqDupGeno), function(x) 
  table(invsGeno[[x]][filteredsamples], invhapFamNoSeqDupGeno[[x]]))
names(comparison) <- names(invhapFamNoSeqDupGeno)

save(invregion_ld_res, invcall_ldExtnd_res, file = "LDRes.Rdata")

tagSNPs <- lapply(invcall_ldExtnd_res, function(x){
  a <- x$res$R.squared
  colnames(a)[a > 0.8]
})
save(tagSNPs, file = "tagSNPs.Rdata")


spectrum <- rainbow(10, start=0, end=1/6)[10:1]
plotX <- function(x){
  pdf(paste0(x, "LDregion.pdf"))
  image(as(invregion_ld_res[[x]]$res$D.prime, "matrix"), col = spectrum, cuts = 9,
        colorkey=TRUE, main = x, lwd = 0, useRaster = TRUE)
  snpsGR <- makeGRangesFromDataFrame(invregion_ld_res[[x]]$annot, start.field = "position", 
                                     end.field = "position")
  over <- findOverlaps(snpsGR, inversionRanges[x])
  abline(h = min(from(over))/length(snpsGR), v = min(from(over))/length(snpsGR), col = "black")
  abline(h = max(from(over))/length(snpsGR), v = max(from(over))/length(snpsGR), col = "black")
  dev.off()
}

lapply(names(invregion_ld_res), plotX)

lapply(names(invcall_ldExtnd_res), function(x){
  pdf(paste0(x, "LD.pdf"))
  plot(invcall_ldExtnd_res[[x]]$annot$position, invcall_ldExtnd_res[[x]]$res$R.squared, pch = 16, ylim=c(0,1), ylab="R.squared", xlab="Position") # Plot de las posiciones de los SNPs y el resultado de calcular la r2 para el alelo 3.
  abline(h=0.8, lty=3, col="red") # linea para marcar los SNP con una r2 > 0.8
  abline(v=start(inversionRanges[x]), lty=3, col="blue") 
  abline(v=end(inversionRanges[x]), lty=3, col="blue") 
  title(x)
  dev.off()
})

lapply(names(invcall_ldExtnd_res), function(x){
  pdf(paste0(x, "Dprime.pdf"))
  plot(invcall_ldExtnd_res[[x]]$annot$position, invcall_ldExtnd_res[[x]]$res$D.prime, pch = 16, ylim=c(0,1), ylab="D.prime", xlab="Position") # Plot de las posiciones de los SNPs y el resultado de calcular la r2 para el alelo 3.
  abline(h=0.8, lty=3, col="red") # linea para marcar los SNP con una r2 > 0.8
  abline(v=start(inversionRanges[x]), lty=3, col="blue") 
  abline(v=end(inversionRanges[x]), lty=3, col="blue") 
  title(x)
  dev.off()
})