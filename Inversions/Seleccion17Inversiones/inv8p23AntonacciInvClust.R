#' Script to detect inv 15q13 haplotypes using Antonnacie

library(invClust)
library(GenomicRanges)
library(VariantAnnotation)
library(parallel)

source("/home/cruiz/InversionSequencing/InversionNGSutils.R")

load("/SYNCRW10125/DATASETS/STUDY/1000GENOME/Samples_Pop1GK.Rdata")
load("/home/cruiz/InversionSequencing/SeqDupGR.RData")

range <- GRanges("8:7238552-12442658") ## Obtained from Paper and remapping
names(range) <- "8p23"

invs <- read.csv2("/home/cruiz/InversionSequencing/17GoodInversions/antonnaci_Inv.csv", nrows = 27)
invs$Sample_Name <- paste0("NA", substring(as.character(invs[,1]), 3, 7))

rownames(invs) <- invs$Sample_Name

inv8p23 <- invs[samp_pop[rownames(invs), "superpop"] == "EUR", "X8p23"]
inv8p23 <- relevel(inv8p23, "not inverted")
inv <- as.numeric(inv8p23)
names(inv) <- rownames(invs)[samp_pop[rownames(invs), "superpop"] == "EUR"]
inv.SM <- new("SnpMatrix", as.matrix(inv)) 

setwd("/SYNCRW10125/DATASETS/STUDY/1000GENOME/VCF")
snpsVCF <- getVCFmatrix(range, rownames(inv.SM), minmaf = 0)
ld <- ld(inv.SM[rownames(snpsVCF$genotypes), ], snpsVCF$genotypes, stats = "R.squared") 

setwd("/home/cruiz/InversionSequencing/17GoodInversions/Antonacci/")

pdf("8p23AntonacciHaplosLD.pdf")
plot(snpsVCF$map$position, ld[1,], pch = 16, ylim=c(0,1), ylab="R.squared", xlab="Position", type = "n")
locdup <- GRdup[subjectHits(findOverlaps(range, GRdup))]
if (length(locdup)){
  lapply(locdup, function(x) rect(start(x), -10, end(x), 10, col = "darkgoldenrod"))
}
points(snpsVCF$map$position, ld[1,])  
abline(h=0.8, lty=3, col="red") # linea para marcar los SNP con una r2 > 0.8
title("LD of SNPs with Antonacci genotypes: 8p23")
dev.off()

snps <- colnames(ld)[ld[1,] > 0.2]

setwd("/SYNCRW10125/DATASETS/STUDY/1000GENOME/VCF")
EUR <- rownames(samp_pop)[samp_pop$pop %in% c("GBR", "FIN", "IBS", "TSI", "CEU")]
invhap <- computeInvClust(range, EUR, snps.names = snps, minmaf = 0)


setwd("/home/cruiz/InversionSequencing/17GoodInversions/Antonacci/")

pdf("15q13AntonacciinvClust.pdf")
plotInv(invhap, title = "invClust from Antonacci")
dev.off()

inv2 <- as.numeric(invGenotypes(invhap))
names(inv2) <- invhap$datin$ids # Les asigno el nombre que les corresponde
inv2 <- inv2[getCert(invhap) > 0.99] # Solo nos quedamos con los de buena certidumbre
inv2.SM <- new("SnpMatrix", as.matrix(inv2)) # A partir del vector numérico, creo una SnpMatrix

setwd("/SYNCRW10125/DATASETS/STUDY/1000GENOME/VCF")
snpsVCF <- getVCFmatrix(range, rownames(inv2.SM), minmaf = 0)
ld2 <- ld(inv2.SM, snpsVCF$genotypes, stats = "R.squared") 


setwd("/home/cruiz/InversionSequencing/17GoodInversions/Antonacci/")

pdf("15q13AntonacciinvClustLD.pdf")
plot(snpsVCF$map$position, ld2[1,], pch = 16, ylim=c(0,1), ylab="R.squared", xlab="Position", type = "n")
locdup <- GRdup[subjectHits(findOverlaps(range, GRdup))]
if (length(locdup)){
  lapply(locdup, function(x) rect(start(x), -10, end(x), 10, col = "darkgoldenrod"))
}
points(snpsVCF$map$position, ld2[1,])  
abline(h=0.8, lty=3, col="red") # linea para marcar los SNP con una r2 > 0.8
title("LD of SNPs with from invClust: 15q13")
dev.off()

