#' Script to check the SNPs of the alternative reference (INV 141)
#' Run in /NewLacie_CRW10082/DATASETS/STUDY/dbSNP
curl -O ftp://hgdownload.soe.ucsc.edu//apache/htdocs/goldenPath/hg38/database/snp146Common.txt.gz


gunzip -c snp146Common.txt.gz | cut -f2,3,4,5,7,8,9,10 | gzip - > snp146Common_small.txt.gz

#### R code
library(data.table)
library(Biostrings)
source("/home/cruiz/InversionSequencing/InversionNGSutils.R")

load("/home/cruiz/InversionSequencing/17GoodInversions/SNPsClassification/ClassificationSNPsData.RData")

snps <- fread("gunzip -c snp146Common_small.txt.gz", header = FALSE,
              stringsAsFactors = FALSE)
colnames(snps) <- c("chr", "start", "end", "name", "strand", "refNCBI", "refUCSC", "alleles")
snpsInv <- snps[snps$name %in% names(filteredSNPsR2$inv141)]

snpsRefSeq <- snpsInv[snpsInv$chr == "chr17", refNCBI]
snpsRefSeq <- sapply(snpsRefSeq, function(x) paste(rep(x, 2), collapse = ""))
names(snpsRefSeq) <- snpsInv[snpsInv$chr == "chr17", name]

snpsInvSeq <- snpsInv[snpsInv$chr == "chr17_KI270857v1_alt", refNCBI]
snpsInvSeq <- sapply(snpsInvSeq, function(x) paste(rep(x, 2), collapse = ""))
names(snpsInvSeq) <- snpsInv[snpsInv$chr == "chr17_KI270857v1_alt", name]

common <- intersect(names(snpsInvSeq), names(snpsRefSeq))
geno <- t(data.frame(ALT = snpsInvSeq[common], STD = snpsRefSeq[common]))

invclass <- classifSNPs(geno, filteredSNPsR2$inv141, Refs$inv141)
invclass
# ALT STD
# inv 0.7952962   0
# het 0.0000000   0
# std 0.1963100   1
