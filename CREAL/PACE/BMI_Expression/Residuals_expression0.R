library(Hmisc)
library(foreign)

load("/DATA/Methylation_INMA/PACE/BMI/pairsCpGExpr.Rdata") 
load("/home/cruiz/CREAL/PACE/expressionSet.Rdata") 
load("/DATA/Methylation_INMA/450K_blood/DATA/cell_countL.Rdata")
load("/home/cruiz/CREAL/PACE/commonSamplesMethExprs.Rdata")

library(Biobase)
library(sva)

exprs0 <- colnames(set)
substring(exprs0, nchar(exprs0), nchar(exprs0)) <- "0"
colnames(set) <- exprs0
set <- set[, common0y]

covars <- read.csv("/DATA/Methylation_INMA/450K_blood/DATA/ID/Samplelist_Full.csv", header = T, stringsAsFactors = FALSE)
colnames(covars) <- covars[1, ]
covars <- covars[-1, ]
covars <- subset(covars, !duplicated(MeDALL_ID))
rownames(covars) <- covars$MeDALL_ID

pheno <- merge(covars, count.all2, by = 0)
rownames(pheno) <- pheno[,1]
pheno <- pheno[,-1]

pheno2 <- read.dta("/DATA/Methylation_INMA/450K_blood/DATA2/samples/BD_INMASab_metilacio.dta")
rownames(pheno2) <- paste("04", pheno2$idnum, "0", sep = "_")
pheno <- cbind(pheno, pheno2[rownames(pheno), ])


#Common Samples
common <- intersect(rownames(pheno), colnames(set))
set <- set[, common]
pheno <- pheno[common, ]

#Formula (you can customize this formula as long as the phenotype of interest is placed first)
fm <- y ~ Gender + CD8T + CD4T + NK + Bcell + Mono + Gran + msmk

### Compute SVA
mod <- model.matrix(~ Gender + CD8T + CD4T + NK + Bcell + Mono + Gran, data = pheno)
mod0 <- model.matrix(~1, data = pheno)
n.sv <- num.sv(exprs(set), mod)
svavars <- sva(exprs(set), mod, mod0, n.sv = n.sv)$sv
colnames(svavars) <- letters[1:n.sv]
pheno <- cbind(pheno, svavars)

fmsva <- as.formula(paste("y ~ Gender + CD8T + CD4T + NK + Bcell + Mono + Gran +", 
                 paste(letters[1:n.sv], collapse = " + ")))

method = "LMM"; # Set to RLM if you want robust linear regression.

if (method == "LMM") {
        # LMM
        library(lme4);
        doOne <- function(i) {
                pheno$y <- as.numeric(exprs(set)[i,]);
                result <- lm(fm, data=pheno);
                
                # Returns residuals
                result <- c(residuals(result));
                return (result);
        }
} 

residuals_exprs0 <- do.call(rbind, lapply(1:NROW(set), doOne));
rownames(residuals_exprs0) <- rownames(set);
colnames(residuals_exprs0) <- colnames(set);

doOnesva <- function(i) {
        pheno$y <- as.numeric(exprs(set)[i,]);
        result <- lm(fmsva, data=pheno);
        
        # Returns residuals
        result <- c(residuals(result));
        return (result);
}

residuals_exprsSVA0 <- do.call(rbind, lapply(1:NROW(set), doOnesva));
rownames(residuals_exprsSVA0) <- rownames(set);
colnames(residuals_exprsSVA0) <- colnames(set);

save(residuals_exprs0, residuals_exprsSVA0, file = "/DATA/Methylation_INMA/PACE/BMI/Expression_residuals0y.Rdata")