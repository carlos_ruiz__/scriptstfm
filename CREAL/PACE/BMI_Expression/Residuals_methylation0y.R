library(Hmisc)
library(foreign)
load("/DATA/Methylation_INMA/450K_blood/DATA/Residue.Rdata")
load("/DATA/Methylation_INMA/450K_blood/DATA/cell_countL.Rdata")
load("/DATA/Methylation_INMA/PACE/BMI/SelectedCpg.Rdata")
load("/home/cruiz/CREAL/PACE/commonSamplesMethExprs.Rdata")

#Common cpgs
commoncpgs <- intersect(cpgs, rownames(resd))
resd <- resd[commoncpgs, common0y]

# Phenotype data loading
# Assumption: rows = num samples, cols = num phenotypes
covars <- read.csv("/DATA/Methylation_INMA/450K_blood/DATA/ID/Samplelist_Full.csv", header = T, stringsAsFactors = FALSE)
colnames(covars) <- covars[1, ]
covars <- covars[-1, ]
covars <- subset(covars, !duplicated(MeDALL_ID))
rownames(covars) <- covars$MeDALL_ID

pheno <- merge(covars, count.all2, by = 0)
rownames(pheno) <- pheno[,1]
pheno <- pheno[,-1]

pheno2 <- read.dta("/DATA/Methylation_INMA/450K_blood/DATA2/samples/BD_INMASab_metilacio.dta")
rownames(pheno2) <- paste("04", pheno2$idnum, "0", sep = "_")
pheno <- cbind(pheno, pheno2[rownames(pheno), ])


#Common Samples
common <- intersect(rownames(pheno), colnames(resd))
resd <- resd[, common]
pheno <- pheno[common, ]

#Formula (you can customize this formula as long as the phenotype of interest is placed first)
fm <- y ~ Gender + CD8T + CD4T + NK + Bcell + Mono + Gran + msmk


method = "LMM"; # Set to RLM if you want robust linear regression.

if (method == "LMM") {
        # LMM
        library(lme4);
        doOne <- function(i) {
                pheno$y <- as.numeric(resd[i,]);
                result <- lm(fm, data=pheno);
                
                # Returns residuals
                result <- c(residuals(result));
                return (result);
        }
} 

# Serial version: 
residuals_meth0 <- do.call(rbind, lapply(1:NROW(resd), doOne));
rownames(residuals_meth0) <- rownames(resd);
colnames(residuals_meth0) <- colnames(resd);
save(residuals_meth0, file = "/DATA/Methylation_INMA/PACE/BMI/Methylation_residuals0y.Rdata")