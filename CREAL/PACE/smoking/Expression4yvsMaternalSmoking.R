load("/home/cruiz/CREAL/PACE/pairsCpGExpr.Rdata") 
load("/home/cruiz/CREAL/PACE/expressionSet.Rdata") 
load("/home/cruiz/CREAL/PACE/commonSamplesMethExprs.Rdata")

library(sva)
library(limma)
library(Biobase)

set <- set[unique(crp_metexp[,2]), common4y]

smoking <- read.csv2("/home/cruiz/CREAL/PACE/smokingall_INMA.csv", stringsAsFactors = FALSE)
rownames(smoking) <- paste0(gsub(" ", "", smoking$MeDallID), "_4")

covars <- read.csv("/DATA/Methylation_INMA/450K_blood/DATA/ID/Samplelist_Full.csv", header = T, stringsAsFactors = FALSE)
colnames(covars) <- covars[1, ]
covars <- covars[-1, ]
covars <- subset(covars, !duplicated(MeDALL_ID))
rownames(covars) <- covars$MeDALL_ID

pheno <- merge(smoking, covars, by = 0)
rownames(pheno) <- pheno[, 1]
pheno <- pheno[,-1]
pheno <- pheno[!is.na(pheno$anysmoking) & !is.na(pheno$Sust_smoke), ]

common <- intersect(rownames(pheno), colnames(set))
set <- set[, common]
pheno <- pheno[common, ]

pData(set) <- pheno
exp <- exprs(set)

### Compute SVA any smoking
mod <- model.matrix(~ anysmoking + Gender, data = pheno)
mod0 <- model.matrix(~ Gender, data = pData(set))
n.sv <- num.sv(exprs(set), mod)
svavars <- sva(exprs(set), mod, mod0, n.sv = n.sv)$sv
anymodSv <- cbind(mod, svavars)
anyfit <- lmFit(exp, mod)
anyfit <- eBayes(anyfit)
anyres <- topTable(anyfit, number = Inf, coef = 2, confint = TRUE)
anyfitSVA <- lmFit(exp, anymodSv)
anyfitSVA <- eBayes(anyfitSVA)
anyresSVA <- topTable(anyfitSVA, number = Inf, coef = 2, confint = TRUE)

### Compute SVA sust smoking
mod <- model.matrix(~ Sust_smoke + Gender, data = pData(set))
mod0 <- model.matrix(~ Gender, data = pData(set))
n.sv <- num.sv(exprs(set), mod)
svavars <- sva(exprs(set), mod, mod0, n.sv = n.sv)$sv
sustmodSv <- cbind(mod, svavars)
sustfit <- lmFit(exp, mod)
sustfit <- eBayes(sustfit)
sustres <- topTable(sustfit, number = Inf, coef = 2, confint = TRUE)
sustfitSVA <- lmFit(exp, sustmodSv)
sustfitSVA <- eBayes(sustfitSVA)
sustresSVA <- topTable(sustfitSVA, number = Inf, coef = 2, confint = TRUE)

save(anyres, anyresSVA, sustres, sustresSVA, file = "/home/cruiz/CREAL/PACE/ExpressionvsSmoking.Rdata")

dfs <- c(anyfit$df.total[1], anyfitSVA$df.total[1], sustfitSVA$df.total[1], 
         sustfit$df.total[1])
constantCI <- qt(0.975, dfs)

        
results <- list(anyres, anyresSVA, sustres, sustresSVA)
results <- lapply(1:length(results), function(x){
        df <- results[[x]]
        res <- data.frame(rownames(df), df[,1], (df[, 1] - df[, 2])/constantCI[x], 
                          df[, 6:7], fData(set)[rownames(df), 4])
        colnames(res) <- c("probe_Id", "logFC", "se", "pval", "p.adj", "genesymbol")
        res})

filenames <- c("anySmoking", "anySmokingSVA", "SustainedSmoking", "SustainedSmokingSVA")
lapply(1:length(filenames), function(x){
        write.table(results[[x]], 
        file = paste0("/DATA/Methylation_INMA/PACE/smoking_expression_methylation/20150707/Expressionvs", filenames[x], ".txt"),
        row.names = FALSE)})