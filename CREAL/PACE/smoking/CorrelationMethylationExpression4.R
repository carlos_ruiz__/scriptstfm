load("/home/cruiz/CREAL/PACE/Expression_residuals4y.Rdata")
load("/home/cruiz/CREAL/PACE/Methylation_residuals4y.Rdata")
load("/home/cruiz/CREAL/PACE/pairsCpGExpr.Rdata") 
load("/home/cruiz/CREAL/PACE/expressionSet.Rdata") 
library(Biobase)
annot <- fData(set)

merge_res <- t(rbind(residuals_meth4, residuals_exprs4))

metexp2 <- function (residuals, instructions){
        
        associations <- as.data.frame(matrix(nrow=dim(instructions)[1], ncol=5))
        colnames(associations) <- c("cpg","probe_Id","beta","se","pval")
        instructions <- instructions[instructions$cpg %in% colnames(residuals),]
        
        
        for (i in 1:dim(instructions)[1]){
                exp_name <- as.character(instructions$exp[i])
                
                cpg_name <-as.character(instructions$cpg[i])
                
                
                if(exp_name %in% colnames(residuals)){
                        
                        exp_var <- residuals[,exp_name]      
                        cpg_var <- residuals[,cpg_name]
                        fit<-lm(exp_var~cpg_var)
                        
                        associations$cpg[i] <- cpg_name
                        associations$probe_Id[i] <- exp_name
                        associations$beta[i] <- summary(fit)$coef[2,1]
                        associations$se[i] <- summary(fit)$coef[2,2]
                        associations$pval[i] <- summary(fit)$coef[2,4]
                        
                }
                
                associations$cpg[i] <- cpg_name
                associations$probe_Id[i] <- exp_name
                
                
        }
        
        return(associations) 
        
}

crp_metexp4 <- metexp2(merge_res,crp_metexp)  
crp_metexp4$p.adj <- p.adjust(crp_metexp4$pval, "BH")
crp_metexp4$genesymbol <- annot[crp_metexp4[,2], "gene"]

merge_ressva <- t(rbind(residuals_meth4, residuals_exprsSVA4))
crp_metexp4sva <- metexp2(merge_ressva, crp_metexp)  
crp_metexp4sva$p.adj <- p.adjust(crp_metexp4sva$pval, "BH")
crp_metexp4sva$genesymbol <- annot[crp_metexp4sva[,2], "gene"]

save(crp_metexp4,crp_metexp4sva, file = "/home/cruiz/CREAL/PACE/CorrelationExprsMeth4y.Rdata")