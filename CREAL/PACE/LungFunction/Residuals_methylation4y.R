## Set working directory to Lung Function folder
setwd("~/data/WS_INMA/Methylation_INMA/PACE/lung_function/")

# Load Expression data
load("~/data/WS_INMA/gene_expression_4y/INMA_HTA-RMA/inma_expset_all_20180615.rda") 

# Load Cell counts
load("~/data/WS_INMA/Methylation_INMA/450K_blood/DATA/cell_countL.Rdata")

# Load Methylation data
load("~/data/WS_INMA/Methylation_INMA/450K_blood/DATA/Residue.Rdata")

load("Results/SelectedCpg.Rdata")

## Load common samples between expression and methylation
load("Results/commonSamplesMethExprs.Rdata")

#### Warning: Not all selected CpGs were present in our results!!!!
#Common cpgs
commoncpgs <- intersect(unique(c(FEV1, FEV1FVC, FEF75)), rownames(resd))
resd <- resd[commoncpgs, common4y]

# Phenotype data loading
# Assumption: rows = num samples, cols = num phenotypes
covars <- read.csv("~/data/WS_INMA/Methylation_INMA/450K_blood/DATA/ID/Samplelist_Full.csv", header = T, stringsAsFactors = FALSE)
colnames(covars) <- covars[1, ]
covars <- covars[-1, ]
covars <- subset(covars, !duplicated(MeDALL_ID))
rownames(covars) <- covars$MeDALL_ID

pheno <- merge(covars, count.all2, by = 0)
rownames(pheno) <- pheno[,1]
pheno <- pheno[,-1]

#Common Samples
common <- intersect(rownames(pheno), colnames(resd))
resd <- resd[, common]
pheno <- pheno[common, ]

#Formula (you can customize this formula as long as the phenotype of interest is placed first)
fm <- y ~ Gender + CD8T + CD4T + NK + Bcell + Mono + Gran


method = "LMM"; # Set to RLM if you want robust linear regression.

if (method == "LMM") {
        # LMM
        library(lme4);
        doOne <- function(i) {
                pheno$y <- as.numeric(resd[i,]);
                result <- lm(fm, data=pheno);
                
                # Returns residuals
                result <- c(residuals(result));
                return (result);
        }
} 

# Serial version: 
residuals_meth4 <- do.call(rbind, lapply(1:NROW(resd), doOne));
rownames(residuals_meth4) <- rownames(resd);
colnames(residuals_meth4) <- colnames(resd);

save(residuals_meth4, file = "Results/Methylation_residuals4y.Rdata")