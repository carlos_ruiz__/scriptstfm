# Script to test ComBat reduction of batch. Model 2: BREATHE

#load packages
library(foreign) #to read stata file
library(R.utils) # for gzip
library(data.table)# to process results
library(parallel) # to use multicore approach - part of base R
library(plyr) # to process results
library(matrixStats) #for rowSums, rowIQRs etc.
library(MEAL)
library(sandwich) #Huberís estimation of the standard error
library(MASS) # rlm function for robust linear regression
library(lmtest) # to use coeftest

load("/DATA/Methylation_INMA/450K_blood/QC/FinalSetsEUR.Rdata")
mset <- mset[, pData(mset)$age == 0 & pData(mset)$Project == "BREATHE"]
betas <- betas(mset)
phenotypes <- pData(mset)

Pheno <- phenotypes[, c("Sample_Name", "msmk", "sex", "edadm", "meduc")]
colnames(Pheno)[1] <- "aln"

#Pheno is a dataframe containing phenotype information for all samples. The first column contains sample IDs, the second column contains the trait of interest. The trait of interest should be a NUMERIC variable coded 0 (controls) and 1 (cases). All other columns contain covariates. There should be no extra columns. One phenotype file/object per model (i.e. the contents of the object defines the model). In the case of model 1a, this object would contain [ID; childhood AD status; gender; batch].

#meth is a matrix of Illumina beta values for all samples(columns) and probes(rows)

#Match
meth<-betas[,na.omit(match(Pheno$aln,colnames(betas)))]
Pheno<-Pheno[match(colnames(meth),Pheno$aln),]
ifelse(all(Pheno$aln==colnames(meth)), "meth and phenotype data successfully matched","Data not matched!")
Pheno<-droplevels(Pheno) 

#Remove Outliers
rowIQR <- rowIQRs(meth, na.rm = T)
row2575 <- rowQuantiles(meth, probs = c(0.25, 0.75), na.rm = T)
maskL <- meth < row2575[,1] - 3 * rowIQR 
maskU <- meth > row2575[,2] + 3 * rowIQR 
initial_NAs<-rowSums(is.na(meth))
meth[maskL] <- NA
meth[maskU] <- NA

#EWAS function (AD as outcome as the outcome)
RLMtest = function(meth_matrix,methcol, X1, X2, X3, X4) {
  mod = try(rlm(meth_matrix[, methcol]~X1+X2+X3+X4,maxit=200))
  if(class(mod) == "try-error"){
    print(paste("error thrown by column", methcol))
    invisible(rep(NA, 3))
  }else
    cf = coeftest(mod, vcov=vcovHC(mod, type="HC0"))
  cf[2, c("Estimate", "Std. Error", "Pr(>|z|)")]
}
#Run EWAS
meth<-t(meth)
ewas_res <- mclapply(setNames(seq_len(ncol(meth)), dimnames(meth)[[2]]), RLMtest, 
                     meth_matrix=meth, X1=Pheno[,2], X2=Pheno[,3], X3=Pheno[,4], X4=Pheno[,5], mc.cores = 15)

ewas_res<- ldply(ewas_res, rbind) ## coerce results to a data table
names(ewas_res) <- c("probeID", "BETA", "SE", "P_VAL")

#Create N column
meth<-t(meth) # ensure that meth has probes as rows and samples as columns
if(!all(ewas_res$probeID==rownames(meth))){
  ewas_res<- ewas_res[match(rownames(meth),ewas_res$probeID),]} #Double check probes are in the same order in ewas_res and meth and match them if they’re not.

ewas_res$N <-rowSums(!is.na(meth))

#Save file in PACE format:
write.table(ewas_res, "model2.txt", na="NA", quote = FALSE, sep = "\t", row.names = FALSE)