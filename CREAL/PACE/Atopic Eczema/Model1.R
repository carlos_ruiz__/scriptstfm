#=============================#
# PACE Consortium             #
# INMA – models 1a-1b         #
# Carlos Ruiz, CREAL          #
# 24 February 2015            #
#=============================#

#load packages
library(foreign) #to read stata file
library(R.utils) # for gzip
library(data.table)# to process results
library(parallel) # to use multicore approach - part of base R
library(plyr) # to process results
library(matrixStats) #for rowSums, rowIQRs etc.

load("/DATA/Methylation_INMA/450K_blood/QC/FinalSetsComBat.Rdata")
mset <- mset[, pData(mset)$age == 0]
betas <- betas(mset)
phenotypes <- pData(mset)

pheno <- read.dta("/DATA/Methylation_INMA/PACE/atopic_eczema/db/PACE_EWAS_eczema.dta")
rownames(pheno) <- pheno$idnum
phenotypes <- cbind(phenotypes, pheno[phenotypes$idnum, "childhood_ADbis2", drop = FALSE])
phenotypes$ecz <- phenotypes$childhood_ADbis2 == "case"

phenotypes<-subset(phenotypes,!is.na(phenotypes$ecz) & !phenotypes$ethnic_origin %in% c("Black", "American indios"))
betas<-betas[,!is.na(phenotypes$ecz) & !phenotypes$ethnic_origin %in% c("Black", "American indios")]

## Model 1a
Pheno <- phenotypes[, c("Sample_Name", "ecz", "sex")]
colnames(Pheno)[1] <- "aln"

#Pheno is a dataframe containing phenotype information for all samples. The first column contains sample IDs, the second column contains the trait of interest. The trait of interest should be a NUMERIC variable coded 0 (controls) and 1 (cases). All other columns contain covariates. There should be no extra columns. One phenotype file/object per model (i.e. the contents of the object defines the model). In the case of model 1a, this object would contain [ID; childhood AD status; gender; batch].

#meth is a matrix of Illumina beta values for all samples(columns) and probes(rows)

#Match
meth<-betas[,na.omit(match(Pheno$aln,colnames(betas)))]
Pheno<-Pheno[match(colnames(meth),Pheno$aln),]
ifelse(all(Pheno$aln==colnames(meth)), "meth and phenotype data successfully matched","Data not matched!")
Pheno<-droplevels(Pheno) 

#Remove Outliers
rowIQR <- rowIQRs(meth, na.rm = T)
row2575 <- rowQuantiles(meth, probs = c(0.25, 0.75), na.rm = T)
maskL <- meth < row2575[,1] - 3 * rowIQR 
maskU <- meth > row2575[,2] + 3 * rowIQR 
initial_NAs<-rowSums(is.na(meth))
meth[maskL] <- NA
meth[maskU] <- NA

#EWAS function (AD as outcome as the outcome)
GLM = function(methcol, meth_matrix) {
        FORM=formula(paste(paste("Pheno$",colnames(Pheno[2]),sep=""),"~meth_matrix[, methcol]+",paste("Pheno$",colnames(Pheno[-(1:2)]),sep="",collapse="+")))
        mod = glm(FORM,family="binomial") ## logistic regression model between meth data and the binary outcome for eczema 
        cf = summary(mod)$coefficients ## returns the coefficients from the mod (logit model)
        cf[2, c("Estimate", "Std. Error", "Pr(>|z|)")]
}

#Run EWAS
meth<-t(meth)
ewas_res<- mclapply(setNames(seq_len(ncol(meth)), dimnames(meth)[[2]]), GLM, meth_matrix=meth, mc.cores = 10) 
ewas_res<- ldply(ewas_res, rbind) ## coerce results to a data table
names(ewas_res) <- c("probeID", "BETA", "SE", "P_VAL")

#Create N column
meth<-t(meth) # ensure that meth has probes as rows and samples as columns
if(!all(ewas_res$probeID==rownames(meth))){
        ewas_res<- ewas_res[match(rownames(meth),ewas_res$probeID),]} #Double check probes are in the same order in ewas_res and meth and match them if they’re not.

ewas_res$N <-rowSums(!is.na(meth))

# Create N cases and N controls columns (this will only work if your trait of interest is coded correctly! i.e. 0=controls, 1=cases)
ewas_res$N_cases<-rowSums(!is.na(meth[,Pheno[,2]==1]))
ewas_res$N_controls<-rowSums(!is.na(meth[,Pheno[,2]==0]))

#Save file in PACE format:
write.table(ewas_res, paste("INMA_AE_model1a_",Sys.Date(),".txt", sep = ""),na="NA", quote = FALSE)
gzip(paste("INMA_AE_model1a_",Sys.Date(),".txt", sep = ""))


## Model 1b
Pheno <- phenotypes[, c("Sample_Name", "ecz", "sex", "edadm", "smkpreg", "meduc", "sges",
                        "NK", "Bcell", "CD4T", "CD8T", "Eos", "Mono", "Neu")]
Pheno$meduc <- as.factor(Pheno$meduc)
colnames(Pheno)[1] <- "aln"

#Pheno is a dataframe containing phenotype information for all samples. The first column contains sample IDs, the second column contains the trait of interest. The trait of interest should be a NUMERIC variable coded 0 (controls) and 1 (cases). All other columns contain covariates. There should be no extra columns. One phenotype file/object per model (i.e. the contents of the object defines the model). In the case of model 1a, this object would contain [ID; childhood AD status; gender; batch].

#meth is a matrix of Illumina beta values for all samples(columns) and probes(rows)

# Use only complete cases
Pheno <- Pheno[rowSums(is.na(Pheno)) == 0, ]

#Match
meth<-betas[,na.omit(match(Pheno$aln,colnames(betas)))]
Pheno<-Pheno[match(colnames(meth),Pheno$aln),]
ifelse(all(Pheno$aln==colnames(meth)), "meth and phenotype data successfully matched","Data not matched!")
Pheno<-droplevels(Pheno) 

#EWAS function (AD as outcome as the outcome)
GLM = function(methcol, meth_matrix) {
        FORM=formula(paste(paste("Pheno$",colnames(Pheno[2]),sep=""),"~meth_matrix[, methcol]+",paste("Pheno$",colnames(Pheno[-(1:2)]),sep="",collapse="+")))
        mod = glm(FORM,family="binomial") ## logistic regression model between meth data and the binary outcome for eczema 
        cf = summary(mod)$coefficients ## returns the coefficients from the mod (logit model)
        cf[2, c("Estimate", "Std. Error", "Pr(>|z|)")]
}

#Run EWAS
meth<-t(meth)
ewas_res<- mclapply(setNames(seq_len(ncol(meth)), dimnames(meth)[[2]]), GLM, meth_matrix=meth, mc.cores = 10) 
ewas_res<- ldply(ewas_res, rbind) ## coerce results to a data table
names(ewas_res) <- c("probeID", "BETA", "SE", "P_VAL")

#Create N column
meth<-t(meth) # ensure that meth has probes as rows and samples as columns
if(!all(ewas_res$probeID==rownames(meth))){
        ewas_res<- ewas_res[match(rownames(meth),ewas_res$probeID),]} #Double check probes are in the same order in ewas_res and meth and match them if they’re not.

ewas_res$N <-rowSums(!is.na(meth))

# Create N cases and N controls columns (this will only work if your trait of interest is coded correctly! i.e. 0=controls, 1=cases)
ewas_res$N_cases<-rowSums(!is.na(meth[,Pheno[,2]==1]))
ewas_res$N_controls<-rowSums(!is.na(meth[,Pheno[,2]==0]))

#Save file in PACE format:
write.table(ewas_res, paste("INMA_AE_model1b_",Sys.Date(),".txt", sep = ""),na="NA", quote = FALSE)
gzip(paste("INMA_AE_model1b_",Sys.Date(),".txt", sep = ""))



