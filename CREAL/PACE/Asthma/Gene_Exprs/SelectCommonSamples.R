## Set working directory to Asthma results folder
setwd("~/data/WS_INMA/Methylation_INMA/PACE/asthma/")

# Load Expression data
load("~/data/WS_INMA/gene_expression_4y/INMA_HTA-RMA/inma_expset_all_20180615.rda") 

# Load Methylation data 4 years
load("db/preparedata_age4.Rdata")

## Transform ExpressionSet identifiers to INMA
expnames <- colnames(inma.expset)
expnames <- gsub("X", "", expnames)
expnames4 <- paste(expnames, 4, sep = "_")

common4y <- intersect(expnames4, colnames(dat))
common4y <- common4y[!common4y %in% c("04_111_4", "04_613_4", "04_198_4", 
                                      "04_646_4")]

# Load Methylation data 0 years
load("db/preparedata.Rdata")

expnames0 <- paste(expnames, 0, sep = "_")
common0y <- intersect(expnames0, colnames(dat))
common0y <- common0y[!common0y %in% c("04_111_0", "04_613_0", "04_198_0", 
                                   "04_646_0")]

save(common4y, common0y, file = "gene_expr/proc_files/commonSamplesMethExprs.Rdata")