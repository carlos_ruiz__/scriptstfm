### Example Asthma Analysis Code
### Model1b - Preschool wheeze 4y with celltype correction
### Contains summary of probes of wheeze 4y 
### PACE Consortium
### INMA
### Carlos Ruiz
### December 31, 2014
### Updated: January 07, 2016


## Load Libraries
library(MEAL)
library(compare)
library(parallel)
library(plyr)
library(logistf)


### Establish Directories
load("/DATA/Methylation_INMA/PACE/asthma/db/preparedata.Rdata")
outdir<-paste("/DATA/Methylation_INMA/PACE/asthma/results_",chartr(" ", "_", chartr(":", "_", date())), sep="")
dir.create(outdir)
outfile = paste(outdir,"/INMA_wheeze7y_model3b.txt", sep = "")


## Define Function
## Adjusted GLM for binary outcome, i.e. asthma = methylation + covariates
## Set up for 6 covariates: add/remove additional if needed
f.LogF.Adjusted.Try.Parallel = function(methcol, x, y, X1, X2, X3, X4, X5, 
                                        CT1, CT2, CT3, CT4, CT5, CT6, CT7, set) { 
        result = try(logistf(y~x[,methcol]+X1+X2+X3+X4+X5+
                                     CT1+CT2+CT3+CT4+CT5+CT6+CT7, pl=FALSE))
        N <- sum(!is.na(x[,methcol]))
        N_cases <- sum(!is.na(x[y==1,methcol]))
        N_controls <- sum(!is.na(x[y==0,methcol]))
        if(class(result)[1] == "try-error") {
                cat("error thrown by CpG", set,"; ")
                out = c(rep(NA, 3), N, N_cases, N_controls) }
        else {
                coef = result$coef[2]
                se = (diag(result$var)^0.5)[2]
                p.value = result$prob[2]
                out = c(coef, se, p.value, N, N_cases, N_controls) }
        names(out) = c("coef","se", "pvalue","N","N_cases","N_controls")
        return(out)
}



###############################################################################################################
# Read data
load("/DATA/Methylation_INMA/450K_blood/QC/cellCounts.cordRef.full.rda")
cellCounts.cordRef <- as.data.frame(cellCounts.cordRef)
counts <- cellCounts.cordRef[rownames(phenotypes), ]
phenotypes <- phenotypes[, -c(68:74)]
phenotypes <- cbind(phenotypes, counts)

dim(dat)
dim(phenotypes)
dim(counts)

compare(rownames(phenotypes),colnames(dat))
if(compare(rownames(phenotypes),colnames(dat))[[1]]==FALSE) stop("Sample order does not match.")

###############################################################################################################
## OUTCOME of interest

## Preschool wheeze case/control variable 
table(phenotypes$wheezeV7,useNA="always")


###############################################################################################################
## SUBSET DATA TO THOSE NOT MISSING OUTCOME VARIABLE OF INTEREST

### Preschool Wheeze
phenotypes2<-subset(phenotypes,!is.na(phenotypes$wheezeV7) & !phenotypes$ethnic_origin %in% c("Black", "American indios"))
dat2<-dat[,!is.na(phenotypes$wheezeV7) & !phenotypes$ethnic_origin %in% c("Black", "American indios")]


dim(phenotypes2)
dim(dat2)

#once verified looks ok, give original file name for ease of code below
phenotypes<-phenotypes2
tdat<-t(dat2)

#outcome variable
asthma<-phenotypes$wheezeV7
table(asthma,useNA="always")


###############################################################################################################
## SUMMARIZE PROBES & CELL TYPES

descriptives<-function(x){
  tmp<-c(min(x,na.rm=T), quantile(x,probs=c(.1,.25,.5),na.rm=T), mean(x,na.rm=T), sd(x,na.rm=T),
         quantile(x,probs=c(.75,.90),na.rm=T),max(x,na.rm=T),sum(is.na(x)))
  names(tmp)[c(1,5:6,9:10)]<-c("Min.","Mean","SD","Max.","NA")
  return(tmp)
}

desc<-t(apply(tdat,2,descriptives))
write.table(desc, file = paste(outdir,"/INMA_Descriptives.txt", sep = ""), sep = "\t", col.names = T, row.names = T, append = F, quote=FALSE)

celltype_desc<-t(apply(phenotypes[,which(names(phenotypes) %in% names(counts))],2,descriptives))
write.table(celltype_desc, file = paste(outdir,"/INMA_CellType_Descriptives.txt", sep = ""), sep = "\t", col.names = T, row.names = T, append = F, quote=FALSE)

###############################################################################################################
## ORGANIZE COVARIATES 

### Child gender
sex <- phenotypes$sex
table(sex,useNA="always")


### Age
age <- phenotypes$edadm
summary(age) ; sd(age)


### Maternal asthma
masthmaany <- phenotypes$ia1m
table(masthmaany,useNA="always")


### Smoking
smk2 <- phenotypes$smkpreg
table(smk2,useNA="always")

### Maternal Education - SES stand in
meducation<-as.factor(phenotypes$meduc)
table(meducation,useNA="always")


### Batch
# not necessary in MoBa analyses since methylation data has been ComBat corrected

### Celltype
celltype<-as.matrix(phenotypes[,which(names(phenotypes) %in% names(counts))])
if(compare(colnames(celltype),c("nRBC","Gran","CD4T","CD8T","Bcell","Mono","NK"),allowAll=T)[[1]]==FALSE){
        stop("Incorrect cell types used.")
}

cov_desc<-c(sum(sex=="Male"),mean(age), sd(age), sum(masthmaany=="yes"), table(smk2), table(meducation))
names(cov_desc)<-c("sex (male)","age (mean)","age (sd)","masthmaany","smk2 (0)","smk2 (1)","smk2 (2)","meducation (university)","meducation (High Sch)","meducation (Some college)")
write.table(cov_desc, file = paste(outdir,"/INMA3b_covariates_table1.txt", sep = ""), sep = "\t", col.names = T, row.names = T, append = F, quote=FALSE)

###############################################################################################################
#Create covariate list

# X <- data.frame(age, smk2, masthmaany, sex, meducation)
X <- data.frame(age, smk2, masthmaany, sex, meducation, celltype)

###############################################################################################################
#Analysis

detectCores()

# ### TEST
# tdat_sub<-tdat[,1:10]
# indiv.res_test <- mclapply(setNames(seq_len(ncol(tdat_sub)), 
# dimnames(tdat_sub)[[2]]), 
# 	f.GLM.Bin.Adjusted.Try.Parallel, x=tdat_sub, y=asthma, 
# 		X1=X[,1], X2=X[,2], X3=X[,3], X4=X[,4], X5=X[,5], 
# 		CT1=X[,6], CT2=X[,7], CT3=X[,8], CT4=X[,9], CT5=X[,10], 
#		CT6=X[,11], CT7=X[,12])
# ########


system.time(
        indiv.res <- mclapply(setNames(seq_len(ncol(tdat)), dimnames(tdat)[[2]]), 
                              f.LogF.Adjusted.Try.Parallel, x=tdat, y=asthma, 
                              X1=X[,1], X2=X[,2], X3=X[,3], X4=X[,4], X5=X[,5], 
                              CT1=X[,6], CT2=X[,7], CT3=X[,8], CT4=X[,9], CT5=X[,10],
                              CT6=X[,11], CT7=X[,12], mc.cores = 40)
)
# coerce the list back to a data.frame
indiv.res.par <- ldply(indiv.res, rbind)
names(indiv.res.par) <- c("CpG","coef","se", "pvalue")

write.table(indiv.res.par, file = outfile, sep = "\t", col.names = T, row.names = F, append = F, quote=FALSE)

## Calculate lambda
lambda <- qchisq(median(indiv.res.par$pvalue,na.rm=T), df = 1, lower.tail = F)/qchisq(0.5, 1)

## Save analysis sample sizes & lambda:
N<-dim(tdat)[1]
N_case<-sum(asthma==1)
N_control<-sum(asthma==0)
save(N,N_case,N_control,lambda,file=paste(outdir,"/sample_size_lambda.RData",sep=""))
