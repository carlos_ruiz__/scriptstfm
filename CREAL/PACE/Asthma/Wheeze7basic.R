### Example Asthma Analysis Code
### Model3a - school wheeze basic
### PACE Consortium
### INMA
### Carlos Ruiz
### December 31, 2014
### Updated: January 08, 2016


## Load Libraries
library(MEAL)
library(compare)
library(parallel)
library(plyr)


### Establish Directories
load("/DATA/Methylation_INMA/PACE/asthma/db/preparedata.Rdata")
outdir<-paste("/DATA/Methylation_INMA/PACE/asthma/results_",chartr(" ", "_", chartr(":", "_", date())), sep="")
dir.create(outdir)
outfile = paste(outdir,"/INMA_wheeze7y_model3a.txt", sep = "")


## Define Function
## Adjusted GLM for binary outcome, i.e. asthma = methylation + covariates
## Set up for 6 covariates: add/remove additional if needed
f.GLM.Bin.Adjusted.Try.Parallel = function(methcol, x, y, X1, X2, X3, X4, X5) {
  mod = try(glm(y~x[,methcol]+X1+X2+X3+X4+X5, family=binomial))
  if( sum(class(mod) %in% "try-error") > 0){
    cat("error thrown by column", methcol," ; ")
    invisible(rep(NA, 3))
  }else{
    if (floor(methcol/1000)==methcol/1000){cat(methcol," of ",dim(x)[2]," ; ")}
    cf = summary(mod)$coefficients
  }
  cf[2, c("Estimate", "Std. Error", "Pr(>|z|)")]
}



###############################################################################################################
# Read data
dim(dat)
dim(phenotypes)
dim(counts)

compare(rownames(phenotypes),colnames(dat))
if(compare(rownames(phenotypes),colnames(dat))[[1]]==FALSE) stop("Sample order does not match.")

###############################################################################################################
## OUTCOME of interest

## Preschool wheeze case/control variable 
table(phenotypes$wheezeV7,useNA="always")


###############################################################################################################
## SUBSET DATA TO THOSE NOT MISSING OUTCOME VARIABLE OF INTEREST

### Preschool Wheeze
phenotypes2<-subset(phenotypes,!is.na(phenotypes$wheezeV7) & !phenotypes$ethnic_origin %in% c("Black", "American indios"))
dat2<-dat[,!is.na(phenotypes$wheezeV7) & !phenotypes$ethnic_origin %in% c("Black", "American indios")]


dim(phenotypes2)
dim(dat2)

#once verified looks ok, give original file name for ease of code below
phenotypes<-phenotypes2
tdat<-t(dat2)

#outcome variable
asthma<-phenotypes$wheezeV7
table(asthma,useNA="always")


###############################################################################################################
## ORGANIZE COVARIATES 

### Child gender
sex <- phenotypes$sex
table(sex,useNA="always")


### Age
age <- phenotypes$edadm
summary(age) ; sd(age)


### Maternal asthma
masthmaany <- phenotypes$ia1m
table(masthmaany,useNA="always")


### Smoking
smk2 <- phenotypes$smkpreg
table(smk2,useNA="always")

### Maternal Education - SES stand in
meducation<-as.factor(phenotypes$meduc)
table(meducation,useNA="always")


###############################################################################################################
#Create covariate list

# X <- data.frame(age, smk2, masthmaany, sex, meducation)
X <- data.frame(age, smk2, masthmaany, sex, meducation)

###############################################################################################################
#Analysis

detectCores()

# ### TEST
# tdat_sub<-tdat[,1:10]
# indiv.res_test <- mclapply(setNames(seq_len(ncol(tdat_sub)), 
# dimnames(tdat_sub)[[2]]), 
# 	f.GLM.Bin.Adjusted.Try.Parallel, x=tdat_sub, y=asthma, 
# 		X1=X[,1], X2=X[,2], X3=X[,3], X4=X[,4], X5=X[,5], 
# 		CT1=X[,6], CT2=X[,7], CT3=X[,8], CT4=X[,9], CT5=X[,10], 
#		CT6=X[,11], CT7=X[,12])
# ########


system.time(
  indiv.res <- mclapply(setNames(seq_len(ncol(tdat)), dimnames(tdat)[[2]]), 
                        f.GLM.Bin.Adjusted.Try.Parallel, x=tdat, y=asthma, 
                        X1=X[,1], X2=X[,2], X3=X[,3], X4=X[,4], X5=X[,5], mc.cores = 5)
)
# coerce the list back to a data.frame
indiv.res.par <- ldply(indiv.res, rbind)
names(indiv.res.par) <- c("CpG","coef","se", "pvalue")

write.table(indiv.res.par, file = outfile, sep = "\t", col.names = T, row.names = F, append = F, quote=FALSE)

## Calculate lambda
lambda <- qchisq(median(indiv.res.par$pvalue,na.rm=T), df = 1, lower.tail = F)/qchisq(0.5, 1)

## Save analysis sample sizes & lambda:
N<-dim(tdat)[1]
N_case<-sum(asthma==1)
N_control<-sum(asthma==0)
save(N,N_case,N_control,lambda,file=paste(outdir,"/sample_size_lambda.RData",sep=""))