# Script to test analyze neuro effect in INMA samples
library(MEAL)
library(foreign)

load("/DATA/Methylation_INMA/450K_blood/QC/FinalSetsEURComBat.Rdata")
para <- read.dta("/DATA/Methylation_INMA/PACE/paracetamol/data/2aepigen21jan.dta")
para2 <- read.dta("/DATA/Methylation_INMA/PACE/paracetamol/data/bdepigen02mar.dta")

para <- subset(para, cohorte == 3)
rownames(para) <- para$idnum
para <- para[, -c(1:2)]

para2 <- subset(para2, cohort8 == "Sabadell")
rownames(para2) <- para2$idnum
para2 <- para2[, -c(1:2)]

mset <- mset[, mset$age == 0]
pheno <- pData(mset)
pheno <- cbind(pheno, para[pheno$idnum, ], para2[pheno$idnum, ])
pData(mset) <- pheno


# Crude analysis
# Methylation = ptotal_cast  + sex
castset <- mset[, !is.na(pheno[, "ptotal_cast"])]
cast <- DAPipeline(castset, "ptotal_cast", covariable_names = "sex")
# Adjusted analysis
# Methylation = ptotal_cast + sex + gestational age + maternal age + maternal smoking during preg (1/2/3) + maternal education (1/2/3)
castset <- castset[, rowSums(is.na(pData(castset)[, c("sex", "edadm", "sges", "smkpreg", "meduc")])) == 0]
castadj <- DAPipeline(castset, "ptotal_cast", covariable_names = c("sex", "edadm", "sges", "smkpreg", "meduc"))

# Crude analysis
# Methylation =  nsymp_Hyperactivity + sex
hyperset <- mset[, !is.na(pheno[, "nsymp_Hyperactivity"])]
hyper <- DAPipeline(hyperset, "nsymp_Hyperactivity", covariable_names = "sex")
# Adjusted analysis
# Methylation = nsymp_Hyperactivity + sex + gestational age + maternal age + maternal smoking during preg (1/2/3) + maternal education (1/2/3)
hyperset <- hyperset[, rowSums(is.na(pData(hyperset)[, c("sex", "edadm", "sges", "smkpreg", "meduc")])) == 0]
hyperadj <- DAPipeline(hyperset, "nsymp_Hyperactivity", covariable_names = c("sex", "edadm", "sges", "smkpreg", "meduc"))

# Methylation = Comissions_global   + sex
comset <- mset[, !is.na(pheno[, "Comissions_global"])]
com <- DAPipeline(comset, "Comissions_global", covariable_names = "sex")
# Adjusted analysis
# Methylation = Comissions_global + sex + gestational age + maternal age + maternal smoking during preg (1/2/3) + maternal education (1/2/3)
comset <- comset[, rowSums(is.na(pData(comset)[, c("sex", "edadm", "sges", "smkpreg", "meduc")])) == 0]
comadj <- DAPipeline(comset, "Comissions_global", covariable_names = c("sex", "edadm", "sges", "smkpreg", "meduc"))

# Methylation =  Detectability_global + sex
detectset <- mset[, !is.na(pheno[, "Detectability_global"])]
detect <- DAPipeline(detectset, "Detectability_global", covariable_names = "sex")
# Adjusted analysis
# Methylation = Detectability_global + sex + gestational age + maternal age + maternal smoking during preg (1/2/3) + maternal education (1/2/3)
detectset <- detectset[, rowSums(is.na(pData(detectset)[, c("sex", "edadm", "sges", "smkpreg", "meduc")])) == 0]
detectadj <- DAPipeline(detectset, "Detectability_global", covariable_names = c("sex", "edadm", "sges", "smkpreg", "meduc"))

write.table(probeResults(cast)[[1]][1:100,], "cast.txt", na="NA", quote = FALSE, sep = "\t", row.names = TRUE)
write.table(probeResults(castadj)[[1]][1:100,], "cast_adj.txt", na="NA", quote = FALSE, sep = "\t", row.names = TRUE)
write.table(probeResults(hyper)[[1]][1:100,], "hyper.txt", na="NA", quote = FALSE, sep = "\t", row.names = TRUE)
write.table(probeResults(hyperadj)[[1]][1:100,], "hyper_adj.txt", na="NA", quote = FALSE, sep = "\t", row.names = TRUE)
write.table(probeResults(com)[[1]][1:100,], "com.txt", na="NA", quote = FALSE, sep = "\t", row.names = TRUE)
write.table(probeResults(comadj)[[1]][1:100,], "com_adj.txt", na="NA", quote = FALSE, sep = "\t", row.names = TRUE)
write.table(probeResults(detect)[[1]][1:100,], "detect.txt", na="NA", quote = FALSE, sep = "\t", row.names = TRUE)
write.table(probeResults(detectadj)[[1]][1:100,], "detect_adj.txt", na="NA", quote = FALSE, sep = "\t", row.names = TRUE)
save(cast, castadj, hyper, hyperadj, com, comadj, detect, detectadj, file = "Results_neuro.Rdata")

write.table(sampleNames(cast), "cast_ids.txt", na="NA", quote = FALSE, sep = "\t", row.names = TRUE)
write.table(sampleNames(castadj), "cast_adj_ids.txt", na="NA", quote = FALSE, sep = "\t", row.names = TRUE)
write.table(sampleNames(hyper), "hyper_ids.txt", na="NA", quote = FALSE, sep = "\t", row.names = TRUE)
write.table(sampleNames(hyperadj), "hyper_adj_ids.txt", na="NA", quote = FALSE, sep = "\t", row.names = TRUE)
write.table(sampleNames(com), "com_ids.txt", na="NA", quote = FALSE, sep = "\t", row.names = TRUE)
write.table(sampleNames(comadj), "com_adj_ids.txt", na="NA", quote = FALSE, sep = "\t", row.names = TRUE)
write.table(sampleNames(detect), "detect_ids.txt", na="NA", quote = FALSE, sep = "\t", row.names = TRUE)
write.table(sampleNames(detectadj), "detect_adj_ids.txt", na="NA", quote = FALSE, sep = "\t", row.names = TRUE)
  
