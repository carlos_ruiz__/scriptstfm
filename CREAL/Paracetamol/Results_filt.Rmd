---
title: "Paracetamol vs Methylation"
author: "Carlos Ruiz"
date: "9 de febrero de 2016"
output: pdf_document
---
In this analyses, probes containing CpGs, crosshibridated probes and sexual probes have been removed.

# Load data

```{r}
load("C:/Users/cruiz/Documents/scriptstfm/CREAL/Paracetamol/Results_filt.Rdata")
library(MEAL)
```

# Crude analysis

## Ever
```{r, dev='CairoPNG'}
plotQQ(ever)
plotEWAS(ever)
probeResults(ever)[[1]][1:10,]
```

## Long 

### Never vs Sporadic
```{r, dev='CairoPNG'}
plotQQ(dosis, "para_long1")
plotEWAS(dosis, "para_long1")
probeResults(dosis)[[1]][1:10,]
```

### Never vs Persistent
```{r, dev='CairoPNG'}
plotQQ(dosis, "para_long2")
plotEWAS(dosis, "para_long2")
probeResults(dosis)[[2]][1:10,]
```


# Adjusted analysis

## Ever
```{r, dev='CairoPNG'}
plotQQ(ever_adj)
plotEWAS(ever_adj)
probeResults(ever_adj)[[1]][1:10,]
```

## Long 

### Never vs Sporadic
```{r, dev='CairoPNG'}
plotQQ(dosis_adj, "para_long1")
plotEWAS(dosis_adj, "para_long1")
probeResults(dosis_adj)[[1]][1:10,]
```

### Never vs Persistent
```{r, dev='CairoPNG'}
plotQQ(dosis_adj, "para_long2")
plotEWAS(dosis_adj, "para_long2")
probeResults(dosis_adj)[[2]][1:10,]
```