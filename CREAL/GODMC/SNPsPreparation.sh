#' Script to prepare SNPs data for GODMC

cd /DATA/GoDMC

for i in {1..22}
do

echo \"/DATA/GWAS_INMA/QC/IMPUTATION_1000_GENOME_MARCH2012/RESULTS/IMPUTATIONS_NEW/IMPUTED_chr\"$i\"\"
j=/DATA/GWAS_INMA/QC/IMPUTATION_1000_GENOME_MARCH2012/RESULTS/IMPUTATIONS_NEW/IMPUTED_chr$i
o=./IMPUTED_1000_GENOME_$i.gen

/DATA/GWAS_INMA/EGG_EAGLE_OCT2013/Tools/gtool -S --g $j --s /DATA/GWAS_INMA/QC/BD_FINAL_QC/INMA_QC3_FINAL.sample --og $o  --os IMPUT_WITHOUT_PREPHASE_FINAL.sample --sample_id /DATA/GoDMC/goodsamples.list 

done


#Then run the bash script below.

#!/bin/bash
for i in {1..22}
do
## Add chromosome number to files
awk -v i=$i '$1=i' IMPUTED_1000_GENOME_$i.gen > data_chr${i}.gen

# First filter out snps (info<0.8) and maf <0.01 and samples using qctool.
/home/cruiz/Software/qctool_v1.4-linux-x86_64/qctool -g data_chr$i.gen -og filteredchr${i}.bgen -maf 0.01 1 -info 0.8 1
# Now calculate summary stats on the filtered data. Please note you need to exclude the samples from the .sample file manually. 
/home/cruiz/Software/qctool_v1.4-linux-x86_64/qctool -g filteredchr${i}.bgen -snp-stats data_chr$i.snp-stats

# Now extract the best guess data from the bgen files, variants with a "." will be recoded to chr:pos_allele1_allele2.

/home/cruiz/Software/plink1.90/plink --bgen filteredchr${i}.bgen snpid-chr --sample IMPUT_WITHOUT_PREPHASE_FINAL.sample --make-bed --out data_chr${i}_filtered --hard-call-threshold 0.499999
/home/cruiz/Software/plink1.90/plink --bfile data_chr${i}_filtered --make-bed --out data_chr${i}_filtered --set-missing-var-ids @:#\$1,\$2

# Rename the SNP IDs if necessary to avoid possible duplicates

cp data_chr${i}_filtered.bim data_chr${i}_filtered.bim.orig
awk '{
if (($5 == "A" || $5 == "T" || $5 == "C" || $5=="G") &&  ($6 == "A" || $6 == "T" || $6 == "C" || $6=="G")) 
print $1, "chr"$1":"$4":SNP", $3, $4, $5, $6;
else 
print $1, "chr"$1":"$4":INDEL", $3, $4, $5, $6;
}' data_chr${i}_filtered.bim.orig > data_chr${i}_filtered.bim

# For simplicity remove any duplicates

cp data_chr${i}_filtered.bim data_chr${i}_filtered.bim.orig2
awk '{
if (++dup[$2] > 1) { 
print $1, $2".duplicate."dup[$2], $3, $4, $5, $6 
} else { 
print $0 }
}' data_chr${i}_filtered.bim.orig2 > data_chr${i}_filtered.bim
    grep "duplicate" data_chr${i}_filtered.bim | awk '{ print $2 }' > duplicates.chr${i}.txt

/home/cruiz/Software/plink1.90/plink --bfile data_chr${i}_filtered --exclude duplicates.chr${i}.txt --make-bed --out data_chr${i}_filtered


# Relabel the SNP IDs and extract relevant columns in the snp-stats file
# Assumes column 4 is the position
# Assumes columns 5 and 6 are the allele names
# Assumes column 15 is the MAF
# Assumes columns 19 is the info score

awk -v chr=$i '{
if (($5 == "A" || $5 == "T" || $5 == "C" || $5=="G") &&  ($6 == "A" || $6 == "T" || $6 == "C" || $6=="G")) 
print "chr"chr":"$4":SNP", $15, $19;
else 
print "chr"chr":"$4":INDEL", $15, $19;
}' data_chr${i}.snp-stats > data_chr${i}.info

# Remove duplicates from snp-stats
cp data_chr${i}.info data_chr${i}.info.orig
awk '{
if (++dup[$1] > 1) {
print $1".duplicate."dup[$1], $2, $3
} else {
print $0 }
}' data_chr${i}.info.orig > data_chr${i}.info
    fgrep -v -w -f duplicates.chr${i}.txt <data_chr${i}.info >chr${i}_filtered.info
done

# Merge them into one dataset

for i in {2..22}
do 
echo "data_chr${i}_filtered"
done > mergefile.txt

/home/cruiz/Software/plink1.90/plink --bfile data_chr1_filtered --merge-list mergefile.txt --make-bed --out data_filtered

# Combine info files into a single file

head -n1 chr1_filtered.info > data_filtered.info

for i in {1..22}
do
awk ' NR>1 {print $0}' < chr${i}_filtered.info |cat >> data_filtered.info
done


# Merge them into one dataset with rs names

for i in {1..22}
do 
/home/cruiz/Software/plink1.90/plink --bgen filteredchr${i}.bgen snpid-chr --sample IMPUT_WITHOUT_PREPHASE_FINAL.sample --make-bed --out data_chr${i}_bis --hard-call-threshold 0.499999
/home/cruiz/Software/plink1.90/plink --bfile data_chr${i}_bis --make-bed --out data_chr${i}_bis --set-missing-var-ids @:#\$1,\$2
# For simplicity remove any duplicates

cp data_chr${i}_bis.bim data_chr${i}_bis.orig2.bim
awk '{
if (++dup[$2] > 1) { 
print $1, $2".duplicate."dup[$2], $3, $4, $5, $6 
} else { 
print $0 }
}' data_chr${i}_bis.orig2.bim > data_chr${i}_bis.bim
    grep "duplicate" data_chr${i}_bis.bim | awk '{ print $2 }' > duplicates.chr${i}.orig.txt

/home/cruiz/Software/plink1.90/plink --bfile data_chr${i}_bis --exclude duplicates.chr${i}.orig.txt --make-bed --out data_chr${i}_bis
done

for i in {2..22}
do 
echo "data_chr${i}_bis"
done > mergefile_ori.txt
/home/cruiz/Software/plink1.90/plink --bfile data_chr1_bis --merge-list mergefile_ori.txt --make-bed --out data_filtered_bis
