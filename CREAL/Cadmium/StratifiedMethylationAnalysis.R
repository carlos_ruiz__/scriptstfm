#' Methylation Analysis using MEAL testing all the models in the stratified sets

#Load files and library
load("/home/cruiz/CREAL/Cadmium/CadmiumSexSetSVA.Rdata")
library(MEAL)

#boys
#' Methylation Analysis 
#' Variables: Cadmium (cdgg)
#' Covariables: none
boysraw <- DAPipeline(set = boys, variable_names = "cdgg", verbose = TRUE)

#' Methylation Analysis Stratified by sex
#' Variables: Cadmium (cdgg)
#' Covariables: sex (sexo)
boysbase <- DAPipeline(set = boys, variable_names = "cdgg", 
                              covariable_names = "cohorte", verbose = TRUE)


#girls
#' Methylation Analysis Stratified by sex
#' Variables: Cadmium (cdgg)
#' Covariables: none
girlsraw <- DAPipeline(set = girls, variable_names = "cdgg", verbose = TRUE)

#' Methylation Analysis Stratified by sex
#' Variables: Cadmium (cdgg)
#' Covariables: sex (sexo)
girlsbase <- DAPipeline(set = girls, variable_names = "cdgg", 
                           covariable_names = "cohorte", verbose = TRUE)

#' Methylation Analysis Stratified by sex
#' Variables: Cadmium (cdgg)
#' Covariables: sex (sexo), three first SVs (X1, X2, X3)
girlsSVA <- DAPipeline(set = girls, variable_names = "cdgg", 
                             covariable_names = c("cohorte", "X1", "X2", "X3"),
                             verbose = TRUE)

#' Methylation Analysis Stratified by sex
#' Variables: Cadmium (cdgg)
#' Covariables: three first SVs (X1base, X2base, X3base)
girlsSVAraw <- DAPipeline(set = girls, variable_names = "cdgg", 
                           covariable_names = c("X1base", "X2base", "X3base"),
                           verbose = TRUE)


save(boysraw, boysbase, girlsraw, girlsbase, girlsSVA, girlsSVAraw, file = "/home/cruiz/CREAL/Cadmium/cadmiumsexResults.Rdata")
