#'##############################################################################
#'##############################################################################
#' Create final HELIX SNPs files
#'##############################################################################
#'##############################################################################

## Convert VCFs to plink 

### Compress vcfs (folder ~/data/WS_HELIX/HELIX_preproc/gwas/HRC_imp/Final_data_Michigan)
for i in `ls *.vcf` 
do
bgzip $i
done

### Remove SNPs with R2 < 0.8 (folder ~/data/WS_HELIX/HELIX_preproc/gwas/HRC_imp/)
for i in {1..22}
do
~/data/software/plink2/plink2 --vcf Final_data_Michigan/chr$i.dose.vcf.gz --max-alleles 2 --extract-if-info "R2 >= 0.8" --make-bed --out QC_post_imp/VCF_PLINK/chr$i
done

### Merge plinks (folder ~/data/WS_HELIX/HELIX_preproc/gwas/HRC_imp/QC_post_imp/VCF_PLINK)
for i in {1..22}
do
echo chr$i >> merge.list
done 

plink  --merge-list merge.list --make-bed --out HELIX_GWAS_VCF_PLINK
# 
# PLINK v1.90b6.2 64-bit (12 Jun 2018)           www.cog-genomics.org/plink/1.9/
# (C) 2005-2018 Shaun Purcell, Christopher Chang   GNU General Public License v3
# Logging to HELIX_GWAS_VCF_PLINK.log.
# Options in effect:
#   --exclude HELIX_GWAS_VCF_PLINK-merge.missnp
#   --make-bed
#   --merge-list merge.list
#   --out HELIX_GWAS_VCF_PLINK
# 
# 257755 MB RAM detected; reserving 128877 MB for main workspace.
# Error: 6510 variants with 3+ alleles present.
# * If you believe this is due to strand inconsistency, try --flip with
#   HELIX_GWAS_VCF_PLINK-merge.missnp.
#   (Warning: if this seems to work, strand errors involving SNPs with A/T or C/G
#   alleles probably remain in your data.  If LD between nearby SNPs is high,
#   --flip-scan should detect them.)
# * If you are dealing with genuine multiallelic variants, we recommend exporting
#   that subset of the data to VCF (via e.g. '--recode vcf'), merging with
#   another tool/script, and then importing the result; PLINK is not yet suited
#   to handling them.

## Exclude SNPs with multiple alleles from plinks

### Remove SNPs with R2 < 0.8 (folder ~/data/WS_HELIX/HELIX_preproc/gwas/HRC_imp/)
for i in {1..22}
do
~/data/software/plink2/plink2 --vcf Final_data_Michigan/chr$i.dose.vcf.gz --max-alleles 2 --extract-if-info "R2 >= 0.8" --exclude QC_post_imp/VCF_PLINK/HELIX_GWAS_VCF_PLINK-merge.missnp --make-bed --out QC_post_imp/VCF_PLINK/chr$i
done

### Merge plinks (folder ~/data/WS_HELIX/HELIX_preproc/gwas/HRC_imp/QC_post_imp/VCF_PLINK)
plink  --merge-list merge.list --make-bed --out HELIX_GWAS_VCF_PLINK 