#' Script to annotate the cpgs to the SNPs
#### Bash code
curl -O ftp://hgdownload.soe.ucsc.edu/goldenPath/hg19/database/snp147Common.txt.gz

gunzip -c snp147Common.txt.gz | cut -f2,3,4,5,7,8,12,25 | gzip - > snp147All_small.txt.gz

#### Download file from UCSC -> hg19 -> annotation database -> Common
processUCSCsnp <- function(snpfile) {
  require(GenomicRanges)
  cat("Reading file\n")
  df <- read.delim(gzfile(snpfile), header = FALSE,
                   stringsAsFactors = FALSE)
  names(df) <- c("chr", "start", "end", "name", "strand",
                 "refNCBI", "class", "alleleFreqs")
  print(table(df$chr))
  cat("Only keeping chrs 1-22, X, Y\n")
  df <- df[df$chr %in% paste0("chr", c(1:22, "X", "Y")),]
  print(table(df$class))
  cat("Only keeping class 'single'\n")
  df <- df[df$class == "single",]
  cat("Computing MAF\n")
  df$alleleFreqs <- sub(",$", "", df$alleleFreqs)
  sp <- strsplit(df$alleleFreqs, ",")
  sp <- lapply(sp, function(x) ifelse(!length(x), NA, x))
  minFreq <- sapply(sp, function(xx) min(as.numeric(xx)))
  cat("Instatiating object\n")
  rm(sp)
  gc()
  grSNP <- GRanges(seqnames = df$chr, strand = df$strand, ranges = IRanges(start = df$start + 1, end = df$end),
                   MAF = minFreq, ref = df$refNCBI)
  names(grSNP) <- df$name
  grSNP
}

### Uncompress prior to processing
grSNP <- processUCSCsnp("snp147All_small.txt.gz")

library(IlluminaHumanMethylation450kanno.ilmn12.hg19)
anno <- get("IlluminaHumanMethylation450kanno.ilmn12.hg19")
Locations <- anno@data$Locations
Manifest <- anno@data$Manifest

map <- cbind(Locations, Manifest)
map <- GRanges(seqnames = map$chr, ranges = IRanges(start = map$pos, width = 1),
               Strand = map$strand, Type = map$Type)
map <- minfi:::getProbePositionsDetailed(map)
names(map) <- rownames(Locations)


doSNPoverlap <- function (map, grSnp) 
{
  stopifnot(is(map, "GRanges"))
  stopifnot(is(grSnp, "GRanges"))
  stopifnot(all(c("SBE", "probeStart", "probeEnd") %in% names(mcols(map))))
  cat("removing Snps with width != 1\n")
  grSnp <- grSnp[width(grSnp) == 1]
  cpgGR <- GRanges(seqnames(map), IRanges(start(map), width = 2))
  ooCpG <- findOverlaps(cpgGR, grSnp)
  sbeGR <- GRanges(seqnames(map), IRanges(map$SBE, map$SBE))
  ooSbe <- findOverlaps(sbeGR, grSnp)
  
  probeGR <- GRanges(seqnames(map), IRanges(map$probeStart, map$probeEnd))
  ooProbe <- findOverlaps(probeGR, grSnp, ignore.strand = TRUE)
  snpAnno <- DataFrame(matrix(nrow = length(map), ncol = 6))
  colnames(snpAnno) = c("Probe_rs", "Probe_maf", "CpG_rs", 
                        "CpG_maf", "SBE_rs", "SBE_maf")
  rownames(snpAnno) <- names(map)
  snpAnno$Probe_rs[queryHits(ooProbe)] <- names(grSnp)[subjectHits(ooProbe)]
  snpAnno$Probe_maf[queryHits(ooProbe)] <- grSnp$MAF[subjectHits(ooProbe)]
  snpAnno$CpG_rs[queryHits(ooCpG)] <- names(grSnp)[subjectHits(ooCpG)]
  snpAnno$CpG_maf[queryHits(ooCpG)] <- grSnp$MAF[subjectHits(ooCpG)]
  snpAnno$SBE_rs[queryHits(ooSbe)] <- names(grSnp)[subjectHits(ooSbe)]
  snpAnno$SBE_maf[queryHits(ooSbe)] <- grSnp$MAF[subjectHits(ooSbe)]
  snpAnno
}

SNPs147All <- doSNPoverlap(map, grSNP)

SNPs147 <- data.frame(SNPs147All[, c(2,4,6)])
SNPs147[is.na(SNPs147)] <- 0
SNPs147[(SNPs147) == "Inf"] <- 0
SNPs147[(SNPs147)  > 0.5] <- 1 - SNPs147[(SNPs147)  > 0.5] 
SNPs147All[, c(2, 4, 6)] <- DataFrame(SNPs147)

save(grSNP, SNPs147All, file = "/Lacie_CRW10023/HELIX_preproc/methylation/QC_CRA/SNPsProbesData/SNPinCpGfiles.Rdata")