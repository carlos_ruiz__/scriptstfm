#' Create final sets from final GenomicMethylationSet
load("/Lacie_CRW10023/HELIX_preproc/methylation/QC_CRA/GenomicMethylationSetAllGoodSamplesNewPheno.Rdata")
load("/Lacie_CRW10023/HELIX_preproc/methylation/QC_CRA/badprobes.Rdata")

## Remove Hapmap Samples
set <- set[, -grep("NA|Na", set$LabID)]

# Add 6 Cell types 
load("cellCounts6types.Rdata")
colData(set) <- cbind(colData(set), cellCounts[colnames(set), ])
colnames(colData(set))[40:45] <- paste0(colnames(colData(set))[40:45], "_6")


## Remove duplicates
grset <- set[ , !duplicated(set$SampleID)]
colnames(grset) <- grset$SampleID
pheno <- colData(grset)

# Add ethnicity
load("../QC_MB/ethn_dummy.Rdata")
pheno$ethn_dummy <- ethn_dummy[rownames(pheno), "ethn_dummy"]
        
ethn <- read.table("../Epistructure/HELIX_EthnPCs_FINAL_ALLsamples.txt", sep="\t", header=T)
row.names(ethn) <- ethn$SampleID

pheno <- cbind(pheno, ethn[rownames(pheno), -1])

# add ethnicity_3cat
pheno$h_ethnicity_3cat <- NA
pheno$h_ethnicity_3cat[pheno$h_ethnicity_c=="African"]<-"Other"
pheno$h_ethnicity_3cat[pheno$h_ethnicity_c=="Asian"]<-"Pakistani_Asian"
pheno$h_ethnicity_3cat[pheno$h_ethnicity_c=="Caucasian"]<-"WhiteEur_WhiteOther"
pheno$h_ethnicity_3cat[pheno$h_ethnicity_c=="Native_American"]<-"Other"
pheno$h_ethnicity_3cat[pheno$h_ethnicity_c=="Other"]<-"Other"
pheno$h_ethnicity_3cat[pheno$h_ethnicity_c=="Pakistani"]<-"Pakistani_Asian"

## Add bmi
bmi <- read.delim("~/data/WS_HELIX/HELIX_preproc/exposome/FinalDataset/zbmi_helix_clinical_harmo_27nov2017.csv", sep=",")
pheno <- merge(pheno, bmi, by="HelixID")
rownames(pheno) <- pheno$SampleID
pheno <- pheno[colnames(grset), ]

### Add last meal and blood collection
sample <- read.csv("~/data/WS_HELIX/HELIX_raw/HELIX_db_omicsQC_sample_data_20180305_withoutcodebook.csv", sep=",")
rownames(sample) <- sample$SampleID
pheno <- cbind(pheno, sample[rownames(pheno), c("hs_dift_mealblood_imp", "blood_sam4")])

colData(grset) <- pheno

library(IlluminaHumanMethylation450kanno.ilmn12.hg19)
rowData(grset) <-  IlluminaHumanMethylation450kanno.ilmn12.hg19::Other[featureNames(grset),]

save(grset, file = "/Lacie_CRW10023/HELIX_preproc/methylation/QC_CRA/GenomicMethylationSetFinalSamples.Rdata")


grpanel <- grset[ , grset$Period != "1X"]
grsubcohort <- grset[ , grset$Period != "1B"]

library(MEAL)

methylome_panel_notfitr  <- prepareMethylationSet(grpanel, data.frame(colData(grpanel)))
methylome_subcohort_notfitr <- prepareMethylationSet(grsubcohort, data.frame(colData(grsubcohort)))

save(methylome_panel_notfitr,
     file ="/Lacie_CRW10023/HELIX_preproc/methylation/QC_CRA/methylome_panel_notfitr.Rdata")
save(methylome_subcohort_notfitr,
     file ="/Lacie_CRW10023/HELIX_preproc/methylation/QC_CRA/methylome_subcohort_notfitr.Rdata")


### Version v2: Use GenomicRatioSet

methylome_panel_notfitr <- grset[ , grset$Period != "1X"]
methylome_subcohort_notfitr <- grset[ , grset$Period != "1B"]

save(methylome_panel_notfitr,
     file ="/Lacie_CRW10023/HELIX_preproc/methylation/QC_CRA/methylome_panel_notfitr_v2.Rdata")
save(methylome_subcohort_notfitr,
     file ="/Lacie_CRW10023/HELIX_preproc/methylation/QC_CRA/methylome_subcohort_notfitr_v2.Rdata")


### Version v4: Use GenomicRatioSet with all chrY probes

methylome_panel_notfitr <- grset[ , grset$Period != "1X"]
methylome_subcohort_notfitr <- grset[ , grset$Period != "1B"]

save(methylome_panel_notfitr, file = "methylome_panel_notfitr_v4.Rdata")
save(methylome_subcohort_notfitr, file ="methylome_subcohort_notfitr_v4.Rdata")

## Apply ComBat (Protect sex, age, cohort and 6 cell types)
### Convert beta values to M-values (to apply combat)
beta <- getBeta(grset)
beta[beta == 0] <- 0.00001
beta[beta == 1] <- 0.99999
mvals <- logit2(beta)

### Combat - Slide
modcombat <- model.matrix(~ e3_sex + age_sample_years + cohort + NK_6 + Bcell_6 +
                                  CD4T_6 + CD8T_6 + Gran_6 + Mono_6, 
                          data=pData(grset))
batch <- grset$Slide
combat_Slide <- ComBat(dat=mvals, batch=batch, mod=modcombat, par.prior=TRUE, 
                       prior.plots=FALSE)

### Substitute raw methylation values by ComBat methylation values (beta values)
grset_slide <- grset
grset_slide@assays$data$Beta <- ilogit2(combat_Slide)

### save ALL samples and ALL CpGs
save(grset_slide, file = "methylome_all_ComBatSlide.Rdata")


methylome_subcohort_ComBatSlide_notfitr <- grset_slide[,grset_slide$Period!="1B"]
save(methylome_subcohort_ComBatSlide_notfitr, file = "methylome_subcohort_ComBatSlide_notfitr_v4.Rdata")

methylome_panel_ComBatSlide_notfitr <- grset_slide[,grset_slide$Period!="1X"]
save(methylome_panel_ComBatSlide_notfitr, file = "methylome_panel_ComBatSlide_notfitr_v4.Rdata")



## Filter bad probes
badProbes <- unique(c(sexProbes, crossProbes, SNPsProbes))

grpanel <- grpanel[!rownames(grpanel) %in% badProbes, ]
grsubcohort <- grsubcohort[!rownames(grsubcohort) %in% badProbes, ]
methylome_panel <- prepareMethylationSet(grpanel, data.frame(colData(grpanel)))
methylome_subcohort <- prepareMethylationSet(grsubcohort, data.frame(colData(grsubcohort)))

save(methylome_panel,
     file ="/Lacie_CRW10023/HELIX_preproc/methylation/QC_CRA/methylome_panel.Rdata")
save(methylome_subcohort,
     file ="/Lacie_CRW10023/HELIX_preproc/methylation/QC_CRA/methylome_subcohort.Rdata")

### Version v2: Use GenomicRatioSet

methylome_panel <- methylome_panel_notfitr[!rownames(methylome_panel_notfitr) %in% badProbes, ]
methylome_subcohort <- methylome_subcohort_notfitr[!rownames(methylome_subcohort_notfitr) %in% badProbes, ]

save(methylome_panel,
     file ="/Lacie_CRW10023/HELIX_preproc/methylation/QC_CRA/methylome_panel_v2.Rdata")
save(methylome_subcohort,
     file ="/Lacie_CRW10023/HELIX_preproc/methylation/QC_CRA/methylome_subcohort_v2.Rdata")





### Create final version of targets (without duplicates)
load("/Lacie_CRW10023/HELIX_preproc/methylation/QC_CRA/targets_final.RData")
outliers <- read.csv("all_outliers.csv", row.names = 1, as.is = TRUE)

## Remove outliers
targets <- targets[!rownames(targets) %in% outliers$ID,  ]

## Remove Hapmap Samples
targets_noHapMap <- targets[-grep("NA|Na", targets$Sample_Name), ]

## Remove duplicates
targets_final <- targets_noHapMap[!duplicated(targets_noHapMap$Sample), ]


write.csv(targets_final[, 1:11], file = "/Lacie_CRW10023/HELIX_preproc/methylation/QC_CRA/finalSamples.csv")

outliers$HapMap <- FALSE
outliers$Duplicates <- FALSE

HapMap <- data.frame(ID = rownames(targets)[grep("NA|Na", targets$Sample_Name)], MU = FALSE, 
                     OP = FALSE, BS = FALSE, HC = FALSE, DP = FALSE, Sex = FALSE, SNPs = FALSE, 
                     Sample_Name = targets$Sample_Name[grep("NA|Na", targets$Sample_Name)], 
                     HapMap = TRUE, Duplicates = FALSE)

Duplicates <- data.frame(ID = rownames(targets_noHapMap)[duplicated(targets_noHapMap$Sample)], MU = FALSE, 
                     OP = FALSE, BS = FALSE, HC = FALSE, DP = FALSE, Sex = FALSE, SNPs = FALSE, 
                     Sample_Name = targets$Sample_Name[duplicated(targets_noHapMap$Sample)],
                     HapMap = FALSE, Duplicates = TRUE)

outliers <- rbind(outliers, HapMap, Duplicates)
rownames(outliers) <- outliers$ID

write.csv(outliers, file = "/Lacie_CRW10023/HELIX_preproc/methylation/QC_CRA/outliers_final.csv")
