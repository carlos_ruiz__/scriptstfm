###################################################################################
# Cell counts statistics
###################################################################################

# Create plots of cell counts #####
###################################################################################
library(loadxls)
library(foreign)
library(ggplot2)

load("/Lacie_CRW10023/HELIX_preproc/methylation/QC_CRA/FinalSetsAllProbes.Rdata")

cellTypes <- colData(grsubcohort)[, 47:53]

res <- sapply(cellTypes, function(x) c(min = min(x), max = max(x), mean = mean(x), median = median(x), sd = sd(x)))
write.csv(res, file ="/Lacie_CRW10023/HELIX_preproc/methylation/QC_CRA/cellTypesDescriptives.csv")

pdf("/Lacie_CRW10023/HELIX_preproc/methylation/QC_CRA/cellTypesHistograms.pdf")
sapply(names(cellTypes), function(x) hist(cellTypes[, x], main = x, col = "cyan", xlab = ""))
dev.off()


sapply(names(cellTypes), function(x) {
  pdf(paste0("/Lacie_CRW10023/HELIX_preproc/methylation/QC_CRA/Histogram", x, ".pdf"));
  hist(cellTypes[, x], main = x, col = "cyan", xlab = "")
  dev.off()})


#  Compute the correlation between the cell counts and the smears #####
###################################################################################
smears <- read_sheet("/Lacie_CRW10023/HELIX_preproc/blood_cell_prop/HELIX_cell_prop_smears_20161104.xlsx", 
                     "Cell_prop_smears")

load("/Lacie_CRW10023/HELIX_preproc/methylation/QC_CRA/GenomicMethylationSetFinalSamples.Rdata")

colnames(smears) <- smears[1, ]
smears <- smears[-1, ]
rownames(smears) <- smears$HELIX_sample_ID
smears[, 10:15] <- lapply(smears[, 10:15], as.numeric)

celltypes <- colData(grset)[, 47:53]
rownames(celltypes) <- toupper(grset$Sample_Name)

common <- intersect(rownames(celltypes), rownames(smears))
whole <- data.frame(celltypes[common, ], smears[common, ])

attach(whole)

pdf("/Lacie_CRW10023/HELIX_preproc/methylation/QC_CRA/NeutrophilsCorr.pdf")
#' Neutrophils (NEUSEG + NEUBANDA = Neu (minfi))
ggplot(whole, aes(x=NEUSEG + NEUBANDA, y=Neu)) + geom_point() + geom_smooth(method=lm) +
  ylab("Minfi prediction") + xlab("Smears") + ggtitle("Neutrophils measures")
dev.off()
round(unlist(cor.test(x=NEUSEG + NEUBANDA , y=Neu, method = "spearman")[c("estimate", "p.value")]), 3)

#' EOS: Eos (minfi)
pdf("/Lacie_CRW10023/HELIX_preproc/methylation/QC_CRA/EosinophilsCorr.pdf")
ggplot(whole, aes(x=EOS , y=Eos)) + geom_point() + geom_smooth(method=lm) +
  ylab("Minfi prediction") + xlab("Smears") + ggtitle("Eosinophils measures")
dev.off()

round(unlist(cor.test(x=EOS , y=Eos)[c("estimate", "p.value")]), 3)

# Lymphocytes (LINFOS: CD8T+CD4T+Nk+Bcell (minfi))
pdf("/Lacie_CRW10023/HELIX_preproc/methylation/QC_CRA/LimphosCorr.pdf")
ggplot(whole, aes(x=LINFOS, y= CD8T+CD4T+NK+Bcell)) + geom_point() + geom_smooth(method=lm) +
  ylab("Minfi prediction") + xlab("Smears") + ggtitle("Lymphocytes meaures")
dev.off()

round(unlist(cor.test(x=LINFOS , y= CD8T+CD4T+NK+Bcell)[c("estimate", "p.value")]), 3)

# MONOS: Mono (minfi)
pdf("/Lacie_CRW10023/HELIX_preproc/methylation/QC_CRA/MonoCorr.pdf")
ggplot(whole, aes(x=MONOS , y=Mono)) + geom_point() + geom_smooth(method=lm) +
  ylab("Minfi prediction") + xlab("Smears") + ggtitle("Monocytes measures")
dev.off()
round(unlist(cor.test(x=MONOS , y=Mono)[c("estimate", "p.value")]), 3)
