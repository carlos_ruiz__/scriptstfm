#!/bin/bash


# Script for running the linear regressions between
# methylation and expression data using an R script
# sampling expression samples

# Linear reg 2

for j in {1..100}
  do 
  echo simulation $j
  mkdir sim$j
  for i in {1..22}
  do
  echo $i
  Rscript linear_m_subsets2_simulations.R $i $j
  done
done 2> sims.log

