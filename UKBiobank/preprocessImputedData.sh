#'#################################################################################
#'#################################################################################
#' Preprocess imputed data from UK Biobank for scoreInvHap
#'#################################################################################
#'#################################################################################

### Create folder in scracth
#### Link data folder
ln -s ~/PublicData/STUDY/UKBIOBANK/ data

mkdir results
mkdir results/preprocess

preproc=results/preprocess

## Create file with ranges (R)
library(scoreInvHap)
library(GenomicRanges)

seqlevelsStyle(inversionGR) <- "NCBI"
df <- data.frame(as.character(inversionGR), stringsAsFactors = FALSE)
df$chr <- as.character(seqnames(inversionGR))

onePoschr <- nchar(df$chr) == 1 & df$chr != "X"
df[onePoschr, 1] <- paste0("0", df[onePoschr, 1])

write.table(df, row.names = TRUE, col.names = FALSE, 
            quote = FALSE, "results/preprocess/ranges.txt")

## Create VCFs ####

$1=inv
$2=range
$3=chr
makeVCF(){
  
  qctool_v2.0.2 -g data/ukb_imp_chr${3}_v3.bgen -og $preproc/${1}.bgen -incl-range ${2}
  
  ## Remove SNPs with low MAF (< 1%)
  qctool_v2.0.2 -g $preproc/${1}.bgen -snp-stats -osnp $preproc/snp-stats_${1}.txt
  awk '$14 < 0.01 {print $2}' $preproc/snp-stats_${1}.txt > $preproc/bad_snps_${1}.txt
  
  ## Convert to vcf
  qctool_v2.0.2 -g $preproc/${1}.bgen -og $preproc/${1}.vcf -s data/ukb43983_imp_chr8_v3_s487324.sample -excl-rsids $preproc/bad_snps_${1}.txt
  bgzip $preproc/${1}.vcf 
  tabix -p vcf $preproc/${1}.vcf.gz
  
}

while read -r LINE 
do
  inv=`echo $LINE | cut -d' ' -f1`
  range=`echo $LINE | cut -d' ' -f2`
  chr=`echo $LINE | cut -d' ' -f3`

  makeVCF $inv $range $chr
done < $preproc/ranges.txt

## Repeat for invX_006 (Use other sample file)
qctool_v2.0.2 -g $preproc/invX_006.bgen -snp-stats -osnp $preproc/snp-stats_invX_006.txt
awk '$14 < 0.01 {print $2}' $preproc/snp-stats_invX_006.txt > $preproc/bad_snps_invX_006.txt
qctool_v2.0.2 -g $preproc/invX_006.bgen -og $preproc/invX_006.vcf -s data/ukb43983_imp_chrX_v3_s486669.sample -excl-rsids $preproc/bad_snps_invX_006.txt 
bgzip $preproc/invX_006.vcf 
tabix -p vcf $preproc/invX_006.vcf.gz


## Change vcf header
for i in $(cut -d' ' -f1 $preproc/ranges.txt)
do
echo $i
  gzip -cd $preproc/$i.vcf.gz | head -n 10 | grep '#' > $preproc/head_${i}.txt
  sed 's/Number=G/Number=3/g' $preproc/head_${i}.txt > $preproc/head_${i}_corr.txt
  bcftools reheader $preproc/${i}.vcf.gz -h $preproc/head_${i}_corr.txt -o $preproc/${i}.header.vcf.gz
done

# Code for inv8 ####
## Subset
qctool_v2.0.2 -g data/ukb_imp_chr8_v3.bgen -og $preproc/inv8.bgen -incl-range 8:8055789-11980649 

## Remove SNPs with low MAF (< 1%)
qctool_v2.0.2 -g $preproc/inv8.bgen -snp-stats -osnp $preproc/snp-stats_inv8.txt
awk '$14 < 0.01 {print $2}' $preproc/snp-stats_inv8.txt > $preproc/bad_snps_inv8.txt

## Convert to vcf
qctool_v2.0.2 -g $preproc/inv8.bgen -og $preproc/inv8.vcf -s data/ukb43983_imp_chr8_v3_s487324.sample -excl-rsids $preproc/bad_snps_inv8.txt
bgzip $preproc/inv8.vcf 
tabix -p vcf $preproc/inv8.vcf.gz

## Change header
gzip -cd inv8.vcf.gz | head -n 3 > head.txt
## Change text with nano -> Number=3
bcftools reheader inv8.vcf.gz -h head.txt -o inv8.header.vcf.gz

## Get SNP freqs
bgzip -cd inv8.header.vcf.gz > inv8.header.vcf
plink2 --vcf inv8.header.vcf.gz 'dosage=GP'  --freq --out inv8

# Code for inv16 ####
i=inv16_009
gzip -cd $preproc/$i.vcf.gz | head -n 10 | grep '#' > $preproc/head_${i}.txt
sed 's/Number=G/Number=3/g' $preproc/head_${i}.txt > $preproc/head_${i}_corr.txt
bcftools reheader $preproc/${i}.vcf.gz -h $preproc/head_${i}_corr.txt -o $preproc/${i}.header.vcf.gz

# Code for inv16 ####
i=inv10_005
makeVCF inv10_005 10:27220925-27656433 10
gzip -cd $preproc/$i.vcf.gz | head -n 10 | grep '#' > $preproc/head_${i}.txt
sed 's/Number=G/Number=3/g' $preproc/head_${i}.txt > $preproc/head_${i}_corr.txt
bcftools reheader $preproc/${i}.vcf.gz -h $preproc/head_${i}_corr.txt -o $preproc/${i}.header.vcf.gz
