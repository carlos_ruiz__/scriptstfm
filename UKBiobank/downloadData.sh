#'#################################################################################
#'#################################################################################
#' Download imputed data from UK Biobank
#'#################################################################################
#'#################################################################################

## Download SNP data
for i in 1 2 3 6 7 8 11 12 14 17 21 X; 
do
  ~/PublicData/ukbiobank/download_software/ukbgene cal -c$i -a"k43983.key" -v
done;

## Chromosome X
~/PublicData/ukbiobank/download_software/ukbgene cal -c23 -a"k43983.key" -v


## Download Imputed data
for i in 1 2 3 6 7 8 11 12 14 17 21 X; 
do
  ~/PublicData/ukbiobank/download_software/ukbgene imp -c$i -a"k43983.key" -v | tee chr$i.imput.log; 
done;

## Download intensities
for i in 22 Y; 
do
  ~/PublicData/ukbiobank/download_software/ukbgene int -c$i -a"k43983.key" -v | tee chr$i.intensities.log; 
done;

## Download CNV log2r
for i in 22 Y; 
do
  ~/PublicData/ukbiobank/download_software/ukbgene l2r -c$i -a"k43983.key" -v | tee chr$i.l2r.log; 
done;


## Download CNV baf
for i in 22 Y; 
do
  ~/PublicData/ukbiobank/download_software/ukbgene baf -c$i -a"k43983.key" -v | tee chr$i.baf.log; 
done;

## Chromosome 16
~/PublicData/ukbiobank/download_software/ukbgene imp -c16 -a"k43983.key" -v | tee chr16.imput.log; 

## Chromosome 10
~/PublicData/ukbiobank/download_software/ukbgene imp -c10 -a"k43983.key" -v | tee chr10.imput.log; 


## Download link file
~/PublicData/ukbiobank/download_software/ukbgene imp -c8 -a"k43983.key" -m

## Download link file for chromosome X
~/PublicData/ukbiobank/download_software/ukbgene imp -cX -a"k43983.key" -m

## Download phenotype data
## Test md5sum
~/PublicData/ukbiobank/download_software/ukbmd5 ukb27743.enc

## Unpack
~/PublicData/ukbiobank/download_software/ukbunpack ukb27743.enc k43983_b.key

## Convert
~/PublicData/ukbiobank/download_software/ukbconv  ukb27743.enc_ukb r
~/PublicData/ukbiobank/download_software/ukbconv  ukb27743.enc_ukb docs

## Download phenotype data (29121)
## Test md5sum
~/PublicData/ukbiobank/download_software/ukbmd5 ukb29121.enc

## Unpack
~/PublicData/ukbiobank/download_software/ukbunpack ukb29121.enc k43983_id29121.key

## Convert
~/PublicData/ukbiobank/download_software/ukbconv  ukb29121.enc_ukb r
~/PublicData/ukbiobank/download_software/ukbconv  ukb29121.enc_ukb docs


## Download phenotype data (31001)
## Test md5sum
~/PublicData/ukbiobank/download_software/ukbmd5 ukb31001.enc

## Unpack
~/PublicData/ukbiobank/download_software/ukbunpack ukb31001.enc k43983_id31001.key

## Convert
~/PublicData/ukbiobank/download_software/ukbconv  ukb31001.enc_ukb r
~/PublicData/ukbiobank/download_software/ukbconv  ukb31001.enc_ukb docs


## Download phenotype data (39983)
## Test md5sum
~/PublicData/ukbiobank/download_software/ukbmd5 ukb39983.enc

## Unpack
~/PublicData/ukbiobank/download_software/ukbunpack ukb39983.enc k43983_id39983.key

## Convert
~/PublicData/ukbiobank/download_software/ukbconv  ukb39983.enc_ukb r
~/PublicData/ukbiobank/download_software/ukbconv  ukb39983.enc_ukb docs
