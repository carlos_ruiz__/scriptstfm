#Download Files

wget https://tcga-data.nci.nih.gov/docs/publications/gbm_2013/unc.edu_GBM.H-miRNA_8x15K.Level_3.1.8.0.tar.gz
wget https://tcga-data.nci.nih.gov/docs/publications/gbm_2013/mdanderson.org_GBM.MDA_RPPA_Core.Level_3.1.0.0.tar.gz
wget https://tcga-data.nci.nih.gov/docs/publications/gbm_2013/GBM.Clinical.tar

# Methylation
wget https://tcga-data.nci.nih.gov/docs/publications/gbm_2013/GBM.HumanMethylation450.Level_3.tar
tar xf GBM.HumanMethylation450.Level_3.tar
mkdir methylation
mv jhu-usc.edu_GBM.HumanMethylation450.Level_3.* ./methylation/
mv GBM.HumanMethylation450.Level_3.tar methylation/
cd methylation
for filename in *.tar.gz
do
tar zxf $filename
done       

# Expression
wget https://tcga-data.nci.nih.gov/docs/publications/gbm_2013/GBM.Gene_Expression.Level_3.tar
tar xf GBM.Gene_Expression.Level_3.tar
mkdir expression
mv broad.mit.edu_GBM.HT_HG-U133A.Level_3.* ./expression/
mv GBM.Gene_Expression.Level_3.tar ./expression/
cd expression
for filename in *.tar.gz
do
tar zxf $filename
done    