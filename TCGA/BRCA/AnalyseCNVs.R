library(SNPassoc)

load("/home/cruiz/TCGA/CNVs/inv45CNVs.Rdata")

summary(glm(inversion ~ recessive(invGenotype)*tissue, data = CNVpartInV, family = "poisson"))
##pval significativo
summary(glm(chromosome ~ recessive(invGenotype)*tissue, data = CNVpartInV, family = "poisson"))
summary(glm(total ~ recessive(invGenotype)*tissue, data = CNVpartInV, family = "poisson"))
##pval significativo

summary(glm(inversion ~ recessive(invGenotype)*tissue, data = CNVallInV, family = "poisson"))
##pval significativo
summary(glm(chromosome ~ recessive(invGenotype)*tissue, data = CNVallInV, family = "poisson"))
summary(glm(total ~ recessive(invGenotype)*tissue, data = CNVallInV, family = "poisson"))
##pval significativo