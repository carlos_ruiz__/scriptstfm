## Analyze CNVs BRCA
load("/NewLacie_CRW10082/DATASETS/STUDY/TCGA/INVERSIONS/invCalling.Rdata")
brca <- getFirehoseData("BRCA", runDate=rundates[1], gistic2_Date=analysisdates[1], 
                        CNV_SNP=TRUE)

cnvs <- extract(brca, "CNV_SNP")
ids <- toupper(substr(names(cnvs), 1,12))
ids.ok <- intersect(ids, rownames(inversions))