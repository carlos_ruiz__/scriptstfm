obtainRegionValues <- function(invname){
  load(paste0(invname, ".Rdata"))
  values <- regionR2(results$dominant$region)
  values <- c(values, regionPval(results$dominant$region))
  values <- c(values, regionR2(results$recessive$region))
  values <- c(values, regionPval(results$recessive$region))
  values <- c(values, regionR2(results$additive$region))
  values <- c(values, regionPval(results$additive$region))
  return (values)
}

library(methassoc)
inversions <- sapply(dir(, pattern = ".Rdata"), function(x) substring(x, 1, nchar(x) - 6), USE.NAMES = FALSE)
regionvalues <- sapply(inversions, obtainRegionValues)
regionvalues <- t(regionvalues)
colnames(regionvalues) <- rep(c("R2", "pVal"), 3)
write.csv2(regionvalues, file = "rdaValues.csv")
