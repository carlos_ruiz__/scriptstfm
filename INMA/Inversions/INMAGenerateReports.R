saveRMD <- function(inv){
  invtext <- writeRMD(inv)
  write(invtext, file=paste0(inv, ".Rmd"))
}

writeRMD <- function(inv){
  paste0("```{r set-options, echo=FALSE, cache=FALSE}
options(width = 120)
```
         
# Inversion ", inv, "\n```{r, message=FALSE}
library(methassoc)
load(\"",inv,".Rdata\")
range <- getRange(results$dominant$region)
```

## Dominant model

### Global results
```{r, warning=FALSE}
#Probe results
head(probeResults(results$dominant$global)[[1]])
#Bumphunter
head(bumps(results$dominant$global)[[1]])
#DMRcate
head(dmrCate(results$dominant$global)[[1]])
```
```{r, warning=FALSE,dev=\"CairoPNG\", echo=FALSE}
plotBestCPGs(results$dominant$global, 5)
plotQQ(results$dominant$global)
plotEWAS(results$dominant$global, range = range)
```

### Region Analysis
```{r, warning=FALSE}
#Probe results
head(probeResults(results$dominant$region)[[1]])
#Bumphunter
head(bumps(results$dominant$region)[[1]])
#DMRcate
head(dmrCate(results$dominant$region)[[1]])
```
```{r, warning=FALSE,dev=\"CairoPNG\", echo=FALSE}
bestcpgs <- probeResults(results$dominant$region)[[1]][1:5, 1]
invisible(lapply(bestcpgs, function(x) plotRegionR2(results$dominant$region, cpg = x)))
plotRegion(results$dominant$region)
plotRDA(results$dominant$region)
```
## Recessive model

### Global results
```{r, warning=FALSE}
#Probe results
head(probeResults(results$recessive$global)[[1]])
#Bumphunter
head(bumps(results$recessive$global)[[1]])
#DMRcate
head(dmrCate(results$recessive$global)[[1]])
```
```{r, warning=FALSE,dev=\"CairoPNG\", echo=FALSE}
plotBestCPGs(results$recessive$global, 5)
plotQQ(results$recessive$global)
plotEWAS(results$recessive$global, range = range)
```

### Region Analysis
```{r, warning=FALSE}
#Probe results
head(probeResults(results$recessive$region)[[1]])
#Bumphunter
head(bumps(results$recessive$region)[[1]])
#DMRcate
head(dmrCate(results$recessive$region)[[1]])
```
```{r, warning=FALSE,dev=\"CairoPNG\", echo=FALSE}
bestcpgs <- probeResults(results$recessive$region)[[1]][1:5, 1]
invisible(lapply(bestcpgs, function(x) plotRegionR2(results$recessive$region, cpg = x)))
plotRegion(results$recessive$region)
plotRDA(results$recessive$region)
```

## Additive model

### Global results
```{r, warning=FALSE}
#Probe results
head(probeResults(results$additive$global)[[1]])
#Bumphunter
head(bumps(results$additive$global)[[1]])
#DMRcate
head(dmrCate(results$additive$global)[[1]])
```
```{r, warning=FALSE,dev=\"CairoPNG\", echo=FALSE}
plotBestCPGs(results$additive$global, 5)
plotQQ(results$additive$global)
plotEWAS(results$additive$global, range = range)
```

### Region Analysis
```{r, warning=FALSE}
#Probe results
head(probeResults(results$additive$region)[[1]])
#Bumphunter
head(bumps(results$additive$region)[[1]])
#DMRcate
head(dmrCate(results$additive$region)[[1]])
```
```{r, warning=FALSE,dev=\"CairoPNG\", echo=FALSE}
bestcpgs <- probeResults(results$additive$region)[[1]][1:5, 1]
invisible(lapply(bestcpgs, function(x) plotRegionR2(results$additive$region, cpg = x)))
plotRegion(results$additive$region)
plotRDA(results$additive$region)
```
") 
}

library(knitr)

inversions <- sapply(dir(, pattern = ".Rdata"), function(x) substring(x, 1, nchar(x) - 6), USE.NAMES = FALSE)
lapply(inversions, saveRMD)
lapply(paste0(inversions, ".Rmd"), knit2html)