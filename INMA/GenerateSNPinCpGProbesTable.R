#### Download file from UCSC -> hg19 -> annotation database -> all
processUCSCsnp <- function(snpfile) {
  require(GenomicRanges)
  cat("Reading file\n")
  df <- read.delim(gzfile(snpfile), header = FALSE,
                   stringsAsFactors = FALSE)
  names(df) <- c("chr", "start", "end", "name", "strand",
                 "refNCBI", "class", "alleleFreqs")
  print(table(df$chr))
  cat("Only keeping chrs 1-22, X, Y\n")
  df <- df[df$chr %in% paste0("chr", c(1:22, "X", "Y")),]
  print(table(df$class))
  cat("Only keeping class 'single'\n")
  df <- df[df$class == "single",]
  cat("Computing MAF\n")
  df$alleleFreqs <- sub(",$", "", df$alleleFreqs)
  sp <- strsplit(df$alleleFreqs, ",")
  sp <- lapply(sp, function(x) ifelse(!length(x), NA, x))
  minFreq <- sapply(sp, function(xx) min(as.numeric(xx)))
  cat("Instatiating object\n")
  rm(sp)
  gc()
  grSNP <- GRanges(seqnames = df$chr, strand = df$strand, ranges = IRanges(start = df$start + 1, end = df$end),
                   MAF = minFreq, ref = df$refNCBI)
  names(grSNP) <- df$name
  grSNP
}


#### Bash code
curl -O ftp://hgdownload.soe.ucsc.edu//apache/htdocs/goldenPath/hg19/database/snp142.txt.gz

gunzip -c snp142.txt.gz | cut -f2,3,4,5,7,8,12,25 | gzip - > snp142All_small.txt.gz

### Uncompress prior to processing
grSNP <- processUCSCsnp("snp142All_small.txt.gz")

library(IlluminaHumanMethylation450kanno.ilmn12.hg19)
anno <- get("IlluminaHumanMethylation450kanno.ilmn12.hg19")
Locations <- anno@data$Locations
Manifest <- anno@data$Manifest

map <- cbind(Locations, Manifest)
map <- GRanges(seqnames = map$chr, ranges = IRanges(start = map$pos, width = 1),
               Strand = map$strand, Type = map$Type)
map <- minfi:::getProbePositionsDetailed(map)
names(map) <- rownames(Locations)

SNPs142All <- minfi:::.doSnpOverlap(map, grSNP)

SNPs142 <- data.frame(SNPs142All[, c(2,4,6)])
SNPs142[is.na(SNPs142)] <- 0
SNPs142[(SNPs142) == "Inf"] <- 0
SNPsProbe <- rownames(SNPs142)[rowSums(SNPs142) > 0]
SNPsNoProbe <- rownames(SNPs142)[!rowSums(SNPs142) > 0]
SNPsCpG <- rownames(SNPs142)[rowSums(SNPs142[, 2:3]) >  0]
SNPsNoCpG <- rownames(SNPs142)[!rowSums(SNPs142[, 2:3]) > 0]