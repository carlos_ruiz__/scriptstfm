---
title: "Untitled"
author: "Carlos Ruiz"
date: "Wednesday, February 18, 2015"
output: html_document
---

# Cell count histogram

```{r, echo=FALSE}
  load("/Lacie_RRW10023/DATASETS/STUDY/INMA/Methylation_INMA/450K_blood/DATA/cell_countL.Rdata")
a <- lapply(colnames(count.all2), function(x) hist(count.all2[,x], main=x, xlab=x))
```
